import { Router } from '@angular/router';
import {
  OnInit,
  Output,
  Component,
  EventEmitter,
} from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Subscription } from "rxjs/Subscription";
import { DataService } from "../../admin/settings/data.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { AuthenticationService } from './../../api-module/services/authentication/authentication.service';

@Component({
  selector: 'app-main-header-component',
  templateUrl: './main-header-component.component.html',
  styleUrls: ['./main-header-component.component.css'],
})

export class MainHeaderComponentComponent implements OnInit {
  isLoggedin: boolean = false;
  toggleNotification: boolean;
  newNotificationToggle: boolean;
  notificationBackgroundClass: any[];
  CurentUser: any = {};
  toggelingNotificationIconInterval: any;
  quotationSubscription: Subscription;
  callSubscription: Subscription;
  orderSubscription: Subscription;
  notifications: any[] = [];
  toggelingNotificationIconClass: string[] = [];

  image: string = '';
  aboutus: string = '';

  @Output() loggedOut = new EventEmitter();

  constructor(
    public authenticationService: AuthenticationService,
    private router: Router,
    private utilities: UtilitiesService,
    private lookupService: LookupService,
    private dataService: DataService,
    private titleService: Title
  ) {

    this.dataService.subject.subscribe(msg => {
      for (const key in msg) {
        if (msg.hasOwnProperty(key)) {
          const element = msg[key];
          if (element.key === 'image') {
            this.image = element.value;
          } else if (element.key === 'title') {
            this.titleService.setTitle(element.value);
          }
        }
      }
    });

  }



  ngOnInit() {
    this.getSettings();
    this.isLoggedin = this.authenticationService.isLoggedIn();
    this.CurentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    this.notifications = [];
  }


  routeToNotification(notificationData) {
    this.utilities.routingFromAndHaveSearch = true;
    this.utilities.currentSearch = {};
    if (notificationData.type == 'quotation') {
      this.utilities.currentSearch.searchText = notificationData.refNumber;
      this.utilities.currentSearch.searchType = 'quotation';
      this.utilities.routingFromAndHaveSearch = true;
      this.utilities.setSavedNotificationText(notificationData.refNumber);
      this.router.navigate(['/search/quotation'])
    } else if (notificationData.type == 'call') {
      this.utilities.currentSearch.searchText = notificationData.callerNumber;
      this.utilities.routingFromAndHaveSearch = true;
      this.utilities.setSavedNotificationText(notificationData.callerNumber);
      this.router.navigate(['/search/calls-history'])
    } else if (notificationData.type == 'order') {
      console.log('order');
      this.utilities.setSavedNotificationText(notificationData);
      // this.utilities.currentNotificationOrder = notificationData;
      this.router.navigate(['/dispatcher'])
    }
    this.toggleNotificationBody();
  }

  LogoutAll() {
    this.notifications = [];
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ['show-notification-icon'];
    this.stopIntervalNotificationIconToggeling();
    this.loggedOut.emit();
    this.router.navigate(['/login']);
  }

  toggleNotificationBody() {
    this.toggleNotification = !this.toggleNotification;
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ['show-notification-icon'];
    this.stopIntervalNotificationIconToggeling();
    if (this.notificationBackgroundClass.toString() == ['show-notification-transparent-background'].toString()) {
      this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    } else {
      this.notificationBackgroundClass = ['show-notification-transparent-background'];
    }
  }

  toggelingNotificationIcon() {
    this.toggelingNotificationIconInterval = setInterval(() => {
      console.log(this.newNotificationToggle);
      if (this.newNotificationToggle) {
        if (this.toggelingNotificationIconClass.toString() == ['show-notification-icon'].toString()) {
          this.toggelingNotificationIconClass = ['hide-notification-icon'];
        } else {
          this.toggelingNotificationIconClass = ['show-notification-icon'];
        }
      } else {
        this.toggelingNotificationIconClass = ['show-notification-icon'];
      }
      // this.toggelingNotificationIcon(this.newNotificationToggle);
    }, 1000)
  }

  stopIntervalNotificationIconToggeling() {
    clearInterval(this.toggelingNotificationIconInterval);
  }


  /**
   * settings 
   * 
   * 
   */
  getSettings() {
    this.lookupService.getWebsiteSettingsBykey().subscribe(settings => {
      let array = [];
      array = settings;
      array.map(item => {
        if (item.key === 'image') {
          this.image = item.value
        } else if (item.key === 'aboutus' || item.key === 'about-us') {
          this.aboutus = item.value;
        } else if (item.key === 'title') {
          this.titleService.setTitle(item.value)
        }
      })
    });
  }

  changeLogo() {
    this.dataService.subject.subscribe(res => {
      console.log(res);
    })
  }
}

