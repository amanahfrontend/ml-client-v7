import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResetService {

  private messageSource = new BehaviorSubject(false);
  currentStatus = this.messageSource.asObservable();

  constructor() { }

  /**
   * 
   * @param status 
   */
  changeStatus(status: boolean) {
    console.log(status);
    this.messageSource.next(status);
  }

}
