import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ResetService } from './reset.service';

@Component({
  selector: 'app-date-from-to',
  templateUrl: './date-from-to.component.html',
  styleUrls: ['./date-from-to.component.css']
})

export class DateFromToComponent implements OnInit {

  @Input() submitted: boolean;
  @Input() required: boolean;
  @Input() layout: string;
  @Input() startDateLable: string;
  @Input() endDateLable: string;


  @Output() startDate = new EventEmitter();
  @Output() endDate = new EventEmitter();

  condition: boolean = true;
  date: any = {};
  layoutClass: string;

  constructor(private resetService: ResetService) {
  }

  ngOnInit() {
    this.layoutClass = this.layout;

    this.resetService.currentStatus.subscribe(res => {
      if (res) {
        this.resetStartDate();
        this.resetEndDate();
      }
    })
  }

  resetStartDate() {
    this.date.startDate = null;
    this.condition = false;
    this.condition = true;
  }

  resetEndDate() {
    this.date.endDate = null;
    this.condition = false;
    this.condition = true;
  }

  emitStartDate() {
    this.startDate.emit(this.date.startDate);
  }

  emitEndDate() {
    this.endDate.emit(this.date.endDate);
  }

  formatDate(date: Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('/');
  }


}
