import { Component, OnInit, ViewEncapsulation, Output, Input, EventEmitter } from '@angular/core';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { CustomerCrudService } from '../../api-module/services/customer-crud/customer-crud.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class SearchComponent implements OnInit {

  @Input() placholder: string;
  @Input() hideFromTo: boolean;
  @Input() hideDate: boolean = true;
  @Output() searchValue = new EventEmitter;
  @Output() searchCanceled = new EventEmitter;

  searchText: string;
  fromDate: any;
  toDate: any;
  errorFlag: boolean;
  submitted: boolean = false;

  constructor(private utilities: UtilitiesService, private customerService: CustomerCrudService) {
  }

  ngOnInit() {
    if (this.hideFromTo === undefined) {
      this.hideFromTo = true;
    } else {
      this.hideFromTo = false;
    }
  }

  clearSearch() {
    this.errorFlag = false;
    this.customerService.paginationFlag = false;
    this.utilities.paginationFlag = false;

    this.searchText = '';
    this.fromDate = '';
    this.toDate = '';
    this.utilities.currentSearch = this.searchText;
    this.searchCanceled.emit();
  }

  toggleErrorFlag() {
    this.errorFlag = true;
  }

  outPutSearchValue(formValues) {

    const text = formValues.value.searchText.trim();

    if (this.searchText == undefined || this.searchText == '' || this.searchText == null) {
      this.clearSearch();
      return;
    }
    if (formValues.value.searchText !== undefined) {
      this.submitted = true;

      if (!this.hideFromTo) {

        let fromDateToPost = formValues.value.fromDate && new Date(formValues.value.fromDate);
        let toDateToPost = formValues.value.toDate && new Date(formValues.value.toDate);

        let searchObject = {};
        if (text || fromDateToPost || toDateToPost) {
          text && (searchObject['searchText'] = text);
          fromDateToPost && (searchObject['fromDate'] = fromDateToPost.toISOString());
          toDateToPost && (searchObject['toDate'] = toDateToPost.toISOString());
        }

        this.utilities.currentSearch = searchObject;
        this.searchValue.emit(searchObject);
      } else {
        this.utilities.currentSearch = { searchText: text };
        this.searchValue.emit(text);
      }

    }
    
    

  }
}
