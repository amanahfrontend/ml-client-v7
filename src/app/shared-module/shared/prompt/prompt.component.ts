import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnDestroy
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-prompt',
  templateUrl: './prompt.component.html',
  styleUrls: ['./prompt.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PromptComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() type;
  @Input() data: any;
  @Input() placeholder: string = 'Please type value ...';

  statesSubscriptions: Subscription;
  states: any[];

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
    if (this.type == 'Order Progresses') {
      this.getStates();
    }
  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  close(result) {
    if (this.type == 'Order Progresses') {
      result = {
        id: this.data.id,
        name: result.name,
        fK_OrderStatus_Id: result.fK_OrderStatus_Id
      }
    } else if (this.type == 'Order Types') {
      result = {
        id: this.data.id,
        name: result.name,
        textColor: result.textColor,
        bgColor: result.bgColor,
      }
    } else {
      result = {
        id: this.data.id,
        name: result.name,
      }
    }
    this.activeModal.close(result);
  }

  getStates() {
    this.statesSubscriptions = this.lookup.getOrderStatus().subscribe((states) => {
      this.states = states;
    });
  }
}
