/**
 * An object used to get page information from the server
 */
export class Page {
  //The number of elements in the page
  pageSize: number = 0;
  totalElements: number = 0;
  searchBy: any;

  //The current page number
  pageNumber: number = 0;
  paginatedItemsViewModel: any;
  size: number;
  id: number;
}
