import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from 'src/app/api-module/services/authentication/authentication.service';
import { tap } from 'rxjs/operators';
import Swal from "sweetalert2";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authSer: AuthenticationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authReq = req.clone(
      {
        headers: new HttpHeaders(
          {
            'Authorization': "bearer " + this.authSer.getToken(),
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          }
        )
      }
    );

    return next.handle(authReq).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          setTimeout(() => {
          }, 1000);
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {

          console.log("Inerceort Error ", err);

          setTimeout(() => {

          }, 1000);

          if (err.status === 401) {
            Swal.fire({
              title: 'Session timed out, please login agian !',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#cc2406',
              cancelButtonText: 'Cancel !',
              confirmButtonText: 'Login',
            }).then((result) => {
              if (result.value) {
                this.authSer.logout();
              }
            });
          } else if (err.status === 400 && err.error !== '' && err.error != null ) {
            Swal.fire({
              title: err.error,
              type: 'error',
            });
          } else if (err.status === 400 && (err.error == null || err.error == '')) {
            Swal.fire({
              title: 'Bad request',
              type: 'error',
            });
          }
        }
      })
    );

  }
}
