/**
 * alersts service
 * 
 * @author Mustafa Omran <promustafaomran@hotmail.com>
 */
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

interface Alert {
  title: string;
  details?: any;
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private messageService: MessageService) { }


  /**
   * error
   * 
   * 
   * @param title 
   * @param details 
   */
  alertError(alert: Alert): void {
    this.messageService.add({ severity: 'error', summary: alert.title, detail: alert.details });
  }

  /**
   * success
   * 
   * 
   * @param title 
   * @param details 
   */
  alertSuccess(alert: Alert): void {
    this.messageService.add({ severity: 'success', summary: alert.title, detail: alert.details });
  }

  /**
   * info
   * 
   * 
   * @param title 
   * @param details 
   */
  alertInfo(alert: Alert): void {
    this.messageService.add({ severity: 'info', summary: alert.title, detail: alert.details });
  }

  /**
   * warning
   * 
   * 
   * @param title 
   * @param details 
   */
  alertWarning(alert: Alert): void {
    this.messageService.add({ severity: 'warn', summary: alert.title, detail: alert.details });
  }

  /**
   * clear alerts
   * 
   * 
   */
  clearAlerts(): void {
    this.messageService.clear();
  }
}
