import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { OrderDetailsModalComponent } from "../order-details-modal/order-details-modal.component";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { AuthenticationService } from 'src/app/api-module/services/authentication/authentication.service';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

@Component({
  selector: 'app-visit-time-calender',
  templateUrl: './visit-time-calender.component.html',
  styleUrls: ['./visit-time-calender.component.css']
})
export class VisitTimeCalenderComponent implements OnInit {
  orders: any[];
  header: any;
  calenderOrders: any[];
  orderDetailsRef: any;
  statusColors: any;
  allOrders: any;
  options: any;

  constructor(private lookup: LookupService,
    private auth: AuthenticationService,
    private modalService: NgbModal,
    private utilities: UtilitiesService) { }

  ngOnInit() {
    this.getDispatcherOrders();
    this.statusColors = this.utilities.statusColors;
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      eventClick: (e) => {
        this.showOrderDetails(e);
     } 

     // dateClick: (e) => {
     //   this.showOrderDetails(e);
     //} 
    };
  }

  showOrderDetails(e) {
    console.log(e);
    
    this.orders = this.allOrders;
    this.orderDetailsRef = this.modalService.open(OrderDetailsModalComponent,
      {
      size: "sm"
    });


    this.orderDetailsRef.componentInstance.order = this.orders.filter((order) => {
      return order.id == e.event.id
    })[0];
  }

  getDispatcherOrders() {
    const dispatcherId = this.auth.CurrentUser().id;

    this.lookup.getDisAllOrders(dispatcherId)
      .subscribe(
        (orders: any) => {          
          this.allOrders = orders.orders;
          this.calenderOrders = [];
          this.allOrders.map((order) => {
            this.calenderOrders.push({
              id: order.id,
              title: order.code,
              start: order.startDate,
              end: order.endDate,
              color: order.color
            })
          });
        }, err => {
          console.log(err);
        }
      );
  }

}
