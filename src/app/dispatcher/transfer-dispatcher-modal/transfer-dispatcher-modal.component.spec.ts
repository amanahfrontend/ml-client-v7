import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferDispatcherModalComponent } from './transfer-dispatcher-modal.component';

describe('TransferDispatcherModalComponent', () => {
  let component: TransferDispatcherModalComponent;
  let fixture: ComponentFixture<TransferDispatcherModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferDispatcherModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferDispatcherModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
