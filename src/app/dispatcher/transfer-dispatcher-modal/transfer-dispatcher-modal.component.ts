import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-transfer-dispatcher-modal',
  templateUrl: './transfer-dispatcher-modal.component.html',
  styleUrls: ['./transfer-dispatcher-modal.component.css'],
})

export class TransferDispatcherModalComponent implements OnInit {
  @Input() order;
  dispatchers: any[];
  choosedDispatcher: any;
  toggleLoading: boolean;

  constructor(public activeModal: NgbActiveModal, private lookupService: LookupService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.getDispatchers();
  }

  /**
   * 
   * @param dispatcher 
   */
  chooseDispatcher(dispatcher) {
    let transferToDispatcher = {
      fK_Dispatcher_Id: dispatcher.id,
      fK_Order_Id: this.order.id
    };

    this.lookupService.postTransferOrder(transferToDispatcher).subscribe(() => {
      this.activeModal.close();
    });
  }

  getDispatchers() {
    this.lookupService.getDispatchers().subscribe((dispatchers) => {
      console.log(dispatchers);
      this.dispatchers = dispatchers;
      this.toggleLoading = false;
    });
  }

  close() {
    this.activeModal.dismiss();
  }
}
