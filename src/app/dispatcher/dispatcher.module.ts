import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispatcherOrderBoardComponent } from './dispatcher-order-board/dispatcher-order-board.component';
import { RouterModule } from '@angular/router';
import { dispatcherRoutes } from './dispatcher-routes'
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { OrderCardComponent } from './order-card/order-card.component';
import { SetTimeModalComponent } from './set-time-modal/set-time-modal.component';
import { TransferDispatcherModalComponent } from './transfer-dispatcher-modal/transfer-dispatcher-modal.component';
import { OrderProgressModalComponent } from './order-progress-modal/order-progress-modal.component';
import { AgmCoreModule } from '@agm/core';
import { API_Key } from '../api-module/services/globalPath'
import { SidebarModule } from 'primeng/components/sidebar/sidebar';
import { ReportComponent } from './report/report.component';
import { OrderFilterComponent } from './order-filter/order-filter.component';
import { VisitTimeCalenderComponent } from './visit-time-calender/visit-time-calender.component';
import { ScheduleModule } from 'primeng/components/schedule/schedule';
import { OrderDetailsModalComponent } from './order-details-modal/order-details-modal.component';
import { ToggleButtonModule } from 'primeng/primeng';
import { BulkAssignModalComponent } from './bulk-assign-modal/bulk-assign-modal.component';
import { PreventiveComponent } from './preventive/preventive/preventive.component';
import { PreventiveDetailsModalComponent } from './preventive/preventive-details-modal/preventive-details-modal.component';
import { DialogModule } from 'primeng/primeng';
import { GalleriaModule } from 'primeng/primeng';
import { RoleGuard } from './../api-module/guards/role.guard';
import { CalendarModule } from 'primeng/calendar';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { CheckboxModule } from 'primeng/checkbox';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    SidebarModule,
    ScheduleModule,
    ToggleButtonModule,
    DialogModule,
    GalleriaModule,
    AgmCoreModule.forRoot({
      apiKey: API_Key
    }),
    RouterModule.forChild(dispatcherRoutes),
    CalendarModule,
    FullCalendarModule,
    CheckboxModule,
    PerfectScrollbarModule
  ],
  entryComponents: [
    SetTimeModalComponent,
    TransferDispatcherModalComponent,
    OrderProgressModalComponent,
    OrderDetailsModalComponent,
    BulkAssignModalComponent,
    PreventiveComponent,
    PreventiveDetailsModalComponent
  ],
  declarations: [
    DispatcherOrderBoardComponent,
    OrderCardComponent,
    SetTimeModalComponent,
    TransferDispatcherModalComponent,
    OrderProgressModalComponent,
    ReportComponent,
    OrderFilterComponent,
    VisitTimeCalenderComponent,
    OrderDetailsModalComponent,
    BulkAssignModalComponent,
    PreventiveComponent,
    PreventiveDetailsModalComponent
  ],
  providers: [
    RoleGuard,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})

export class DispatcherModule {
}
