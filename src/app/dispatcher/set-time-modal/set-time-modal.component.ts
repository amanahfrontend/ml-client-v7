import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Message } from "primeng/components/common/message";
import { AlertService } from '../../services/alert.service';
import { formatDate } from './../../helpers/date';

@Component({
  selector: 'app-set-time-modal',
  templateUrl: './set-time-modal.component.html',
  styleUrls: ['./set-time-modal.component.css']
})
export class SetTimeModalComponent implements OnInit {
  @Input() order: any;
  day: any;
  cornerMessage: Message[];
  dateToShowOnCal: any;
  defaultDate: any;
  minDate:Date;
  formatDate = formatDate;
  constructor(
    private activeModal: NgbActiveModal, 
    private lookup: LookupService,
    private alertService:AlertService) {
  }

  ngOnInit() {
    
    this.dateToShowOnCal = {};
    if (this.order.preferedVisitTime.toString() !== '0001-01-01T00:00:00') {      
      
      this.minDate = new Date(this.order.createdDate);

      this.order.preferedVisitTime = new Date(this.order.preferedVisitTime);
      let oldDate = new Date(this.order.preferedVisitTime);
      this.dateToShowOnCal = {
        day: oldDate.getDate(),
        month: oldDate.getMonth() + 1,
        year: oldDate.getFullYear()
      }
    } else {
      this.defaultDate = new Date();
      this.order.preferedVisitTime = this.defaultDate;
    }
  }

  setTime(time) {
    this.cornerMessage = [];
    let choosedTime: any = new Date(time.day);
    choosedTime = choosedTime.toISOString();

    let toPost = {
      preferedVisitTime: choosedTime,
      fK_Order_Id: this.order.id
    };

    this.lookup.postVisitTime(toPost).subscribe(() => {
      this.alertService.alertSuccess({title:'Successfully',details:'Date set Successfully!'});
      this.activeModal.close();
    },
      err => {
        this.alertService.alertSuccess({title:'Failed',details:'Failed to save visit time due to server error!'});
      });
  }

  close() {
    this.activeModal.dismiss();
  }

  /**
   * 
   * @param date 
   */
  getFullYear(date: Date): number {
    let start = new Date(date);
    return start.getFullYear();
  }
}
