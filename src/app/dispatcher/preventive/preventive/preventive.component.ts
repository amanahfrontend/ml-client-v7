import { Component, OnInit, AfterContentInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { AuthenticationService } from '../../../api-module/services/authentication/authentication.service';
import { UtilitiesService } from '../../../api-module/services/utilities/utilities.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PreventiveDetailsModalComponent } from '../preventive-details-modal/preventive-details-modal.component';
import { Page } from '../../../shared-module/shared/model/page';

@Component({
  selector: 'app-preventive',
  templateUrl: './preventive.component.html',
  styleUrls: ['./preventive.component.css'],

})
export class PreventiveComponent implements OnInit, AfterContentInit {
  orders: any;
  savedOrders: any;
  progress: boolean = false;
  rows = new Array<any>();
  totalElements: any;
  paginationFlag: boolean = false;
  searchedString: any;
  page: Page = new Page();

  constructor(private lookup: LookupService, private auth: AuthenticationService, private utilities: UtilitiesService, private modalService: NgbModal) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.getAllOrders({ offset: 0 });
  }

  setPage(pageInfo) {    
    this.page.pageNumber = pageInfo.offset;

    if (this.paginationFlag == false) {
      this.getAllOrders(this.page.pageNumber);
    }
    else {
      this.filterPreventive(this.searchedString);
    }
  }

  getAllOrders(pageNumber?: any) {
    this.progress = true;

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

    this.lookup.GetAllPreventiveMaintainencePaging(pagingData).subscribe((orders) => {
      if (orders) {

        this.rows = orders.result;
        this.totalElements = orders.totalCount;

        orders['result'].map(item => {
          if (item.customer.customerPhoneBook && item.customer.customerPhoneBook.length > 0) {
            item.customer.phone = item.customer.customerPhoneBook[0].phone;
          } else {
            item.customer.phone = '--';
          }
          item.contract.endDate = this.utilities.convertDatetoNormal(item.contract.endDate);
          item.contract.startDate = this.utilities.convertDatetoNormal(item.contract.startDate);
          item.orderDate = this.utilities.convertDatetoNormal(item.orderDate);
        });
        this.orders = orders['result'];
        this.progress = false;

      }
    }, errors => {
      this.progress = false;
    });
  }

  filterPreventive(searchTerms) {
    searchTerms.contractNumbers = [searchTerms.searchText];
    delete searchTerms.searchText;

    this.paginationFlag = true;
    if (searchTerms.contractNumbers != undefined)
      this.searchedString = searchTerms;
    else
      this.searchedString = searchTerms;

    let pageNumber = this.page.pageNumber;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost;
    objToPost = searchTerms;
    objToPost.searchKey = searchTerms.contractNumbers;
    objToPost.paginatedItemsViewModel = Object.assign({}, this.page);

    objToPost.paginatedItemsViewModel.pageNumber = this.page.pageNumber + 1;

    this.lookup.FilterPreventiveMaintainencePagingUrl(objToPost).subscribe((orders) => {
      this.orders = orders.result;
      this.totalElements = orders.totalCount;

      this.orders.map((order) => {
        order.contract.startDate = this.utilities.convertDatetoNormal(order.contract.startDate);
        order.contract.endDate = this.utilities.convertDatetoNormal(order.contract.endDate);
        order.orderDate = this.utilities.convertDatetoNormal(order.orderDate);
      })
    },
      err => {
      });
    // code.value.toDate = this.utilities.convertDatetoNormal(code.value.toDate);
    // code.value.fromDate = this.utilities.convertDatetoNormal(code.value.fromDate);
    // this.savedOrders = this.orders;
    // this.orders = this.orders.filter(sn =>
    //   sn.contract.contractNumber.toLowerCase() === code.value.searchText.toLowerCase() &&
    //   sn.orderDate > code.value.fromDate &&
    //   sn.orderDate < code.value.toDate);
  }


  preventiveDetails(data) {
    const preventiveModalRef = this.modalService.open(PreventiveDetailsModalComponent, { size: 'lg', backdrop: 'static' });
    preventiveModalRef.componentInstance.data = {
      data: data
    };
  }
}
