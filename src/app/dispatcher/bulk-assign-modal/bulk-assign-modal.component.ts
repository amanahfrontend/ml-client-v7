import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";

@Component({
  selector: 'app-bulk-assign-modal',
  templateUrl: './bulk-assign-modal.component.html',
  styleUrls: ['./bulk-assign-modal.component.css']
})
export class BulkAssignModalComponent implements OnInit {
  type: any;
  toggleLoading: boolean;
  selectedTarget: any;
  targets: any[];
  @Input() selectedOrders;
  clicked: boolean = false;

  constructor(private activeModal: NgbActiveModal, private lookup: LookupService, private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.type = undefined;
  }

  assign(value) {
    if (this.type == 'dispatcher') {
      this.clicked = true;
      let postObject = {
        fK_Dispatcher_Id: value.selectedTarget,
        fK_Order_Ids: this.selectedOrders
      };
      this.lookup.assignOrdersToDispatcher(postObject).subscribe(() => {
        this.activeModal.close(this.selectedOrders);
      },
        err => {
        })
    } else if (this.type == 'technician') {
      this.clicked = true;

      let postObject = {
        fK_Technican_Id: value.selectedTarget,
        fK_Order_Ids: this.selectedOrders
      };

      this.lookup.assignOrdersToTec(postObject).subscribe(() => {
        this.activeModal.close();
      },
        err => {
        })
    }

  }

  selectTargetType(event) {
    this.toggleLoading = true;
    if (event.target.value == 'dispatcher') {
      this.lookup.getDispatchers().subscribe((dispatchers) => {
        this.targets = dispatchers;
        this.toggleLoading = false;
      })
    } else if (event.target.value == 'technician') {
      let dispatcherId = this.authService.CurrentUser().id;
      this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
        this.targets = tecs;
        this.toggleLoading = false;
      })
    }
  }

  close() {
    this.activeModal.dismiss();
  }

}
