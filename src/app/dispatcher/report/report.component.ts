import { Component, OnInit, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { OrderFilterComponent } from "../order-filter/order-filter.component";
// import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})

export class ReportComponent implements OnInit {

  images = [];
  orders: any[] = [];
  orderProgressMode: boolean = false;

  @ViewChild(OrderFilterComponent) orderFilterComponent: OrderFilterComponent;
  @ViewChild('reportsTable') table: any;

  display: boolean = false;

  constructor(private utilities: UtilitiesService) {

  }

  ngOnInit() {    
  }

  applyFilter(filteredOrders) {    
    this.orders = filteredOrders;
    this.orders.map((order) => {
      order.createdDateView = this.utilities.convertDatetoNormal(order.order.createdDate);
      order.phoneView = order.order.customer.customerPhoneBook[0].phone
    });

    if (this.orderProgressMode) {
      this.orders.map((order) => {
        order.orderProgress && (order.orderProgress.createdDateView = this.utilities.convertDatetoNormal(order.orderProgress.createdDate));
      })
    }
  }

  // exportCsv() {
  //   let exportData = [];
  //   exportData.push({
  //     'Code': 'Code',
  //     'Created Date': 'Created Date',
  //     'Status': 'Status',
  //     'Type': 'Type',
  //     'Priority': 'Priority',
  //     'Problem': 'Problem',
  //     'Customer Name': 'Customer Name',
  //     'Customer Phone': 'Customer Phone',
  //     'Governorate': 'Governorate',
  //     'Area': 'Area',
  //     'Block': 'Block',
  //     'Street': 'Street',
  //     'Dispatcher Name': 'Dispatcher Name',
  //     'Technician Name': 'Technician Name',
  //     'Order Progress': 'Order Progress',
  //     'Progress Date': 'Progress Date',
  //     'Progress Creator': 'Progress Creator',
  //     'Progress by Tecnician': 'Progress by Tecnician'
  //   });
  //   this.orders.map((order) => {
  //     exportData.push({
  //       'Code': order.order.code,
  //       'Created Date': order.createdDateView,
  //       'Status': order.order.orderStatus.name,
  //       'Type': order.order.orderType.name,
  //       'Priority': order.order.orderPriority.name,
  //       'Problem': order.order.orderProblem.name,
  //       'Customer Name': order.order.customer.name,
  //       'Customer Phone': order.phoneView,
  //       'Governorate': order.order.location.governorate,
  //       'Area': order.order.location.area,
  //       'Block': order.order.location.block,
  //       'Street': order.order.location.street,
  //       'Dispatcher Name': order.order.dispatcherName,
  //       'Technician Name': order.order.technicanName,
  //       'Order Progress': order.orderProgress.progressStatus.name,
  //       'Progress Date': order.orderProgress.createdDateView,
  //       'Progress Creator': order.orderProgress.createdBy.userName,
  //       'Progress by Tecnician': order.orderProgress.technicanName
  //     })
  //   });
  //   let reportName = 'Report' + Date.now();
  //   return new ngxCsv(exportData, 'Report', {
  //     showLabels: true
  //     // showTitle: true
  //   });
  // }

  applyOrderProgressMode(mode) {
    this.orderProgressMode = mode;
  }

  getAllOrdersByDis() {
    // let dispatcherId = this.auth.CurrentUser().id;
    // this.lookup.getDisAllOrders(dispatcherId).subscribe((dis) => {
    //     this.orders = dis.orders;
    //     console.log(this.orders);
    //   },
    //   err => {
    //
    //   });
    this.orders = [];
  }


  /** */
  showDialog(row) {
    this.images = [];
    this.display = true;

    let array = [];
    array = row.order.orderFiles;
    array.map(item => {
      this.images.push({ source: item.fileURL, alt: '', title: '' });
    });

  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

}
