import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatcherOrderBoardComponent } from './dispatcher-order-board.component';

describe('DispatcherOrderBoardComponent', () => {
  let component: DispatcherOrderBoardComponent;
  let fixture: ComponentFixture<DispatcherOrderBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherOrderBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherOrderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
