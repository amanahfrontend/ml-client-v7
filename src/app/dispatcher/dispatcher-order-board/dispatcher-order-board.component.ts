import { Component, OnInit, OnDestroy, OnChanges, AfterViewInit, SimpleChanges } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Subscription } from "rxjs";
import { DragulaService } from 'ng2-dragula';
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BulkAssignModalComponent } from "../bulk-assign-modal/bulk-assign-modal.component";
import { EditOrderModalComponent } from "../../customer/edit-order-modal/edit-order-modal.component";
import * as signalR from "@aspnet/signalr";
import * as myGlobals from "../../api-module/services/globalPath";
import { AlertServiceService } from 'src/app/api-module/services/alertservice/alert-service.service';

@Component({
  selector: 'app-dispatcher-order-board',
  templateUrl: './dispatcher-order-board.component.html',
  styleUrls: ['./dispatcher-order-board.component.css'],
  providers: [
    DragulaService
  ]
})

export class DispatcherOrderBoardComponent implements OnInit, OnDestroy {
  toggleLoading: boolean;
  orders: any[];
  filteredOrders: any[];
  orderMap: any[];
  tecs: any[];
  cornerMessage: any[];
  selectedOrders: any[] = [];
  dispatcherOrdersSubscription: Subscription;
  tecsByDisSubscription: Subscription;
  orderStatusObservable: Subscription;
  allVehiclesSubscription: Subscription;
  isOrderSelected: any;
  orderToPost: any;
  tecToPost: any;
  toggleMap: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  lat: number = 29.378586;
  lng: number = 47.990341;
  zoom: number = 8;
  display: boolean;
  allVehicles: any[] = [];
  availabilityButtons: any[] = [];
  filterStatusClass: string[] = [];
  todayOrdersStatus: boolean;
  vehiclesStatus: boolean;
  currentActiveTab: number;
  notificationSubscription: Subscription;
  message: any;
  subscription: Subscription;
  orderhubUrl: string = "";

  private hubConnection: signalR.HubConnection

  constructor(private alertService: AlertServiceService,
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private dragulaService: DragulaService,
    private authService: AuthenticationService,
    private modelService: NgbModal) {

  }

  ngOnInit() {

    this.lookup.messageSubject.subscribe(message => {
      this.message = message;
      this.loadDispatcherOrders();
      this.deleteOrder();
    });




    this.orderhubUrl = myGlobals.BaseUrlOrder.replace("/api", "") + "orders"

    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.orderhubUrl)
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err))
    this.currentActiveTab = 1;
    this.orderMap = [];
    this.availabilityButtons = [
      { label: 'Yes', value: true },
      { label: 'No', value: false }
    ];
    this.getDispatcherOrders();
    this.getDisAllOrders();
    this.getTecsByDis();
    this.getAllVehicles();
    this.display = true;
    this.todayOrdersStatus = true;
    this.vehiclesStatus = true;
    this.utilities.toggleFullWidth = true;
    this.filterStatusClass = ['zero-filter-width'];
    this.hubInit();
    const allowDrop = false; // <= change this variable to true to allow drag


    this.dragulaService.createGroup('orders', {
      removeOnSpill: false,
      copySortSource: false,
      revertOnSpill: true,
      accepts: (item, target) => {
        return item.classList.contains('order-card') && !target.classList.contains('not-available');
      }
    });

    this.dragulaService.drop('orders').subscribe((value: any) => {

      this.postOrderByTec(value);
    });



    this.hubConnection.on('ChangeOrderStatus', (data) => {

      console.log("Change Order Status ", data);

      let SucsessMessage = "Order Code (" + data.code + ")  Status Changed to  " + data.orderStatus.name;

      this.alertService.success(SucsessMessage, false);

      ////search in about this order in unassigned orders
      let unassignedorderIndex = this.filteredOrders.findIndex(x => x.code == data.code);

      if (unassignedorderIndex > -1) {

        this.filteredOrders[unassignedorderIndex] = data;
        return;
      }

      this.tecs.forEach((tech) => {

        var untechorderIndex = tech.orders.findIndex(x => x.code == data.code);

        if (untechorderIndex > -1) {

          tech.orders[untechorderIndex] = data;
          return;
        }

      });


    });



    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
      console.log(value);
      if (value) {
        this.openViewOrderModal(value);
        let orderFoundFlag = false;
        if (this.orders) {
          this.orders.map((order, i) => {
            if (order.id == value.id) {
              orderFoundFlag = true;
            }
            if (this.orders.length == i + 1 && !orderFoundFlag) {
              this.getDispatcherOrders()
            }
          })
        } else {
          this.getDispatcherOrders();
        }
      }
    },
      err => {
        console.log(err);
      })

  }

  openViewOrderModal(notificationOrder) {
    let editOrderModalRef = this.modelService.open(EditOrderModalComponent);
    editOrderModalRef.componentInstance.data = notificationOrder;
    editOrderModalRef.componentInstance.notificationOrder = true;
  }

  ngOnDestroy() {
    this.dragulaService.destroy('orders');
    this.utilities.toggleFullWidth = false;
    this.dispatcherOrdersSubscription && this.dispatcherOrdersSubscription.unsubscribe();
    this.tecsByDisSubscription && this.tecsByDisSubscription.unsubscribe();
    this.allVehiclesSubscription && this.allVehiclesSubscription.unsubscribe();
    this.orderStatusObservable && this.orderStatusObservable.unsubscribe();
    this.utilities.setSavedNotificationText('');
    this.notificationSubscription && this.notificationSubscription.unsubscribe();

  }
  deleteOrder() {
    if (this.message != undefined) {
      if (this.message.fK_OrderStatus_Id == 5) {
        let selectedTecIndex = this.tecs.findIndex(x => x.id == this.tecToPost.id);
        let index = this.tecs[selectedTecIndex].orders.findIndex(x => x.id == this.message.id);
        this.tecs[selectedTecIndex].orders.splice(index, 1);
      }
    }


  }

  hubInit() {
    let roles = this.authService.CurrentUser().roles;
    let userId = this.authService.CurrentUser().id;
    let token = this.authService.CurrentUser().token.accessToken;
    this.hubStart();
  }

  hubStart() {

  }

  orderTransfered(id) {
    console.log(id);
    this.orders = this.orders.filter((order) => {
      return id != order.id;
    });

    this.tecs.map((tec) => {
      tec.orders = tec.orders.filter((order) => {
        return id != order.id;
      })
    });
  }

  openBulkAssignModal() {
    let modalRef = this.modelService.open(BulkAssignModalComponent);
    modalRef.componentInstance.selectedOrders = this.selectedOrders;
    modalRef.result
      .then((transferedOrders) => {

        this.getDispatcherOrders();
        this.getDisAllOrders();
        this.getTecsByDis();
        this.getAllVehicles();

        this.orders = this.orders.filter((order) => {
          return !transferedOrders.includes(order.id);
        });

        this.tecs.map((tec) => {
          tec.orders = tec.orders.filter((order) => {
            return !transferedOrders.includes(order.id);
          })
        });

        this.selectedOrders = [];
      })
      .catch(() => {

      })
  }

  addOrderToSelection(order) {
    if (!this.selectedOrders.includes(order.id)) {
      this.selectedOrders.push(order.id);
    } else {
      this.selectedOrders = this.selectedOrders.filter((singleOrderId) => {
        return order.id != singleOrderId;
      })
    }
  }

  toggleBulkAssign() {
    if (!this.isBulkAssign) {
      this.selectedOrders = [];
    }
  }

  cancelBulkAssignMode() {
    this.isBulkAssign = false;
    this.selectedOrders = [];
    this.isOrderSelected = false;
  }

  toggleActiveTab(activeTab) {
    this.currentActiveTab = activeTab;
  }

  toggleTodayOrders() {
    this.todayOrdersStatus = !this.todayOrdersStatus;
    if (this.todayOrdersStatus) {
      this.getDisAllOrders();
    } else {
      this.orderMap = [];
    }
  }

  toggleVehiclesStatus() {
    this.vehiclesStatus = !this.vehiclesStatus;
    if (this.vehiclesStatus) {
      this.getAllVehicles();
    } else {
      this.allVehicles = [];
    }
  }

  applyFilterMap(filteredOrders) {
    this.orderMap = filteredOrders;
    this.orderMap.map((order) => {
      if (order.orderStatus) {
        order.statusUrl = 'assets/Images/' + order.orderStatus.id + '.png';
      }
    });
  
    this.todayOrdersStatus = false;
  }

  private _filterOrdersListBoard(list, terms) {
    let termsCount = 0
      , filterOrders = [];

    for (let property in terms) {
      if (terms.hasOwnProperty(property)) {
        termsCount++;
      }
    }

    list.map((order) => {
      let existedTermsCount = 0;
      if (terms.orderNo) {
        if (terms.orderNo.toLowerCase() == order.code.toLowerCase()) {
          existedTermsCount++;
        }
      }
      if (terms.status) {
        terms.status.map((state) => {

          if (order.orderStatus != null && order.orderStatus != undefined) {
            if (state.id == order.orderStatus.id) {
              existedTermsCount++;
            }
          }
        })
      }
      if (terms.problems) {
        terms.problems.map((problem) => {
          if (problem.id == order.orderProblem.id) {
            existedTermsCount++;
          }
        })
      }
      if (terms.startDateFrom) {
        if (new Date(terms.startDateFrom) <= new Date(order.createdDate)) {
          existedTermsCount++;
        }
      }
      if (terms.startDateTo) {
        if (new Date(terms.startDateTo) >= new Date(order.createdDate)) {
          existedTermsCount++;
        }
      }
      if (existedTermsCount == termsCount) {
        filterOrders.push(order);
      }
    });
    return filterOrders;
  }

  applyFilterBoard(terms) {



    this.tecs.map((tec) => {
      tec.filterOrders = this._filterOrdersListBoard(tec.orders, terms);
    });

    this.filteredOrders = this._filterOrdersListBoard(this.orders, terms)
  }

  resetBoardFilter() {
    this.filteredOrders = this.orders.slice();
    this.tecs.map((tec) => {
      tec.filterOrders = tec.orders.slice();
    })
  }

  activeOrder(order) {
    if (this.toggleMap) {
      this.lat = order.location.latitude;
      this.lng = order.location.longitude;
      this.zoom = 16
    }
  }

  getAllVehicles() {
    this.allVehiclesSubscription = this.lookup.getAllVehicles().subscribe((vehicles) => {
      this.allVehicles = vehicles.returnData;
    },
      err => {
      })
  }

  toggleFilter(status) {
    if (status) {
      this.filterStatusClass = ['full-filter-width'];
    } else {
      this.filterStatusClass = ['zero-filter-width'];
    }
  }

  applyToggleMap() {
    if (this.toggleMap) {
      this.toggleOverflowX = ['hide-overflowX']
    } else {
      this.toggleOverflowX = ['show-overflowX']
    }
  }

  setActiveOrder(order) {
    // console.log('order selected');
    this.orderToPost = order;
  }

  setActiveTec(tec) {
    // console.log('tec selected');
    this.tecToPost = tec;
  }

  toggleAvailabilityClass(tec) {
    if (!tec.isAvailable) {
      return ['not-available']
    }
  }

  postTecAvailability(tec) {
    console.log(tec);
    // console.log(e);
    // e.preventDefault();
    if (!tec.orders.length) {
      let availability = {
        id: tec.id,
        isAvailable: tec.isAvailable
      };
      this.lookup.postTecAvailability(availability)
        .subscribe(() => {
        },
          err => {

          })
    } else {
      tec.isAvailable = true;
      // e.checked = true;
      // console.log(e);
    }
  }

  checkTecOrders(tec) {
    return !!tec.orders.length;
  }

  postOrderByTec(val) {

    setTimeout(() => {
      this.toggleLoading = true;

      let orderByTec = {
        fK_technican_id: this.tecToPost.id,
        fK_order_id: this.orderToPost.id,
        // isTransfer: false
      };

      if (val.el.classList.contains('single-tec-overflow') || val.target.classList.contains('single-tec-overflow')) {
        this.lookup.postOrderToTec(orderByTec).subscribe((res) => {
          //let selectedTecIndex = this.tecs.findIndex(x => x.id == orderByTec.fK_technican_id);
          //this.tecs[selectedTecIndex].orders.push(this.orderToPost);
          this.getDispatcherOrders();
          this.getTecsByDis();
          //this.toggleLoading = false;

        },
          err => {
            console.log('fail');
          })
      }
      else if (val.el.classList.contains('orders-overflow-container') || val.target.classList.contains('orders-overflow-container')) {
        this.lookup.postUnAssignedOrder(this.orderToPost.id).subscribe((res) => {
          let selectedTecIndex = this.tecs.findIndex(x => x.id == orderByTec.fK_technican_id);
          let index = this.tecs[selectedTecIndex].orders.findIndex(x => x.id == this.orderToPost.id);
          this.tecs[selectedTecIndex].orders.splice(index, 1);
          this.getTecsByDis();
          this.getDispatcherOrders();

          //this.toggleLoading = false;
        },
          err => {
          })
      }
    }, 200);

  }
  loadDispatcherOrders() {
    this.toggleLoading = true;
    let dispatcherId = this.authService.CurrentUser().id;
    this.dispatcherOrdersSubscription = this.lookup.getDispatcherOrders(dispatcherId).subscribe((dispatcher) => {
      this.orders = dispatcher.orders;
      this.orders.map((order) => {
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.filteredOrders = this.orders.slice();
      // this.orderMap = this.orders.slice();
      //console.log(this.orders);
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      })
  }


  getDispatcherOrders() {
    this.toggleLoading = true;
    let dispatcherId = this.authService.CurrentUser().id;
    this.dispatcherOrdersSubscription = this.lookup.getDispatcherOrders(dispatcherId).subscribe((dispatcher) => {
      this.orders = dispatcher.orders;
      this.orders.map((order) => {
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.filteredOrders = this.orders.slice();
      // this.orderMap = this.orders.slice();
      //console.log(this.orders);
      //this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      })
  }

  getTecsByDis() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
      this.tecs = tecs;
      //this.checkTecOrders();
      this.tecs.map((tec) => {
        tec.filterOrders = tec.orders.slice();
        tec.orders.map((order) => {
          order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
          order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
        })

      });
      if (this.toggleLoading)
        this.toggleLoading = false;
    },
      err => {

      })

  }



  getDisAllOrders() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.todayOrdersStatus = true;
    this.lat = 29.378586;
    this.lng = 47.990341;
    this.zoom = 8;

    this.lookup.getDisAllOrders(dispatcherId).subscribe((orderMap) => {

      this.orderMap = orderMap.orders;
      this.orderMap.map((order) => {
        if (order.orderStatus) {
          order.statusUrl = 'assets/Images/' + order.orderStatus.id + '.png';
        }
      });
    });
  }

}
