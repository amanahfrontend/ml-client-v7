import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
  ViewChild
} from '@angular/core';
import { Subscription } from "rxjs";
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { DateFromToComponent } from '../../shared-module/date-from-to/date-from-to.component';

@Component({
  selector: 'app-order-filter',
  templateUrl: './order-filter.component.html',
  styleUrls: ['./order-filter.component.css']
})

export class OrderFilterComponent implements OnInit, OnDestroy {
  filter: any;
  // orderMap: any[];
  allStatus: any[] = [];
  allProblems: any[] = [];
  allDispatchers: any[] = [];
  orderStatusObservable: Subscription;
  problemsObservable: Subscription;
  dispatchersObservable: Subscription;
  tecsByDisSubscription: Subscription;
  tecs: any[];
  startDateFrom: Date;
  startDateTo: Date;
  reportOrders: any[] = [];
  @Input() type: string;
  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTerms = new EventEmitter;

  @ViewChild(DateFromToComponent) child: DateFromToComponent;
  loading: boolean = false;

  constructor(private lookup: LookupService, private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.filter = {};
    // this.orderMap = [];
    this.getAllStatus();
    this.getAllProblems();
    this.getAllDispatchers();
    this.getTecsByDis();
  }

  ngOnDestroy() {
    this.orderStatusObservable && this.orderStatusObservable.unsubscribe();
    this.problemsObservable && this.problemsObservable.unsubscribe();
    this.dispatchersObservable && this.dispatchersObservable.unsubscribe();
    this.tecsByDisSubscription && this.tecsByDisSubscription.unsubscribe();
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Code': 'Code',
      'Created Date': 'Created Date',
      'Status': 'Status',
      'Type': 'Type',
      'Priority': 'Priority',
      'Problem': 'Problem',
      'Customer Name': 'Customer Name',
      'Customer Phone': 'Customer Phone',
      'Governorate': 'Governorate',
      'Area': 'Area',
      'Block': 'Block',
      'Street': 'Street',
      'Dispatcher Name': 'Dispatcher Name',
      'Technician Name': 'Technician Name',
      'Order Progress': 'Order Progress',
      'Progress Date': 'Progress Date',
      'Progress Creator': 'Progress Creator',
      'Progress by Tecnician': 'Progress by Tecnician'
    });
    this.reportOrders.map((order) => {
      exportData.push({
        'Code': this.isEmpty(order.order) ? ' ' : order.order.code,
        'Created Date': this.isEmpty(order) ? ' ' : order.createdDateView,
        'Status': this.isEmpty(order.order.orderStatus) ? ' ' : order.order.orderStatus.name,
        'Type': this.isEmpty(order.order.orderType) ? " " : order.order.orderType.name,
        'Priority': this.isEmpty(order.order.orderPriority) ? ' ' : order.order.orderPriority.name,
        'Problem': this.isEmpty(order.order.orderProblem) ? ' ' : order.order.orderProblem.name,
        'Customer Name': this.isEmpty(order.order.customer) ? ' ' : order.order.customer.name,
        'Customer Phone': this.isEmpty(order.phoneView) ? ' ' : order.phoneView,
        'Governorate': this.isEmpty(order.order.location) ? ' ' : order.order.location,
        'Area': this.isEmpty(order.order.location) ? ' ' : order.order.location.area,
        'Block': this.isEmpty(order.order.location) ? ' ' : order.order.location.block,
        'Street': this.isEmpty(order.order.location) ? ' ' : order.order.location.street,
        'Dispatcher Name': this.isEmpty(order.order) ? ' ' : order.order.dispatcherName,
        'Technician Name': this.isEmpty(order.order) ? ' ' : order.order.technicanName,
        'Order Progress': this.isEmpty(order.orderProgress) ? ' ' : this.isEmpty(order.orderProgress.progressStatus) ? ' ' : order.orderProgress.progressStatus.name,
        'Progress Date': this.isEmpty(order.orderProgress) ? ' ' : order.orderProgress.createdDateView,
        'Progress Creator': this.isEmpty(order.orderProgress) ? ' ' : this.isEmpty(order.orderProgress.createdBy) ? ' ' : order.orderProgress.createdBy.userName,
        'Progress by Tecnician': this.isEmpty(order.orderProgress) ? ' ' : order.orderProgress.technicanName,
      })
    });
    let reportName = 'Report' + Date.now();
    return new ngxCsv(exportData, reportName, {
      showLabels: true
      // showTitle: true
    });
  }

  setStartDateFrom(date) {
    this.filter.startDateFrom = date;
  }

  setStartDateTo(date) {
    this.filter.startDateTo = date;
  }

  applyFilter() {
    // let dispatcherId = this.authService.CurrentUser().id;
    let filterStatus = [];
    let filterProblems = [];
    let filterdis = [];
    let filterTecs = [];
    let startDateFrom;
    let startDateTo;

    this.filter.status && this.filter.status.map((status) => {
      filterStatus.push(status.id);
    });

    this.filter.problems && this.filter.problems.map((problem) => {
      filterProblems.push(problem.id);
    });

    this.filter.dispatchers && this.filter.dispatchers.map((dis) => {
      filterdis.push(dis.id);
    });

    this.filter.tecs && this.filter.tecs.map((tec) => {
      filterTecs.push(tec.id);
    });

    startDateFrom = this.filter.startDateFrom && this.filter.startDateFrom.toISOString();
    startDateTo = this.filter.startDateTo && this.filter.startDateTo.toISOString();

    let filterObjectToPost = {
      FK_Dispatcher_Ids: filterdis,
      FK_OrderStatus_Ids: filterStatus,
      FK_OrderProblem_Ids: filterProblems,
      FK_Technican_Ids: filterTecs,
      CreatedDateFrom: this.formatDate(startDateFrom),
      CreatedDateTo: this.formatDate(startDateTo),
      OrderCode: this.filter.orderNo,
      HasOrderProgress: this.filter.hasOrderProgress
    };

    if (this.type == 'order board') {


      this.filterTerms.emit(this.filter);
    } else {
      this.postFilterProperties(filterObjectToPost);
    }
  }

  postFilterProperties(filterObjectToPost) {
    if (this.type == 'report') {
      this.loading = true;
      this.lookup.filterOrders(filterObjectToPost).subscribe((res) => {
        this.loading = false;
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        this.reportOrders = res;
        this.filteredOrders.emit(res);
        // this.todayOrdersStatus = false;
      },
        err => {
          this.loading = false;
        });
    } else {
      this.loading = true;
      this.lookup.filterOrdersMap(filterObjectToPost).subscribe((res) => {
        this.loading = false;
        this.filteredOrders.emit(res);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        // this.todayOrdersStatus = false;
      },
        err => {
          this.loading = false;
        });
    }
  }

  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    let checkingObject = Object.assign({}, this.filter);
    delete checkingObject.hasOrderProgress;
    for (let property in checkingObject) {
      if (checkingObject.hasOwnProperty(property) && (checkingObject['' + property + ''].length || checkingObject.startDateFrom || checkingObject.startDateTo)) {
        return false;
      } else {
        return true;
      }
    }
    if (JSON.stringify(checkingObject) == JSON.stringify({})) {
      return true;
    }
  }

  applyResetFilter() {
    this.filter = {};
    this.reportOrders = [];
    this.child.resetStartDate();
    this.child.resetEndDate();

    this.resetFilter.emit();
  }

  getAllProblems() {
    this.problemsObservable = this.lookup.getAllProblems().subscribe((problems) => {
      this.allProblems = problems;
    });
  }

  getAllStatus() {
    this.orderStatusObservable = this.lookup.getOrderStatus().subscribe((status) => {
      this.allStatus = status;
    });
  }

  getAllDispatchers() {
    this.dispatchersObservable = this.lookup.getDispatchers().subscribe((dispatchers) => {
      this.allDispatchers = dispatchers;
    });

  }

  getTecsByDis() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.tecsByDisSubscription = this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
      this.tecs = tecs;
    });
  }


  /**
   * 
   * @param obj 
   */
  isEmpty(obj: object): boolean {
    for (let key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }

    return true;
  }


  formatDate(date: Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }


}
