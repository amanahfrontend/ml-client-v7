import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ReleaseModalComponent } from "../release-modal/release-modal.component";
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { Subscription } from "rxjs";
import { Message } from "primeng/components/common/message";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { SearchComponent } from "../../shared-module/search/search.component";
import { Page } from '../../shared-module/shared/model/page';
import { AlertService } from './../../services/alert.service';
import { ResetService } from './../../shared-module/date-from-to/reset.service';

@Component({
  selector: 'app-assign-items',
  templateUrl: './assign-items.component.html',
  styleUrls: ['assign-items.component.css']
})

export class AssignItemsComponent implements OnInit, OnDestroy {
  tecItems: any[];
  tecReport: any[];
  inventoryItems: any[];
  allItemsSubscription: Subscription;
  tecItemsSubscription: Subscription;
  reportSubscription: Subscription;
  tecs: any[];
  tec: any;
  searchTecObj: any = {};
  toggleLoading: boolean;
  toggleSearch: boolean;
  datesRequired: boolean;
  toggleFilterLoading: boolean;
  formSubmitted: boolean = false;
  activeTab: number;
  cornerMessage: Message[] = [];
  
  @ViewChild(SearchComponent) searchComponent: SearchComponent;

  page = new Page();
  rows = new Array<any>();

  reportPage = new Page();
  reportRows = new Array<any>();

  page1 = new Page();
  rows1 = new Array<any>();


  selectedTechId: number;
  resetDate: boolean = false;

  constructor(private lookup: LookupService,
    private modalService: NgbModal,
    private authService: AuthenticationService,
    private alertService: AlertService,
    private resetService: ResetService
  ) {
    this.reportPage.pageNumber = 0;
    this.reportPage.pageSize = 10;

    this.page.pageNumber = 0;
    this.page.pageSize = 10;


    this.page1.pageNumber = 0;
    this.page1.pageSize = 10;
  }

  ngOnInit() {
    this.tecItems = [];
    this.tecReport = [];
    this.inventoryItems = [];
    this.getAllTecs();
    this.toggleLoading = true;
    this.datesRequired = false;
    this.activeTab = 1;
  }

  ngOnDestroy() {
    this.allItemsSubscription && this.allItemsSubscription.unsubscribe();
    this.tecItemsSubscription && this.tecItemsSubscription.unsubscribe();
    this.reportSubscription && this.reportSubscription.unsubscribe();
  }

  searchItem(searchTerm) {
    this.toggleLoading = true;

    let searchObject = {
      itemId: searchTerm,
      fK_Technican_Id: this.searchTecObj.tec
    };

    this.lookup.searchAssignedItems(searchObject).subscribe((item) => {

      this.searchComponent.searchText = '';

      this.toggleLoading = false;
      if (item === true) {
        this.alertService.alertSuccess({ title: 'Item Already Assigned', details: 'This Item Already Assigned to this Technician' })
      } else if (!item) {
        this.alertService.alertError({ title: 'Item Not Found', details: 'This Item Not found in inventory' })
      } else if (item.id) {
        this.tecItems.length == 0 ? this.page1.totalElements = 1 : this.page1.totalElements;

        this.tecItems.unshift({
          id: item.id,
          totalUsed: 0,
          totalAmount: 0,
          currentAmount: 0,
          item: {
            name: item.name,
            description: item.description
          },
          isNew: true
        });

      }
    },
      err => {
        this.toggleLoading = false;
      })
  }

  printReport(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("report-print");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  createQRcode(item): void {
    let printContents, popupWin;
    printContents = document.getElementById(`${item.id}`).innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  ConvertToString(item) {
    return String(item.id);
  }

  assignStartDate(date) {
    this.searchTecObj.startDate = date;
  }

  assignEndDate(date) {
    this.searchTecObj.endDate = date;
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Order No': 'Order No',
      'Contract No': 'Contract No',
      'Customer Name': 'Customer Name',
      'Customer Mobile': 'Customer Mobile',
      'Governarate': 'Governarate',
      'Area': 'Area',
      'Item Name': 'Item Name',
      'Item Description': 'Item Description',
      'Quantity': 'Quantity',
    });
    this.tecReport.map((item) => {
      exportData.push({
        'Order No': item.order.code,
        'Contract No': item.order.contract.contractNumber,
        'Customer Name': item.order.customer.name,
        'Customer Mobile': item.order.customer.name,
        'Governarate': item.order.location.governorate,
        'Area': item.order.location.area,
        'Item Name': item.item.name,
        'Item Description': item.item.description,
        'Quantity': item.amount,
      })
    });
    return new ngxCsv(exportData, 'Used Items Report', {
      showLabels: true
    });
  }


  Reset() {

    this.searchTecObj = {};
    this.setActiveTab(1);
    this.toggleSearch = false;
  }



  setActiveTab(tab) {
    this.activeTab = tab;
    if (this.tec && this.activeTab == 1) {
      this.getAssignedItems(this.tec, this.page1.pageNumber);
    } else if (this.tec && this.activeTab == 2) {
      this.selectedTechId = this.tec;
      this.getReport();
    } else if (this.activeTab == 3) {
      this.getAllItems(this.page.pageNumber);
    }
  }

  submitToSearch(value) {
    this.toggleFilterLoading = true;
    this.page1.pageNumber = 0;


    if (this.activeTab == 1) {
      this.getAssignedItems(value.tec, this.page1.pageNumber);
      // this.toggleSearch = true;
    } else if (this.activeTab == 2) {
      this.selectedTechId = value.tec;
      this.getReport();
    }
  }

  releaseItems(item) {
    let releaseModalRef = this.modalService.open(ReleaseModalComponent, { backdrop: 'static' });
    releaseModalRef.componentInstance.data = {
      fK_Technican_Id: this.searchTecObj.tec,
      itemId: item.itemId || item.id
    };
    releaseModalRef.result
      .then((res) => {
        // item = res;
        // item.currentAmount = res.currentAmount;
        // item.totalAmount = res.totalAmount;
        // item.totalUsed = res.totalUsed;
        this.getAssignedItems(this.searchTecObj.tec, this.page1.pageNumber);
      })
      .catch(() => {
      })
  }

  /*API calls*/

  getAllTecs() {
    let dispatcherId = this.authService.CurrentUser().id;
    if (this.authService.CurrentUser().userName == 'dispatcher') {
      this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
        this.toggleLoading = false;
        this.tecs = tecs;
      },
        err => {
          this.toggleLoading = false;
        })
    }
    else {
      this.lookup.getAllTecs().subscribe(tecs => {
        this.toggleLoading = false;
        this.tecs = tecs;
      },
        err => {
          this.toggleLoading = false;
        })
    }

  }

  getAllItems(pageNumber: number) {

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;

    this.allItemsSubscription = this.lookup.getAllItems(objToPost).subscribe((items) => {
      this.rows = items.result;
      this.page.totalElements = items.totalCount;
      this.inventoryItems = items.result;
    },
      err => {

      })
  }

  //getAssignedItems(id) {
  //   this.toggleLoading = true;
  //  this.toggleSearch = false;
  //  this.tecItemsSubscription = this.lookup.getTecAssignedItems(id).subscribe((items) => {
  //    this.tecItems = items;
  //    this.toggleLoading = false;
  //    this.toggleSearch = true;
  //    this.toggleFilterLoading = false;
  //  },
  //    err => {
  //      this.toggleSearch = true;
  //      this.toggleLoading = false;
  //      this.toggleFilterLoading = false;
  //    })
  //}




  getAssignedItems(id, pageNumber: number) {
    // this.toggleLoading = true;

    if (pageNumber > 0)
      this.page1.pageNumber = pageNumber;
    else
      this.page1.pageNumber = 0;

    let objToPost = Object.assign({}, this.page1);
    objToPost.pageNumber = this.page1.pageNumber + 1;
    objToPost.searchBy = id;
    this.allItemsSubscription = this.lookup.getTecAssignedItemsPagginated(objToPost).subscribe((items) => {


      this.tecItems = items.result;
      this.page1.totalElements = items.totalCount;
      this.rows1 = items.result;

      this.toggleLoading = false;
      this.toggleSearch = true;
      this.toggleFilterLoading = false;
    });
  }

  getReport(pageNumber?: number) {

    if (pageNumber > 0)
      this.reportPage.pageNumber = pageNumber;
    else
      this.reportPage.pageNumber = 0;

    let tecId = [this.selectedTechId];

    let filterObj = {
      fK_Technican_Ids: tecId,
      dateFrom: this.searchTecObj.startDate && this.searchTecObj.startDate.toISOString(),
      dateTo: this.searchTecObj.endDate && this.searchTecObj.endDate.toISOString(),
      pagingParameterModel: {
        pageNumber: this.reportPage.pageNumber + 1,
      }
    };

    this.reportSubscription = this.lookup.filterUsedItems(filterObj).subscribe((tecReport) => {
      this.rows = tecReport.result;
      this.page.totalElements = tecReport.totalCount;
      this.tecReport = tecReport.result;
      this.toggleFilterLoading = false;
    },
      err => {
        this.toggleFilterLoading = false;
      })
  }

  reset() {
    this.searchTecObj.tec = '';
    this.resetService.changeStatus(true);
  }
}
