import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignItemsComponent } from './assign-items/assign-items.component';
import { Routes, RouterModule } from "@angular/router";
import { ProgressSpinnerModule } from 'primeng/components/progressspinner/progressspinner';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReleaseModalComponent } from './release-modal/release-modal.component';
import { SharedModuleModule } from "../shared-module/shared-module.module";
import { QRCodeModule } from 'angular2-qrcode';
import { GrowlModule } from 'primeng/components/growl/growl';
import { RoleGuard } from "../api-module/guards/role.guard";

const assignItemsRoutes: Routes = [
  {
    path: '',
    component: AssignItemsComponent,
    canActivate: [RoleGuard],
    data: {
      roles: ['Dispatcher', 'Admin', 'SupervisorDispatcher']
    }
  }
];

@NgModule({
  imports: [
    GrowlModule,
    CommonModule,
    FormsModule,
    SharedModuleModule,
    NgxDatatableModule,
    ProgressSpinnerModule,
    QRCodeModule,
    RouterModule.forChild(assignItemsRoutes)
  ],
  entryComponents: [
    ReleaseModalComponent
  ],
  declarations: [AssignItemsComponent, ReleaseModalComponent],
  providers: [
    RoleGuard
  ]
})
export class AssignItemsModule {
}
