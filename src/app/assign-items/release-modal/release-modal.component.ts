import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-release-modal',
  templateUrl: './release-modal.component.html',
  styleUrls: ['./release-modal.component.css']
})

export class ReleaseModalComponent implements OnInit {
  @Input() data;
  toggleLoading: boolean;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
  }

  confirm(value) {
    console.log('value',value);

    // this.lookup.assignItemToTec(value).subscribe((res) => {
    //     this.activeModal.close(res);
    //   });
  }

  close() {
    this.activeModal.dismiss();
  }


}
