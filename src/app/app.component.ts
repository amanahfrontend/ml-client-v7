import { Component, OnInit, DoCheck, Inject, ElementRef, HostListener, HostBinding } from '@angular/core';
import { LookupService } from './api-module/services/lookup-services/lookup.service';
import { AuthenticationService } from './api-module/services/authentication/authentication.service';
import { DOCUMENT } from '@angular/platform-browser';
import { PlatformLocation } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { menu, Item } from './constants/menu';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})



export class AppComponent implements OnInit, DoCheck {


  title = 'app';
  menuItemToggle: boolean;
  roles: string[];
  template: string = `<img src="http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif" />`
  isOpen: boolean = false;

  items: Item[];

  constructor(
    private lookupService: LookupService,
    private authService: AuthenticationService,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    location: PlatformLocation,
    private modalService: NgbModal,
  ) {
    location.onPopState(() => this.closeModals());

    this.lookupService.getWebsiteSettingsBykey().subscribe(settings => {
      let array = [];

      array = settings;
      array.map(item => {
        if (item.key === 'image') {
          this._document.getElementById('appFavicon').setAttribute('href', item.value);
        }
      })
    });
  }

  ngDoCheck() {
    this.menuItemToggle = this.authService.CurrentUser();
  }

  ngOnInit() {
    this.roles = [];
    this.authService.userRoles.subscribe((roles) => {
      this.roles = roles || [];
    });

    this.items = menu();
  }

  /**
   * 
   * @param source 
   * @param target 
   */
  containsAny(target) {
    let result = this.roles.filter((item) => {
      return target.indexOf(item) > -1;
    });
    return (result.length > 0);
  }

  closeModals() {
    this.modalService.dismissAll();
  }


  @HostListener('click', ['$event']) click(e) {
    e.stopPropagation();
  }

  @HostListener("document:click") resetToggle() {
    this._document.getElementById("mySidenav").style.width = "0";
    this._document.getElementById("main").style.marginRight = "0";
  }

  /**
   * 
   * @param e 
   */
  toggle(e) {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      this._document.getElementById("mySidenav").style.width = "250px";
      this._document.getElementById("main").style.marginRight = "250px";
    } else {
      this._document.getElementById("mySidenav").style.width = "0";
      this._document.getElementById("main").style.marginRight = "0";
    }
  }

  reset(event) {
    this._document.getElementById("mySidenav").style.width = "0";
    this._document.getElementById("main").style.marginRight = "0";
  }
}