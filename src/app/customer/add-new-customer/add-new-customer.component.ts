import { Router, ActivatedRoute } from '@angular/router';
// import {newCallDetails} from './../../api-module/models/newCustomer';
// import {Observable} from 'rxjs';
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
// import {allCustomerLookup} from './../../api-module/models/lookups-modal';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { NgForm } from '@angular/forms';
import { DataService } from '../../admin/settings/data.service';


@Component({
  selector: 'app-add-new-customer',
  templateUrl: './add-new-customer.component.html',
  styleUrls: ['./add-new-customer.component.css']
})

export class AddNewCustomerComponent implements OnInit, OnDestroy {
  public newCall: any;
  public firstSubmitTry = false;
  public firstStep = false;
  existedCall: any;
  public selectedTab = 'tab1';
  public prevTab = '';
  files: File;
  base64textString: string;

  // --------- Lookups ---------------
  public callTypes: Subscription;
  public priorities;
  public phoneTypes;
  public customers: any[] = [];

  public ContractType: Subscription;
  public Locations: Subscription;
  public Governorates: Subscription;
  public existedCustomerSubscription: Subscription;
  public areas;
  public blocks;
  public streets;
  public AddedNewLocation: any = {};
  public TotalPhone = [1];
  public totalPhonesData = [];
  model: any = {};

  // ---------------- Location -----------------
  public newLocation: any = {};
  public AddNewLocation: boolean = false;
  public PACI = '';
  public showG: boolean = true;
  public showB: boolean = true;
  public showA: boolean = true;
  public showS: boolean = true;
  lockForm: boolean;
  toggleLocationLoading: boolean;
  isFormSubmitted: boolean = false;

  @Input() alreadyExisted;
  convertIntoTextInputs: boolean;
  governments;

  validAddress: boolean;
  paciStatus: boolean;
  isSaving: boolean = false;
  @ViewChild('form') addressForm: NgForm;

  viewValidation: boolean;
  values;

  governorates: any[];
  newCustomer: any;
  machineTypes: any[] = [];
  toggleLoading: boolean;

  clicked: boolean = false;

  constructor(private router: Router,
    private customerService: CustomerCrudService,
    private lookupService: LookupService,
    private alertService: AlertServiceService,
    private utilities: UtilitiesService,
    private auth: AuthenticationService,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    if (this.activatedRoute.snapshot.queryParams['customerId']) {
      this.getCustomerDetails(this.activatedRoute.snapshot.queryParams['customerId']);
    }


    !this.alreadyExisted && this.utilities.updateCurrentExistedCustomer({});
    this.existedCustomerSubscription = this.utilities.existedCustomer
      .subscribe(
      (val) => {
          this.existedCall = val;
          if (JSON.stringify(this.existedCall) == JSON.stringify({})) {
            this.newCall = {};
            this.lockForm = false;
            this.showG = false;
            this.showA = true;
            this.showB = true;
            this.showS = true;

          } else {
            this.newCall = this.existedCall;
            this.showG = true;
            this.showA = true;
            this.showB = true;
            this.showS = true;
            this.lockForm = true;
          }
        }
      );

    // ----------------- Call all the Lookups ---------------------

    this.lookupService.getCallsTypes()
      .subscribe(
        types => {

          let res = types.filter(this.ignoreType);

          this.callTypes = res;

        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );

    this.lookupService.getPriorities()
      .subscribe(
        (priorities) => {
          this.priorities = priorities;
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );

    this.lookupService.getPhoneTypes()
      .subscribe(
        (phoneTypes) => {
          this.phoneTypes = phoneTypes;
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );

    this.lookupService.getAllCustomers()
      .subscribe(
        (customers) => {
          this.customers = JSON.parse(JSON.stringify(customers));
        }, err => {

          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );

    this.lookupService.getContractTypes()
      .subscribe(
        contractTypes => {
          this.ContractType = contractTypes;
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );

    this.getAllGovs();


  }

  ignoreType(value) {
    if (value.name != "Existing Client" && value.name != "New Client") {
      return value;
    }
  }

  ngOnDestroy() {
    this.existedCustomerSubscription.unsubscribe();


  }

  editCustomer() {
    this.lockForm = false;
  }

  getFiles(event) {
    this.files = event.target.files[0];
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.files);

  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
  }

  getAllGovs() {

    this.lookupService.GetallGovernorates().subscribe(Governorates => {
      this.convertIntoTextInputs = false;

      if (Governorates[0].name == 'Not Found In Paci') {
        this.getAllGovs();
        this.paciStatus = true;
      } else {
        this.Governorates = Governorates;
      }
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.convertIntoTextInputs = true;
        this.paciStatus = false;
        this.getAllGovernments();
      });
  }

  CallArea() {
    this.AddedNewLocation.governorate = this.getSelectedName(this.Governorates, this.newCall.governorate);

    this.lookupService.GetallAreas(this.AddedNewLocation.governorate).subscribe(areas => {
      if (areas[0].name == 'Not Found In Paci') {
        this.CallArea();
      } else {
        this.areas = areas;
        // if (areas != null) {
        this.showA = false;
        // }
      }


      //console.log(areas);
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();


        this.alertService.error('we have Server Error !')
      });
  }

  routeToLogs() {
    this.router.navigate(['/search/calls-history/logs', this.newCall.id])
  }

  routeToCreateEstimations() {
    this.router.navigate(['/search/calls-history/estimation', this.newCall.id])
  }

  CallBlock() {
    this.AddedNewLocation.area = this.getSelectedName(this.areas, this.newCall.area);
    this.lookupService.GetallBlocks(this.AddedNewLocation.area)
      .subscribe(
        blocks => {
          if (blocks[0].name == 'Not Found In Paci') {

            this.CallBlock();
          } else {

            this.blocks = blocks;
            this.showB = false;
          }
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );
  }

  CallStreet() {
    this.AddedNewLocation.block = this.newCall.block;
    this.lookupService.GetallStreets(this.AddedNewLocation.area, this.AddedNewLocation.block, this.AddedNewLocation.governorate)
      .subscribe(
        streets => {
          if (streets[0].name == 'Not Found In Paci') {

            this.CallStreet();
          } else {
            this.streets = streets;
            this.showS = false;
          }
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('we have Server Error !')
        }
      );
  }

  CallGetStreat() {
    this.AddedNewLocation.street = this.getSelectedName(this.streets, this.newLocation.street);
  }

  getSelectedName(list, name) {
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].name == name) {
          return list[i].id;
        }
      }
    }
  }

  checktoSearch(event) {
    if (this.newCall.paciNumber && this.newCall.paciNumber.length > 7) {
      this.GetLocationByPaci();
    }
    else {
      this.newLocation = {};
    }
  }

  GetLocationByPaci() {
    this.toggleLocationLoading = true;
    this.lookupService.GetLocationByPaci(+this.newCall.paciNumber)
      .subscribe(
        loc => {

          if (loc != null) {
            this.newCall.governorate = loc['governorate'].name;
            this.showG = false;
            this.newCall.area = loc['area'].name;
            this.areas = [];
            this.areas.push(loc['area']);
            this.newCall.block = loc['block'].name;
            this.blocks = [];
            this.blocks.push(loc['block']);
            this.newCall.street = loc['street'].name;
            this.streets = [];
            this.streets.push(loc['street']);
          }
          this.toggleLocationLoading = false;
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.toggleLocationLoading = false;
          this.alertService.error('we have Server Error !')
        }
      );
  }

  createNewLocation() {
    this.CallGetStreat();
    if (this.AddedNewLocation.paciNumber != "") {
      this.AddedNewLocation.paciNumber = this.PACI;
    }
    this.AddNewLocation = false;
  }

  // reloadLocation() {
  //   this.lookupService.GetallLocations()
  //     .subscribe(
  //       locations => {
  //         this.Locations = locations;
  //       }, err => {
  //         this.alertService.error('we have Server Error !')
  //       }
  //     );
  // }

  ResetallLocation() {
    this.newLocation = {};
    this.newLocation.Government = {};
    this.AddedNewLocation = {};
    this.PACI = '';
    this.showG = false;
    this.showB = true;
    this.showA = true;
    this.showS = true;
    this.AddNewLocation = false;
  }

  onSubmit(form) {
    if (form.valid) {
      this.AddnewCustomer();
    } else {
      this.firstSubmitTry = true;
    }
  }

  AddnewCustomer() {
    this.customerService.addCustomer(this.newCall)
      .subscribe(
        users => {
          this.alertService.success('user added Successfully');
          this.ResetallLocation();
          this.router.navigate(['search/customer/' + users['customer'].id]);
        }, err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.alertService.error('Error !!')
        }
      );
  }

  /**
   * 
   * @param values 
   */
  postNewCall(values) {

    window.scroll(0, 0);

    this.isSaving = true;
    if (this.newCall.isValid) {
      this.clicked = true;
      this.ContinueAdd(values);
    }
  }


  ContinueAdd(values) {
    this.isSaving = true;

    values.customerServiceName = this.auth.CurrentUser().fullName;
    values.customerServiceUserName = this.auth.CurrentUser().userName;
    values.area = this.newCall.area;
    values.street = this.newCall.street;
    values.block = this.newCall.block;
    values.fK_CallPriority_Id = this.newCall.fK_CallPriority_Id;
    values.fK_PhoneType_Id = this.newCall.fK_PhoneType_Id;
    values.fK_Customer_Id = this.newCall.fK_Customer_Id;
    values.customerDescription = this.newCall.customerDescription;

    values.governorate = this.newCall.governorate;
    values.building = this.newCall.building;
    values.houseNo = this.newCall.houseNo;
    values.addressNote = this.newCall.addressNote;
    values.latitude = this.newCall.latitude;
    values.longitude = this.newCall.longitude;
    values.appartmentNo = this.newCall.appartmentNo;
    values.floorNo = this.newCall.floorNo;
    values.paciNumber = this.newCall.paciNumber;
    values.title = this.newCall.title;


    this.lookupService.postNewCall(values)
      .subscribe(
        (res) => {
          this.clicked = false;
          this.alertService.success('Success!');

          this.router.navigate(['search/calls-history']);

          this.isSaving = false;
        }, err => {
          this.validAddress = true;
          // this.alertService.error('we have Server Error !');
          this.isSaving = false;

        }
      );

  }

  AddPhoneItem() {
    this.TotalPhone.push(1);
    this.newCall.CallerPhones = this.totalPhonesData;
  }

  DeletePhoneItem(index) {
    this.TotalPhone.splice(index, 1);
    this.newCall.CallerPhones.splice(index, 1);
    this.totalPhonesData = this.newCall.CallerPhones;
    this.newCall.CallerPhones = [];
    this.newCall.CallerPhones = this.totalPhonesData;
  }

  CustomerSelected() {
    let selectedcustomer = this.customers.find(x => x.id == this.newCall.FK_Customer_Id, 0);
    this.newCall.fK_CallPriority_Id = selectedcustomer.fK_CustomerType_Id;
  }

  onLocationChange(newLocation) {
    this.isSaving = false;

    this.viewValidation = false;
    this.clicked = false;

    this.newCall.paciNumber = newLocation.paciNumber;
    if (newLocation.governorate == 'undefined') this.newCall.governorate = undefined;
    else this.newCall.governorate = newLocation.governorate;

    if (newLocation.area == 'undefined') this.newCall.area = undefined;
    else this.newCall.area = newLocation.area;

    if (newLocation.block == 'undefined') this.newCall.block = undefined;
    else this.newCall.block = newLocation.block;

    if (newLocation.street == 'undefined') this.newCall.street = undefined;
    else this.newCall.street = newLocation.street;

    if (!newLocation.title) this.newCall.title = undefined;
    else this.newCall.title = newLocation.title;

    this.newCall.latitude = newLocation.latitude;
    this.newCall.longitude = newLocation.longitude;


    if (newLocation.building == 'undefined') this.newCall.building = undefined;
    else this.newCall.building = newLocation.building;

    if (newLocation.appartmentNo == 'undefined') this.newCall.appartmentNo = 0;
    else this.newCall.appartmentNo = newLocation.appartmentNo;


    if (newLocation.floorNo == 'undefined') this.newCall.floorNo = 0;
    else this.newCall.floorNo = newLocation.floorNo;


    if (newLocation.houseNo == 'undefined') this.newCall.houseNo = 0;
    else this.newCall.houseNo = newLocation.houseNo;

    if (newLocation.addressNote == 'undefined') this.newCall.addressNote = '';
    else this.newCall.addressNote = newLocation.addressNote;


    //this.newCall.appartmentNo = newLocation.appartmentNo;
    //this.newCall.floorNo = newLocation.floorNo;
    //this.newCall.houseNo = newLocation.houseNo;
    //this.newCall.addressNote = newLocation.addressNote;
  }


  /**
   * list all governments
   * 
   * 
   */
  getAllGovernments() {
    this.lookupService.getAllGovernorateswithoutPaci().subscribe(res => {
      this.Governorates = res;
    });
  }

  /**
   * areas by government
   * 
   * 
   * @param govId 
   */
  getAreaswithoutPaci(govId: number) {
    this.lookupService.getAreaswithoutPaci(govId).subscribe(res => {
      this.areas = res;
    });
  }


  /**
   * customer details
   * 
   * 
   * @param customerId 
   */
  getCustomerDetails(customerId) {
    this.lookupService.getCustomerDetails(customerId).subscribe((existedCustomerData) => {

      this.newCustomer = existedCustomerData;



      // basic data
      this.newCall.callerName = existedCustomerData['name'];
      this.newCall.civilId = existedCustomerData['civilId'];
      this.newCall.fK_Customer_Id = customerId;
      this.newCall.customerType = existedCustomerData['fK_CallType_Id'];
      this.newCall.callerNumber = existedCustomerData['customerPhoneBook'][0].phone;
      this.newCall.fK_PhoneType_Id = existedCustomerData['customerPhoneBook'][0].fK_PhoneType_Id;
      this.newCall.customerDescription = existedCustomerData['remarks'];

      // address
      this.newCall.area = existedCustomerData['locations'][0].area;
      this.newCall.street = existedCustomerData['locations'][0].street;
      this.newCall.block = existedCustomerData['locations'][0].block;
      this.newCall.fK_CallPriority_Id = existedCustomerData['fK_CallPriority_Id'];
      this.newCall.governorate = existedCustomerData['location'][0].governorate;
      this.newCall.building = existedCustomerData['locations'][0].building;
      this.newCall.houseNo = existedCustomerData['locations'][0].houseNo;
      this.newCall.addressNote = existedCustomerData['locations'][0].addressNote;
      this.newCall.latitude = existedCustomerData['locations'][0].latitude;
      this.newCall.longitude = existedCustomerData['locations'][0].longitude;
      this.newCall.appartmentNo = existedCustomerData['locations'][0].appartmentNo;
      this.newCall.floorNo = existedCustomerData['locations'][0].floorNo;
      this.newCall.paciNumber = existedCustomerData['locations'][0].paciNumber;
      this.newCall.title = existedCustomerData['locations'][0].title;
      this.newCall.isValid = true;
      //this.getMultipleLocations(0, this.newCall);

    },
      err => {
      })
  }

  /**
   * 
   * @param i 
   * @param locations 
   */
  //getMultipleLocations(i, locations) {
  //  this.toggleLoading = true;
  //  this.getGovernorates().then(() => {
  //    this.getAreas(locations).then(() => {
  //      this.getBlocks(locations).then(() => {
  //        this.getStreets(locations).then(() => {

  //          this.toggleLoading = false;
  //          // this.getMultipleLocations(++i, locations)
  //        })
  //      })
  //    })
  //  });

  //}

  getAreas(singleLocation) {
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.governorate, this.Governorates);
      var lookup = this.lookupService;
      (function internalRecursive() {
        lookup.GetallAreas(id).subscribe((areas) => {


          if (areas[0].name == 'Not Found In Paci') {
            internalRecursive();
          } else {
            singleLocation.areas = areas;
            resolve();
          }
        })
      }());
    })
  }

  getMachineTypes() {
    this.lookupService.getAllMachineTypes()
      .subscribe(
        res => {
          this.machineTypes = res;
        }, err => {
        }
      );
  }

  getBlocks(singleLocation) {
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.area, singleLocation.areas);
      var lookUp = this.lookupService;
      (function internalRecursive() {
        lookUp.GetallBlocks(id)
          .subscribe(
            (blocks) => {
              if (blocks[0].name == 'Not Found In Paci') {
                internalRecursive();
              } else {
                singleLocation.blocks = blocks;
                resolve();
              }
            },
            err => {

            })
      })();
    });
  }

  getStreets(singleLocation) {
    return new Promise((resolve, reject) => {
      let govId = this.selectId(singleLocation.governorate, this.Governorates);
      let areaId = this.selectId(singleLocation.area, singleLocation.areas);
      var lookUp = this.lookupService;
      (function internalRecursive() {
        lookUp.GetallStreets(areaId, singleLocation.block, govId)
          .subscribe(
            (streets) => {
              // this.streets = streets;
              if (streets[0].name == 'Not Found In Paci') {
                internalRecursive();
              } else {
                singleLocation.streets = streets;
                resolve();
              }
            },
            err => {
            }
          );
      })();
    });
  }

  selectId(name, list) {
    if (name != undefined) {
      return list.filter((item) => {
        return item.name == name;
      })[0].id;
    }
  }

  getGovernorates() {
    return new Promise((resolve, reject) => {
      this.lookupService.GetallGovernorates()
        .subscribe(
          (govs) => {
            if (govs[0].name == 'Not Found In Paci') {
              this.convertIntoTextInputs = true;
              this.getGovernorates();
            } else {
              this.Governorates = govs;
              this.paciStatus = true;
              this.convertIntoTextInputs = false;
              resolve();
            }
          },
          err => {
            this.getAllGovernments();
            this.paciStatus = false;
            this.convertIntoTextInputs = true;
          });
    });
  }

}
