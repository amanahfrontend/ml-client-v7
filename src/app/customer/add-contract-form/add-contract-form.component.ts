import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import * as moment from 'moment';
import { Subscription } from "rxjs";
import { Router } from "@angular/router";
import { Message } from "primeng/components/common/message";
import { CustomerService } from './../customer-list/customer.service';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-add-contract-form',
  templateUrl: './add-contract-form.component.html',
  styleUrls: ['./add-contract-form.component.css']
})

export class AddContractFormComponent implements OnInit, OnDestroy {
  newContract: any;
  // newCustomer: any;
  contractTypes: any[];
  orderTypes: any[];
  orderPriorities: any[];
  // customerTypes: any[];
  problems: any[];
  // governorates: any[];
  // areas: any[];
  // streets: any[];
  // customer: any;

  @Input() quotationDetails: any;
  @Input() type: string;

  @Output() closedForm = new EventEmitter();
  @Output() dismissForm = new EventEmitter();

  postNewContractSubscription: Subscription;
  subscribtionContractTypes: Subscription;
  toggleLoading: boolean;
  toggleLoadingContractSection: boolean;
  dateConvertedFlag: boolean;
  isNewCustomer: boolean;
  customerId: any;
  customers: any[];
  // phoneTypes: any[];
  // blocks: any[];
  orderPrioritySubscription: Subscription;
  orderTypesSubscription: Subscription;
  cornerMessageContract: Message[] = [];
  uploadedFiles: any[] = [];
  contractFiles: any[] = [];
  toggleUploadButtons: boolean;

  todayDate: Date = new Date();
  // itemsError: Message[];
  selectedCustomer: any = {};
  isCustomerDetailsRoute: boolean = false;

  constructor(
    private lookup: LookupService,
    private router: Router,
    private utilities: UtilitiesService,
    private customerService: CustomerService
  ) {
  }

  ngOnInit() {
    this.customerService.currentCustomer.subscribe(customer => {
      if (customer !== 'default') {
        const parsed = JSON.parse(customer);
        this.isCustomerDetailsRoute = true;
        this.setCustomerId(parsed);
      }
    });

    this.todayDate = new Date();

    this.newContract = {};
    this.dateConvertedFlag = false;
    this.isNewCustomer = false;
    this.toggleUploadButtons = false;

    this.subscribtionContractTypes = this.lookup.getContractTypes().subscribe((contractTypes) => {
      this.contractTypes = contractTypes;
    });

    this.getOrderPriorities();
    this.getOrderTypes();
    this.getProblems();
    this.getCustomers();
  }

  showAtView(event) {
    this.uploadedFiles = [];
    this.contractFiles = [];
    for (let i = 0, l = event.files.length; i < l; i++) {
      this.uploadedFiles.push(event.files[i]);

      let reader = new FileReader();
      let base64file;
      let file = event.files[i];

      reader.onload = (readerEvt) => {

        let binaryStringFileReader = <FileReader>readerEvt.target;
        let binaryString = binaryStringFileReader.result;
        base64file = btoa(binaryString.toString());

        /*********** collect the json object *************/
        let jsonObject = {
          "fileName": file.name,
          "fileContent": base64file
        };
        this.contractFiles.push(jsonObject);
      };

      /* Not sure about the importance of this libe :( */
      let x = reader.readAsBinaryString(event.files[i]);
    }
    // console.log(contractFiles);
  }

  ngOnDestroy() {
    this.subscribtionContractTypes.unsubscribe();
    this.postNewContractSubscription && this.postNewContractSubscription.unsubscribe();
    this.orderPrioritySubscription && this.orderPrioritySubscription.unsubscribe();
    this.orderTypesSubscription && this.orderTypesSubscription.unsubscribe();
  }

  checkChoosedCUstomer() {

    if (this.customerId == 'undefined') {
      this.customerId = undefined;
    }
  }

  setCustomerId(newCustomer) {
    this.selectedCustomer = newCustomer;
    this.customerId = newCustomer.id;
    //this.customers.push(newCustomer);
    this.isNewCustomer = false;
  }

  getCustomers() {
    this.lookup.getAllCustomers().subscribe((customers) => {
      this.customers = customers;
    })
  }

  toggleNewCustomerForm() {
    this.isNewCustomer = !this.isNewCustomer;
  }

  getProblems() {
    this.lookup.getAllProblems().subscribe((problems) => {
      this.problems = problems;
    },
      err => {

      })
  }

  getOrderPriorities() {
    this.orderPrioritySubscription = this.lookup.getOrderPriority().subscribe((orderPriorities) => {
      this.orderPriorities = orderPriorities;
    },
      err => {

      })
  }

  getOrderTypes() {
    this.orderTypesSubscription = this.lookup.getOrderType().subscribe((orderTypes) => {
      this.orderTypes = orderTypes;
    },
      err => {

      })
  }

  // closeFormNow() {
  //   this.closedForm.emit();
  // }

  dismissFormNow() {
    this.dismissForm.emit();
  }

  remove(e) {
    this.contractFiles = this.contractFiles.filter((file) => {
      return e.file.name !== file.fileName;
    })
  }

  generateContract(contractValues) {
    this.toggleLoadingContractSection = true;

    contractValues.startDate = !this.dateConvertedFlag && contractValues.startDate.toISOString();
    contractValues.endDate = !this.dateConvertedFlag && contractValues.endDate.toISOString();

    if (this.quotationDetails) {
      contractValues.contractQuotations = [{
        quotationRefNumber: this.quotationDetails.refNumber
      }];
      contractValues.price = this.quotationDetails.price;
    } else {
      contractValues.fK_Customer_Id = this.customerId;
    }

    contractValues.contractFiles = this.contractFiles;
    this.dateConvertedFlag = true;
    this.postNewContractSubscription = this.lookup.postNewContract(contractValues).subscribe(() => {

      this.closedForm.emit();
      this.utilities.currentSearch = {};

      this.cornerMessageContract.push({
        severity: 'success',
        summary: 'Generated Successfully!',
        detail: 'Contract generated successfully!'
      });

      this.toggleLoadingContractSection = false;
      setTimeout(() => {
        this.router.navigate(['search/contract'])
      }, 2000);
    },
      err => {

        if (err.status == 400) {

          this.cornerMessageContract.push({
            severity: 'error',
            summary: 'Failed!',
            detail: err._body,
          });

        }
        else {
          //console.log(JSON.stringify(err));
          this.utilities.currentSearch = {};
          this.cornerMessageContract.push({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Failed to generate Contract due to network error, please try again later.'
          });
        }
        this.toggleLoadingContractSection = false;


      })
  }


  /**
   * 
   * @param event 
   */
  onCustomerChange(event) {
    this.selectedCustomer = event;
    this.customerId = event.id;
  }


  /**
   * validate end date (end must be greater than start)
   * 
   * 
   * @param endDate 
   */
  validateEndDate(endDate) {
    this.cornerMessageContract = [];

    const status = this.compareTwoDates(this.newContract.startDate, endDate);

    switch (status) {
      case false:
        this.cornerMessageContract.push({
          severity: 'error',
          summary: 'Select other day!',
          detail: 'End date must be greater than start date!'
        });
        this.newContract.endDate = '';
        break;
      default:
        break;
    }
  }


  /**
   * reset end date if start changed by user to validate it again
   * 
   * 
   * @param event 
   */
  resetEndDate(event) {
    this.newContract.endDate = null;
  }

  /**
   * 
   * @param start 
   * @param end 
   */
  compareTwoDates(start: Date, end: Date): boolean {


    let startDate: moment.Moment = moment(start, "DD/MM/YYYY");
    let endDate: moment.Moment = moment(end, "DD/MM/YYYY");

    if (endDate > startDate) {
      return true;
    }
    else if (startDate < endDate) {
      return false;
    }
    else {
      return false;
    }
  }


}
