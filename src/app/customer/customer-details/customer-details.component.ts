import {Component, OnInit, ViewEncapsulation, Input} from '@angular/core';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerDetailsComponent implements OnInit {
  @Input() customerDetails: any;

  constructor() {
  }

  ngOnInit() {
  }

}
