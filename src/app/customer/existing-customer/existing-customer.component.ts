import { Subscription } from 'rxjs/Subscription';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { CustomerCrudService } from './../../api-module/services/customer-crud/customer-crud.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Page } from '../../shared-module/shared/model/page';


@Component({
  selector: 'app-existing-customer',
  templateUrl: './existing-customer.component.html',
  styleUrls: ['./existing-customer.component.css']
})

export class ExistingCustomerComponent implements OnInit, OnDestroy {

  public existCust = false;
  private callId: number;
  private customerId: number;

  private queryParams: any;
  private params: any;


  existedOrders: any[];
  existedCalls: any[];
  callData: any;
  existedCustomerData: any;
  workOrders;
  activeTab: string;

  orderByCustomerSubscription: Subscription;
  callByCustomerSubscription: Subscription;

  toggleLoading: boolean;
  page: Page = new Page();
  custDetails = true;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerCrudService,
    private alertService: AlertServiceService,
    private utilities: UtilitiesService,
    private lookupService: LookupService,
    private router: Router
  ) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
    this.callData = {};
    // //console.log('existed customer component');
    this.existedOrders = [];
    this.existedCalls = [];

    this.queryParams = this.route.queryParams.subscribe(qp => {
      if (qp.customerId) {
        this.customerId = qp.customerId;
        this.getCustomerDetails(this.customerId);
      }
    })

    this.params = this.route.params.subscribe(params => {
      if (params['CurrentCustomer']) {
        this.callId = +params['CurrentCustomer'];
        this.GetCallDetails();
      }
    });

    this.activeTab = 'info';
  }



  ngOnDestroy() {
    this.orderByCustomerSubscription && this.orderByCustomerSubscription.unsubscribe();
    this.callByCustomerSubscription && this.callByCustomerSubscription.unsubscribe();

    this.params.unsubscribe();
    this.queryParams.unsubscribe();
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    if (tab == 'orders') {
      this.getOrderDetails({ offset: 0 });
    } else if (tab == 'calls') {
      this.getCallsDetailsByPaging({ offset: 0 });
    }
  }


  getOrderDetails(pageNumber) {
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.id = this.customerId;

    this.orderByCustomerSubscription = this.lookupService.getOrderByCustomerIdWithPaging(objToPost).subscribe((orders) => {
      this.page.totalElements = orders.totalCount;
      this.existedOrders = orders.result;
    });
  }


  getCallsDetailsByPaging(pageNumber) {

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.id = this.customerId;

    this.callByCustomerSubscription = this.lookupService.getCallByCustomerIdBypaging(objToPost).subscribe((calls) => {
      this.page.totalElements = calls.totalCount;
      this.existedCalls = calls.result;
    });
  }

  getCustomerDetails(customerId) {
    this.lookupService.getCustomerDetails(customerId).subscribe((existedCustomerData) => {
      this.existedCustomerData = existedCustomerData;
    });
  }

  GetCallDetails() {
    this.toggleLoading = true;

    this.customerService.GetCallDetail(this.callId).subscribe(callData => {
      this.callData = callData;
      if (this.callData.fK_Customer_Id) {
        this.customerId = this.callData.fK_Customer_Id;
        this.getCustomerDetails(this.customerId);
      } else {
        this.utilities.updateCurrentExistedCustomer(callData);
      }
      this.toggleLoading = false;
    },
      err => {
        this.alertService.error('we have Server Error !')
      });
  }


}
