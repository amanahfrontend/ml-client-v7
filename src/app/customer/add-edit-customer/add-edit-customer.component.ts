import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import Swal from 'sweetalert2'
import { NgForm } from '@angular/forms';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-edit-customer',
  templateUrl: './add-edit-customer.component.html',
  styleUrls: ['./add-edit-customer.component.css'],
  providers: [NgbAccordionConfig]
})
export class AddEditCustomerComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  newCustomer: any;
  customerTypes: any[];
  governorates: any[];
  machineTypes: any[] = [];
  areas: any[];
  streets: any[];
  phoneTypes: any[];
  blocks: any[];
  cornerMessage: any = [];
  toggleLoadingLocation: boolean;
  location: any = {};
  phoneData: any = {};
  allLocations: any[] = [];
  phoneBook: any[] = [];
  hideCustomerButtons: boolean = false;
  disableForm: boolean = false;
  paciStatus: boolean;
  validAddress: boolean = false;
  convertIntoTextInputs: boolean;
  UpdatedCall: any = {};
  isFormSubmitted: boolean = false;

  isAddressCompnentValid: boolean = false;

  @Input() existedCustomerData: any;
  @Input() isNewCustomerFromContract: boolean = false;
  @Input() addressFromCaller: boolean = false;

  @Output() customerIdValue = new EventEmitter();
  @Output() closeFormNow = new EventEmitter();

  fetchPreviousData: {};

  toggleLoading: boolean = false;
  lockForm;

  isSaving: boolean;

  @ViewChild('newCustomerForm') newCustomerForm: NgForm;
  viewValidation: boolean;
  fK_CustomerType_Id;
  selectedLocation;
  clickedLocation;
  urlSegment: string;

  constructor(private lookup: LookupService,
    private router: Router,
    config: NgbAccordionConfig,
    private activatedRoute: ActivatedRoute
  ) {
    config.type = 'info',
      config.closeOthers = true;

    this.activatedRoute.url.subscribe(UrlSegment => {
      this.urlSegment = UrlSegment[0].path ? UrlSegment[0].path : '';
    })
  }

  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.getCustomerType();
    this.getPhoneTypes();
    if (this.newCustomer == undefined) {
      this.newCustomer = {
        locations: [{}],
        customerPhoneBook: [{}]
      };
    } else {
    }
    //this.newCustomer = this.existedCustomerData;
    //this.getGovernorates();
    this.getMachineTypes();
  }

  ngAfterViewInit() {
    if (this.newCustomer == undefined) {
      this.newCustomer = {
        locations: [{}],
        customerPhoneBook: [{}]
      };
    }
  }

  ngOnChanges() {
    if (this.existedCustomerData) {
      this.newCustomer = JSON.parse(JSON.stringify(this.existedCustomerData));
      if (this.newCustomer.fK_CustomerType_Id == 0) {
        this.newCustomer.fK_CustomerType_Id = undefined;
      }
      if (this.newCustomer.hasOwnProperty('callerName')) {

        //this.hideCustomerButtons = true;
        this.newCustomer.civilId = this.existedCustomerData.civilId;
        this.newCustomer.name = this.existedCustomerData.callerName;
        this.newCustomer.fK_MachineType_Id = this.existedCustomerData.fK_MachineType_Id;
        //this.newCustomer.fK_CustomerType_Id = this.existedCustomerData.fK_CustomerType_Id;

        this.newCustomer.remarks = this.existedCustomerData.customerDescription;
        this.newCustomer.paciNumber = this.newCustomer.paciNumber;
        this.location.area = this.newCustomer.area;
        this.location.block = this.newCustomer.block;
        this.location.street = this.newCustomer.street;
        this.location.addressNote = this.newCustomer.addressNote;
        this.location.governorate = this.newCustomer.governorate;
        this.location.latitude = this.newCustomer.latitude;
        this.location.longitude = this.newCustomer.longitude;
        this.location.title = this.newCustomer.title;
        this.location.building = this.newCustomer.building;
        this.location.appartmentNo = this.newCustomer.appartmentNo;
        this.location.floorNo = this.newCustomer.floorNo;
        this.location.houseNo = this.newCustomer.houseNo;
        this.phoneData.phone = this.newCustomer.callerNumber;
        this.phoneData.fK_PhoneType_Id = this.newCustomer.fK_PhoneType_Id;

        if (Object.keys(this.location).length > 0) {
          this.allLocations.push(this.location);
          this.newCustomer.locations = this.allLocations;
          this.onClickTab(this.allLocations[0]);
        }

        if (Object.keys(this.phoneData).length > 0) {
          this.phoneBook.push(this.phoneData);
          this.newCustomer.customerPhoneBook = this.phoneBook;
        }
      }
      else {
        this.location.area = this.newCustomer.locations[0].area;
        this.location.block = this.newCustomer.locations[0].block;
        this.location.street = this.newCustomer.locations[0].street;
        this.location.addressNote = this.newCustomer.locations[0].addressNote;
        this.location.governorate = this.newCustomer.locations[0].governorate;
        this.location.latitude = this.newCustomer.locations[0].latitude;
        this.location.longitude = this.newCustomer.locations[0].longitude;
        this.location.title = this.newCustomer.locations[0].title;
        this.location.building = this.newCustomer.locations[0].building;
        this.location.appartmentNo = this.newCustomer.locations[0].appartmentNo;
        this.location.floorNo = this.newCustomer.locations[0].floorNo;
        this.location.houseNo = this.newCustomer.locations[0].houseNo;

      }

      if (this.location.area &&
        this.location.block &&
        this.location.street &&
        this.location.governorate) {
        this.validAddress = true;
      }
      //this.getMultipleLocations(0, this.newCustomer.locations)

    }
  }


  PhoneChanged(phone: any) {
    var phones = this.newCustomer.customerPhoneBook.filter(x => x.phone == phone.phone);

    if (phones.length >= 2) {
      phone.isExist = true;
    }
    else {
      phone.isExist = false;
    }
  }




  //getMultipleLocations(i, locations) {

  //  this.newCustomer.locations[i].toggleLoading = true;
  //  console.log(locations.length);
  //  if (i == locations.length) {
  //    console.log('recursive done!');
  //  } else {
  //    this.getGovernorates().then(() => {
  //      console.log('in gov success promise');
  //      this.getAreas(this.newCustomer.locations[i]).then(() => {
  //        console.log('in area success promise');
  //        this.getBlocks(this.newCustomer.locations[i]).then(() => {
  //          console.log('in blocks success promise');
  //          this.getStreets(this.newCustomer.locations[i]).then(() => {
  //            this.newCustomer.locations[i].toggleLoading = false;
  //            this.getMultipleLocations(++i, locations)

  //          },
  //            (err) => {
  //              this.newCustomer.locations[i].toggleLoading = false;
  //            }
  //          )
  //        })
  //      })
  //    });
  //  }
  //}

  addNewLocation() {
    this.newCustomer.locations.push({ isNewLocation: true, fk_Customer_Id: this.newCustomer.id, title: "address"+(this.newCustomer.locations.length+1) });
  }


  /**
   * 
   * @param index 
   */
  removeLocation(index: number) {
    for (let i = 0; i < this.newCustomer.locations.length; i++) {
      if (i == index) {
        this.newCustomer.locations.splice(i, 1);
      }
    }

  }

  addNewPhone() {
    this.newCustomer.customerPhoneBook.push({ fk_Customer_Id: this.newCustomer.id });
  }

  removePhone(index) {
    this.newCustomer.customerPhoneBook.splice(index, 1);
  }

  // saveLocation(loc) {
  //   console.log(loc);
  //   let locationToPost = {
  //     "PACINumber": loc.paciNumber,
  //     "governorate": loc.governorate,
  //     "area": loc.area,
  //     "block": loc.block,
  //     "street": loc.street,
  //     "title": loc.title,
  //     "addressNote": loc.addressNote,
  //     "fk_Customer_Id": this.existedCustomerData.id,
  //     "id": loc.id
  //   };
  //   console.log(locationToPost);
  //   if (loc.isNewLocation) {
  //     this.lookup.postNewLocation(locationToPost)
  //     .subscribe(
  //       (res) => {
  //       this.cornerMessage.push({
  //         severity: "success",
  //         summary: "Successfully!",
  //         detail: "Location Added Successfully!"
  //       });
  //       setTimeout(() => {
  //         this.cornerMessage = [];
  //       }, 2000)
  //     }, err => {
  //         console.log(err);
  //       })
  //   } else {
  //     this.lookup.updateLocation(locationToPost)
  //       .subscribe(
  //         (res) => {
  //           this.cornerMessage.push({
  //             severity: "success",
  //             summary: "Successfully!",
  //             detail: "Location Saved Successfully!"
  //           });
  //           setTimeout(() => {
  //             this.cornerMessage = [];
  //           }, 2000)
  //         }, err => {
  //           console.log('failed');
  //         }
  //       );
  //   }
  // }


  onLocationChange(newLocation) {
    this.viewValidation = false;
    this.location.paciNumber = newLocation.paciNumber;
    this.location.governorate != 'undefined' ? newLocation.governorate : undefined;
    this.location.area != 'undefined' ? newLocation.area : undefined;
    this.location.block != 'undefined' ? newLocation.block : undefined;
    this.location.street != 'undefined' ? newLocation.street : undefined;
    this.location.title != null || undefined || '' ? newLocation.title : undefined;
    this.location.building != 'undefined' ? newLocation.building : '';
    this.location.appartmentNo != 'undefined' ? newLocation.appartmentNo : 0;
    this.location.floorNo != 'undefined' ? newLocation.floorNo : 0;
    this.location.houseNo != 'undefined' ? newLocation.houseNo : 0;
    this.location.addressNote != 'undefined' ? newLocation.addressNote : '';
    this.location.latitude = newLocation.latitude;
    this.location.longitude = newLocation.longitude;
  }

  closeForm() {
    this.closeFormNow.emit();
  }

  getPhoneTypes() {
    this.lookup.getPhoneTypes().subscribe((phoneTypes) => {
      this.phoneTypes = phoneTypes;
    });
  }

  getCustomerType() {
    this.lookup.getCustomerTypes()
      .subscribe(customerTypes => {
        this.customerTypes = customerTypes;
      });
  }


  postNewCustomer() {

    this.disableForm = true;
    let allLocations = [];
    var phoneindex = this.newCustomer.customerPhoneBook.findIndex(x => x.isExist == true);

    this.newCustomer.locations.map((singleLoc) => {
      allLocations.push({
        id: singleLoc.id,
        addressNote: singleLoc.addressNote,
        area: singleLoc.area,
        block: singleLoc.block,
        governorate: singleLoc.governorate,
        latitude: singleLoc.latitude,
        paciNumber: singleLoc.paciNumber,
        longitude: singleLoc.longitude,
        street: singleLoc.street,
        title: singleLoc.title,
        building: singleLoc.building,
        appartmentNO: singleLoc.appartmentNo,
        floorNo: singleLoc.floorNo,
        houseNo: singleLoc.houseNo,
        isValid: singleLoc.isValid,

        fk_Customer_Id: this.newCustomer.id,
      });
    });


    var isValidIndx = this.newCustomer.locations.findIndex(x => x.isValid == false, 0);
    if (phoneindex > -1) {
      return;
    }

    if (isValidIndx == -1) {

      let infoPostObject = {
        name: this.newCustomer.name,
        civilId: this.newCustomer.civilId,
        remarks: this.newCustomer.remarks,
        customerPhoneBook: this.newCustomer.customerPhoneBook,
        fK_CustomerType_Id: this.newCustomer.fK_CustomerType_Id,
        fK_MachineType_Id: this.newCustomer.fK_MachineType_Id,
        locations: allLocations
      };

      if (this.newCustomer.hasOwnProperty('callerName')) {
        this.lookup.postCustomer(infoPostObject)
          .subscribe(
            (res) => {
              this.updateCaller(res);
              Swal.fire({
                type: 'success',
                title: 'Added successfully',
              });
            }, err => {
              this.isSaving = false;
            }
          );
      }
      else {
        //this.newCustomer.locations = allLocations;
        if (this.isNewCustomerFromContract) {
          this.lookup.postCustomer(infoPostObject)
            .subscribe(
              (res) => {

                this.customerIdValue.emit(res);

                Swal.fire({
                  type: 'success',
                  title: 'Added successfully',
                });

              }, err => {
                this.isSaving = false;
              }
            );
        } else if (this.urlSegment == 'new-customer') {
          this.lookup.postCustomer(infoPostObject)
            .subscribe(
              (res) => {
                this.customerIdValue.emit(res);
                Swal.fire({
                  type: 'success',
                  title: 'Added successfully',
                });
                this.router.navigate(['search/customersList']);
              });
        }
        else {
          this.lookup.updateCustomer(this.newCustomer)
            .subscribe(
              (res) => {
                Swal.fire({
                  title: 'Success updated',
                  type: 'success',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#242424',
                  cancelButtonText: 'Stay !',
                  confirmButtonText: 'Back to customers'
                }).then((result) => {
                  if (result.value) {
                    this.router.navigate(['search/customersList']);
                  }
                });
              });
        }
      }
    }
  }

  updateCaller(caller: any): any {

    if (this.newCustomer.hasOwnProperty('callerName')) {
      this.existedCustomerData.callerName = this.newCustomer.name;
      this.existedCustomerData.civilId = this.newCustomer.civilId;
      this.existedCustomerData.callerNumber = this.newCustomer.customerPhoneBook[0].phone;
      this.existedCustomerData.fK_Customer_Id = caller.id;
      this.existedCustomerData.fK_CustomerType_Id = this.newCustomer.fK_CustomerType_Id;
      this.existedCustomerData.area = this.location.area;
      this.existedCustomerData.street = this.location.street;
      this.existedCustomerData.block = this.location.block;
      this.existedCustomerData.governorate = this.location.governorate;
      this.existedCustomerData.building = this.location.building;
      this.existedCustomerData.houseNo = this.location.houseNo;
      this.existedCustomerData.addressNote = this.location.addressNote;
      this.existedCustomerData.latitude = this.location.latitude;
      this.existedCustomerData.longitude = this.location.longitude;
      this.existedCustomerData.appartmentNo = this.location.appartmentNo;
      this.existedCustomerData.floorNo = this.location.floorNo;
      this.existedCustomerData.paciNumber = this.location.paciNumber;
      this.existedCustomerData.title = this.location.title;


      this.lookup.updateCaller(this.existedCustomerData)
        .subscribe(
          (res) => {
            this.router.navigate(['search/contract']);
          });
    }
  }

  //getGovernorates() {
  //  return new Promise((resolve, reject) => {
  //    this.lookup.GetallGovernorates()
  //      .subscribe(
  //        (govs) => {
  //          if (govs[0].name == 'Not Found In Paci') {
  //            this.convertIntoTextInputs = true;
  //            this.getGovernorates();
  //          } else {
  //            this.governorates = govs;
  //            this.paciStatus = true;
  //            this.convertIntoTextInputs = false;
  //            resolve();
  //          }
  //        },
  //        err => {
  //          console.log(err);
  //          this.getAllGovernments();
  //          this.paciStatus = false;
  //          this.convertIntoTextInputs = true;
  //        });
  //  });
  //}

  //getAllGovernments() {
  //  this.lookup.getAllGovernorateswithoutPaci().subscribe(res => {
  //    this.governorates = res;
  //  });
  //}

  //getAreas(singleLocation) {
  //  console.log('in getting areas');
  //  return new Promise((resolve, reject) => {
  //    let id = this.selectId(singleLocation.governorate, this.governorates);
  //    var lookup = this.lookup;
  //    (function internalRecursive() {
  //      lookup.GetallAreas(id).subscribe((areas) => {

  //        if (areas[0] != undefined) {

  //          if (areas[0].name == 'Not Found In Paci') {
  //            reject();
  //            //internalRecursive();
  //          } else {
  //            singleLocation.areas = areas;
  //            resolve();
  //          }
  //        }

  //      })
  //    }());
  //  })
  //}

  getMachineTypes() {
    this.lookup.getAllMachineTypes()
      .subscribe(
        res => {
          this.machineTypes = res;
        }, err => {

        }
      );
  }

  //getBlocks(singleLocation) {
  //  return new Promise((resolve, reject) => {
  //    let id = this.selectId(singleLocation.area, singleLocation.areas);
  //    var lookUp = this.lookup;
  //    (function internalRecursive() {
  //      lookUp.GetallBlocks(id)
  //        .subscribe(
  //        (blocks) => {
  //          if (blocks[0] != undefined) {
  //            if (blocks[0].name == 'Not Found In Paci') {
  //              reject();
  //            } else {
  //              singleLocation.blocks = blocks;
  //              resolve();
  //            }
  //          }
  //        },
  //          err => {
  //            console.log(err);
  //          })
  //    })();
  //  });
  //}

  //getStreets(singleLocation) {
  //  return new Promise((resolve, reject) => {
  //    let govId = this.selectId(singleLocation.governorate, this.governorates);
  //    let areaId = this.selectId(singleLocation.area, singleLocation.areas);
  //    var lookUp = this.lookup;
  //    (function internalRecursive() {
  //      lookUp.GetallStreets(areaId, singleLocation.block, govId)
  //        .subscribe(
  //        (streets) => {

  //          if (streets[0] != undefined) {
  //            // this.streets = streets;
  //            if (streets[0].name == 'Not Found In Paci') {
  //              reject();
  //              //internalRecursive();
  //            } else {
  //              singleLocation.streets = streets;
  //              resolve();
  //            }
  //          }
  //          },
  //          err => {
  //            console.log(err);
  //          }
  //        );
  //    })();
  //  });
  //}

  //getByPaci(locationIndex) {
  //  if (this.newCustomer.locations[locationIndex].paciNumber >= 8) {
  //    this.lookup.GetLocationByPaci(this.newCustomer.locations[locationIndex].paciNumber)
  //      .subscribe(
  //        (location) => {
  //          if (location.governorate) {
  //            this.newCustomer.locations[locationIndex].governorate = location.governorate.name;
  //          }
  //          if (location.area) {
  //            this.newCustomer.locations[locationIndex].areas = [{ name: location.area.name }];
  //            this.newCustomer.locations[locationIndex].area = location.area.name;
  //          }
  //          if (location.block) {
  //            this.newCustomer.locations[locationIndex].blocks = [{ name: location.block.name }];
  //            this.newCustomer.locations[locationIndex].block = location.block.name;
  //          }
  //          if (location.street) {
  //            this.newCustomer.locations[locationIndex].streets = [{ name: location.street.name }];
  //            this.newCustomer.locations[locationIndex].street = location.street.name;
  //          }
  //        },
  //        err => {
  //          console.log(err);
  //        }
  //      );
  //  }
  //}

  selectId(name, list) {
    if (name != undefined) {
      return list.filter((item) => {
        return item.name == name;
      })[0].id;
    }
  }

  /**
   * get customer details
   * 
   * 
   * @param customerId 
   */
  getCustomerDetails(customerId) {
    this.lookup.getCustomerDetails(customerId).subscribe((existedCustomerData) => {
      this.existedCustomerData = existedCustomerData;
    });
  }


  /**
   * 
   * @param i 
   */
  onClickTab(locaton) {
    this.fetchPreviousData = this.location;
  }


  setMargin() {
    let styles = {
      'padding-top': this.urlSegment == 'new-customer' ? '2.5em' : '1em',
    };
    return styles;
  }

  /**
   * check is new customer
   */
  get isNewCustomer(): boolean {
    if (this.urlSegment == 'new-customer') {
      return true;
    }
    return false;
  }
}
