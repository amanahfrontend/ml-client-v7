import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { CallsHistory } from './call-history/calls-history';
import { CustomerSearchPageComponent } from './customer-search-page/customer-search-page.component';
import { ExistingCustomerComponent } from './existing-customer/existing-customer.component';
import { LogsHistoryComponent } from './logs-history/logs-history.component';
import { EstimationComponent } from './estimation/estimation.component';
import { AllEstimationsComponent } from './all-estimations/all-estimations.component';
import { GenerateQuotationComponent } from './generate-quotation/generate-quotation.component';
import { AllQuotationsComponent } from "./all-quotations/all-quotations.component";
import { GenerateContractComponent } from './generate-contract/generate-contract.component';
import { AllContractsComponent } from './all-contracts/all-contracts.component';
import { EditEstimationComponent } from "./edit-estimation/edit-estimation.component";
import { GenerateOrderComponent } from './generate-order/generate-order.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { EstimationReportComponent } from './estimation-report/estimation-report.component';
import { ViewQuotationComponent } from './view-quotation/view-quotation.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerFromCallerComponent } from './add-customer-from-caller/add-customer-from-caller.component';
import { RoleGuard } from './../api-module/guards/role.guard';
import { AddEditCustomerComponent } from './add-edit-customer/add-edit-customer.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerSearchPageComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'customer/:CurrentCustomer',
    component: ExistingCustomerComponent,
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'caller-to-customer/:id',
    component: AddCustomerFromCallerComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'new-customer',
    component: AddEditCustomerComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance']  }
  },
  {
    path: 'new',
    component: AddNewCustomerComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  }
  ,
  {
    path: 'calls-history',
    component: CallsHistory,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'calls-history/logs/:id',
    component: LogsHistoryComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'calls-history/estimation/:id',
    component: EstimationComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'estimation',
    component: AllEstimationsComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'quotation',
    component: AllQuotationsComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Maintenance', 'CallCenter'] }
  },
  {
    path: 'estimation/:id',
    component: GenerateQuotationComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'customersList',
    component: CustomerListComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance','Admin'] }
  },
  {
    path: 'customersList/customerDetails',
    component: ExistingCustomerComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'editEstimation/:estimationHeader',
    component: EditEstimationComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Maintenance', 'CallCenter'] }
  },
  {
    path: 'quotation/viewQuotation/:quotationRefNumber',
    component: ViewQuotationComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'quotation/:refNumber',
    component: GenerateContractComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'contract',
    component: AllContractsComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter','Admin'] }
  },
  {
    path: 'contract/:id',
    component: GenerateOrderComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'caller/:generateOrder/:id',
    component: GenerateOrderComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'order',
    component: AllOrdersComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter','Admin'] }
  },
  {
    path: 'estimations-report',
    component: EstimationReportComponent,
    canActivate: [RoleGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: []
})
export class CustomerRoutingModule { }
