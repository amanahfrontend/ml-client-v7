import { Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Router } from "@angular/router";
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from "primeng/components/common/message";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { SearchComponent } from "../../shared-module/search/search.component";
import { Subscription } from "rxjs/Subscription";
import { ngxCsv } from 'ngx-csv/ngx-csv';
import Swal from 'sweetalert2'
import { Page } from '../../shared-module/shared/model/page';

@Component({
  selector: 'app-all-quotations',
  templateUrl: 'all-quotations.component.html',
  styleUrls: ['all-quotations.component.css'],
})

export class AllQuotationsComponent implements OnInit, OnDestroy {
  quotations: any[];
  quotationsSubscription: any;
  buttonsList: any[];
  activeRow: any;
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  roles: string[];
  toggleLoading: boolean;
  // private _hubConnection: HubConnection;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  notificationSubscription: Subscription;
  rows = new Array<any>();
  searchedString: any;

  page: Page = new Page();
  paginationFlag: boolean = false;
  totalElements: any;
  constructor(private lookup: LookupService, 
    private router: Router,
    private messageService: MessageService, 
    private auth: AuthenticationService,
    private utilities: UtilitiesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }


  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.paginationFlag == false)
      this.getAllQuotations(this.page.pageNumber);
    else
      this.filterQuotations(this.searchedString);
  }

  ngOnInit() {

    this.roles = this.auth.CurrentUser().roles;
    this.quotations = [];
    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
      if (value) {
        this.searchComponent.searchText = value;
        this.utilities.currentSearch = { searchText: value };
        this.filterQuotations(this.utilities.currentSearch);
      } else if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch && this.utilities.currentSearch.searchType == 'quotation') {
        console.log('will search');
        this.searchComponent.searchText = this.utilities.currentSearch.searchText;
        this.searchComponent.fromDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.fromDate);
        this.searchComponent.toDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.toDate);
        this.filterQuotations(this.utilities.currentSearch);
      } else {
        console.log('get all');
        this.getAllQuotations({ offset: 0 });
      }
    },
      err => {

      });
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    //console.log(this.activeRow);
  }

  ngOnDestroy() {
   // this.utilities.setSavedNotificationText('');
    this.quotationsSubscription && this.quotationsSubscription.unsubscribe();
    this.utilities.routingFromAndHaveSearch = false;
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Customer Name': 'Customer Name',
      'Customer Mobile': 'Customer Mobile',
      'Reference no': 'Reference no',
      'Estimation reference no': 'Estimation reference no',
      'Area': 'Area'
    });
    this.quotations.map((item) => {
      exportData.push({
        'Customer Name': (item.customerName == null) ? '' : item.customerName,
        'Customer Mobile': (item.customerMobile == null) ? '' : item.customerMobile,
        'Reference no': (item.refNumber == null) ? '' : item.refNumber,
        'Estimation reference no': (item.estimationRefNumber == null) ? '' : item.estimationRefNumber,
        'Area': (item.area == null) ? '' : item.area
      })
    });
    return new ngxCsv(exportData, 'Contract Report', {
      showLabels: true
    });
  }

  downloadQuotationWord(id) {
    this.lookup.downloadQuotationWord(id).subscribe((res) => {
      let url = res["_body"];
      window.open(url, '', '', true);
    },
      err => {
        console.log('err...', err._body);

        if (err._body == 'This Quotation Not Have Estimation Items') {
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'This Quotation Not Have Estimation Items'
          });
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'Error!',
            detail: 'Server error'
          });
        }

      })
  }

  getAllQuotations(pageNumber: any) {
    this.toggleLoading = true;

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let pagingData = Object.assign({}, this.page);
    pagingData.pageNumber = this.page.pageNumber + 1;

    this.quotationsSubscription = this.lookup.getAllQuotationsBypaging(pagingData).subscribe((quotations) => {
      this.rows = quotations.result;
      this.page.totalElements = quotations.totalCount;

      this.toggleLoading = false;
      this.quotations = quotations.result;

      this.bigMessage = [];
      this.bigMessage.push({
        severity: 'info',
        summary: 'Dropdown arrow',
        detail: 'You can remove any Quotation by click the dropdown arrow and choose remove.'
      });
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Quotations due to server error!'
        });
      })
  }
  //value.searchText && (value.SearchKey = value.searchText);


  filterQuotations(filterObj) {
    this.toggleLoading = true;
    this.paginationFlag = true;
    if (filterObj.searchText != undefined)
      this.searchedString = filterObj;
    else
      this.searchedString = filterObj;

    let pageNumber = this.page.pageNumber;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost;
    objToPost = filterObj;
    objToPost.searchKey = filterObj.searchText;
    objToPost.paginatedItemsViewModel = Object.assign({}, this.page);

    objToPost.paginatedItemsViewModel.pageNumber = this.page.pageNumber + 1;



    this.utilities.currentSearch.searchType = 'quotation';
    ////filterObj.searchText && (filterObj.SearchKey = filterObj.searchText);

    //let filterObjToPost = Object.assign({}, filterObj);

    //delete filterObjToPost.searchText;


    this.lookup.filterQuotationsByPaging(objToPost).subscribe((quotations) => {
      this.page.totalElements = quotations.totalCount;
      this.quotations = quotations.result;
      this.toggleLoading = false;
    },
      (error) => {
        this.cornerMessage.push({
          severity: 'info',
          summary: 'Not found',
          detail: 'This quotation not found.'
        });
        this.toggleLoading = false;
      })
  }

  routeToGenerateContract(refNumber) {
    // this.utilities.setRoutingDataPassed(quotationHeader);
    this.router.navigate(['/search/quotation/', refNumber])
  }

  popupCenter(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }

  routeToViewQuotation(estimationHeader) {
    this.router.navigate(['search/quotation/viewQuotation/', estimationHeader])
  }

  removeQuotation(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.lookup.deleteQuotation(id).subscribe(() => {
          this.quotations.filter((quotation) => {
            return quotation.id != id;
          });
          this.cornerMessage.push({
            severity: 'success',
            summary: 'Success',
            detail: 'Quotation Deleted Successfully!'
          });
          this.quotations = this.quotations.filter((quotation) => {
            return quotation.id != id;
          })
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.cornerMessage.push({
              severity: 'error',
              summary: 'Failed!',
              detail: 'Failed to delete quotation due to connection error.'
            });
          });
      }
    });

  }

}
