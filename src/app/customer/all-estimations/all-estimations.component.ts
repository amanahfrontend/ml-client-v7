import {Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Router} from '@angular/router';
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {SearchComponent} from "../../shared-module/search/search.component";
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-all-estimations',
  templateUrl: './all-estimations.component.html',
  styleUrls: ['./all-estimations.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AllEstimationsComponent implements OnInit, OnDestroy {
  estimations: any[];
  estimationsSubscription: Subscription;
  searchSubscription: Subscription;
  deleteEstimationSubscription: Subscription;
  // public Status: allStatusLookup;
  buttonsList: any[];
  activeRow: any;
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent) searchComponent: SearchComponent;

  constructor(private router: Router, private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService) {
  }

  ngOnInit() {

    console.log('routingFromAndHaveSearch', this.utilities.routingFromAndHaveSearch);
    console.log('currentSearch', this.utilities.currentSearch);
    if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch) {
      console.log('will search');
      this.searchComponent.searchText = this.utilities.currentSearch.searchText;
      this.searchComponent.fromDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.fromDate);
      this.searchComponent.toDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.toDate);
      this.filterEstimations(this.utilities.currentSearch);
    } else {
      console.log('get all');
      this.getAllEstimations();
    }
  }

  filterEstimations(value) {
    this.toggleLoading = true;
    value.searchText && (value.SearchKey = value.searchText);
    console.log(value);
    this.searchSubscription = this.lookup.filterEstimations(value).subscribe((searchResult) => {
        this.estimations = searchResult;
        this.toggleLoading = false;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to find item du to server error!'
        });
        this.toggleLoading = false;
      })
  }

  ngOnDestroy() {
    this.estimationsSubscription && this.estimationsSubscription.unsubscribe();
    this.searchSubscription && this.searchSubscription.unsubscribe();
    this.deleteEstimationSubscription && this.deleteEstimationSubscription.unsubscribe();
    this.utilities.routingFromAndHaveSearch = false;
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Customer Name': 'Customer Name',
      'Customer Mobile': 'Customer Mobile',
      'Reference no': 'Reference no',
      'Area': 'Area'
    });
    this.estimations.map((item) => {
      exportData.push({
        'Customer Name': item.customerName,
        'Customer Mobile': item.customerMobile,
        'Reference no': item.refNumber,
        'Area': item.area
      })
    });
    return new ngxCsv(exportData, 'Estimation Report', {
      showLabels: true
    });
  }


  getAllEstimations() {
    this.toggleLoading = true;
    this.bigMessage = [];
    this.estimationsSubscription = this.lookup.getAllEstimations().subscribe((estimations) => {
        console.log(estimations);
        this.toggleLoading = false;
        this.estimations = estimations;
        this.bigMessage[0] = {
          severity: 'info',
          summary: 'Dropdown arrow',
          detail: 'You can remove any estimation by click the dropdown arrow and choose remove.'
        };
      },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Estimations due to server error!'
        });
      });
  }

  setActiveRow(estimation) {
    this.activeRow = estimation;
    //console.log(this.activeRow);
  }

}
