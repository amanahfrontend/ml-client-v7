// modules
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerModule } from 'primeng/primeng';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { CustomerDropdownModule } from "./../components/customer-dropdown/customer-dropdown.module";
import { CustomerRoutingModule } from "./customer-routing.module";

// components
import { ContractAlmostDoneComponent } from './contract-almost-done/contract-almost-done.component';
import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { CallsHistory } from './call-history/calls-history';
import { CustomerSearchPageComponent } from './customer-search-page/customer-search-page.component';
import { ExistingCustomerComponent } from './existing-customer/existing-customer.component';
import { LogsHistoryComponent } from './logs-history/logs-history.component';
import { EstimationComponent } from './estimation/estimation.component';
import { AllEstimationsComponent } from './all-estimations/all-estimations.component';
import { GenerateQuotationComponent } from './generate-quotation/generate-quotation.component';
import { EstimationTableComponent } from './estimation-table/estimation-table.component';
import { AllQuotationsComponent } from "./all-quotations/all-quotations.component";
import { NewContractModalComponent } from './new-contract-modal/new-contract-modal.component';
import { GenerateContractComponent } from './generate-contract/generate-contract.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { AllContractsComponent } from './all-contracts/all-contracts.component';
import { EditEstimationComponent } from "./edit-estimation/edit-estimation.component";
import { GenerateOrderComponent } from './generate-order/generate-order.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { ExistedCustomerContractsComponent } from './existed-customer-contracts/existed-customer-contracts.component';
import { ContractTableComponent } from './contract-table/contract-table.component';
import { OrderTableComponent } from './order-table/order-table.component';
import { CallTableComponent } from './call-table/call-table.component';
import { AddContractFormComponent } from './add-contract-form/add-contract-form.component';
import { GenerateContractModalComponent } from './generate-contract-modal/generate-contract-modal.component';
import { EstimationReportComponent } from './estimation-report/estimation-report.component';
import { AllEstimationsTableComponent } from './all-estimations-table/all-estimations-table.component';
import { AddEditCustomerComponent } from './add-edit-customer/add-edit-customer.component';
import { CommentModalComponent } from './comment-modal/comment-modal.component';
import { ViewQuotationComponent } from './view-quotation/view-quotation.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerFromCallerComponent } from './add-customer-from-caller/add-customer-from-caller.component';
import { CustomerAddressComponent } from './customer-address/customer-address.component';
import { RoleGuard } from './../api-module/guards/role.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SpinnerModule,
    SharedModuleModule,
    CustomerDropdownModule,
    CustomerRoutingModule
  ],
  declarations: [
    AddCustomerFromCallerComponent,
    ContractAlmostDoneComponent,
    AddNewCustomerComponent,
    LogsHistoryComponent,
    CustomerSearchPageComponent,
    ExistingCustomerComponent,
    CallsHistory,
    EstimationComponent,
    AllEstimationsComponent,
    GenerateQuotationComponent,
    EstimationTableComponent,
    AllQuotationsComponent,
    NewContractModalComponent,
    GenerateContractComponent,
    CustomerDetailsComponent,
    AllContractsComponent,
    EditEstimationComponent,
    GenerateOrderComponent,
    AllOrdersComponent,
    ExistedCustomerContractsComponent,
    ContractTableComponent,
    OrderTableComponent,
    CallTableComponent,
    AddContractFormComponent,
    GenerateContractModalComponent,
    EstimationReportComponent,
    AllEstimationsTableComponent,
    AddEditCustomerComponent,
    CommentModalComponent,
    ViewQuotationComponent,
    CustomerListComponent,
    CustomerAddressComponent
  ],
  entryComponents: [
    NewContractModalComponent,
    GenerateContractModalComponent,
    CommentModalComponent,
    ContractAlmostDoneComponent
  ],
  providers: [
    RoleGuard
  ]
})
export class CustomerModule {
}
