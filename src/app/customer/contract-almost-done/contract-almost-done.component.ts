import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-contract-almost-done',
  templateUrl: './contract-almost-done.component.html',
  styleUrls: ['./contract-almost-done.component.css']
})
export class ContractAlmostDoneComponent {
  @Input() data: any;
  show = false;
  @Output() print = new EventEmitter();


  constructor(public activeModal: NgbActiveModal) { }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Contract Number': 'Contract Number',
      'Type': 'Type',
      'Customer Name': 'Reference no',
      'Start date': 'Start date',
      'End Date': 'End Date',
      'Price': 'Price',
      'Remarks': 'Remarks'
    });
    this.data.map((item) => {
      exportData.push({
        'Contract Number': item.contractNumber,
        'Type': item.contractType.name,
        'Customer Name': item.customer.name,
        'Start date': item.startDate,
        'End Date': item.endDate,
      })
    });
    return new ngxCsv(exportData, 'All contracts almost done', {
      showLabels: true
    });
  }

  emitPrint(): void {
    this.print.emit();
  }
  printContracts(): void {
    this.show = true;
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("contract");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    this.show = false;

    newWin.close();

  }


  closeModal() {
    this.activeModal.close();
  }

}
