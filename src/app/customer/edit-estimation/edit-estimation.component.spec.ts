import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEstimationComponent } from './edit-estimation.component';

describe('EditEstimationComponent', () => {
  let component: EditEstimationComponent;
  let fixture: ComponentFixture<EditEstimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEstimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEstimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
