import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { Params, ActivatedRoute } from "@angular/router";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Message } from "primeng/components/common/message";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";

@Component({
  selector: 'app-edit-estimation',
  templateUrl: 'edit-estimation.component.html',
  styleUrls: ['edit-estimation.component.css']
})
export class EditEstimationComponent implements OnInit, OnDestroy {

  getEstimationDetailsSubscription: Subscription;
  getItemsSubscription: Subscription;
  estimationDetails: any;
  toggleLoading: boolean;
  editToggle: boolean;
  type: string;
  // previousUrl: string;
  msg: Message[];

  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService, private auth: AuthenticationService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.editToggle = this.auth.CurrentUser().roles.includes('Maintenance');
    if (this.editToggle) {
      this.type = 'editEstimation';
    } else {
      this.type = 'contract';
    }

    this.activatedRoute.params.forEach((param: Params) => {
      let id = +param['estimationHeader'];
      this.getEstimationDetailsSubscription = this.lookup.getEstimationDetails(id).subscribe((estimationDetails) => {
        this.toggleLoading = false;
        this.estimationDetails = estimationDetails;
        this.estimationDetails.lstEstimationItems.map((estimation) => {
          estimation.soldQuantity = estimation.quantity;
        });
        //console.log(this.estimationDetails.id);
      },
        err => {
          this.toggleLoading = false;
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Failed to load data due to server error'
          });
        })
    });
  }

  ngOnDestroy() {
    if (this.type == 'editEstimation') {
      this.utilities.routingFromAndHaveSearch = true;
    }
    this.getEstimationDetailsSubscription.unsubscribe();
  }

  requestCode(code) {
    this.toggleLoading = true;

    this.getItemsSubscription = this.lookup.getItem(code).subscribe((item) => {
      item.soldQuantity = 0;
      item.margin = 0;
      item.totalPrice = 0;
      item.totalMarginPrice = 0;
      item.error = '';
      this.estimationDetails.lstEstimationItems.push(item);
      this.toggleLoading = false;
    },
      (err) => {
        this.toggleLoading = false;
      });
  }

  deleteItem(no) {
    this.estimationDetails.lstEstimationItems = this.estimationDetails.lstEstimationItems.filter((item) => {
      return item.no != no;
    });
  }
}
