import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateContractModalComponent } from './generate-contract-modal.component';

describe('GenerateContractModalComponent', () => {
  let component: GenerateContractModalComponent;
  let fixture: ComponentFixture<GenerateContractModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateContractModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateContractModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
