import {
  Component, OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { EditOrderModalComponent } from "../edit-order-modal/edit-order-modal.component"
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message } from 'primeng/components/common/api';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { OrderModalStatusComponent } from '../order-modal-status/OrderModalStatus.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { Page } from '../../shared-module/shared/model/page';
import { AuthService } from './../../services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})

export class OrderTableComponent implements OnInit, OnChanges {
  @Input() orders: any[];
  @Output() emitAllData: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitRefresh: EventEmitter<boolean> = new EventEmitter<boolean>();


  @Input() isCustomerDetails: boolean = false;

  // deleteOrderSubscription: Subscription;
  cornerMessage: Message[] = [];
  modalRef: any;
  buttonsList: any[];
  activeRow: any;
  @Input() page: Page;

  isAdmin: boolean = false;

  constructor(
    private modalService: NgbModal,
    private utilities: UtilitiesService,
    private authService: AuthService,
    private lookupService: LookupService,
  ) {
    if (this.authService.isAdmin) {
      this.isAdmin = true;
    }
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.orders = this.orders || [];
    this.orders.map((order) => {
      order.startDateView = this.utilities.convertDatetoNormal(order.startDate);

      order.endDateView = this.utilities.convertDatetoNormal(order.endDate);

      order.name = order.customer ? order.customer.name : order.call.callerName;

    });
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.utilities.paginationFlag == false)
      this.emitAllData.emit(this.page.pageNumber);
    else
      this.emitSearchedData.emit({ pageNumber: this.page.pageNumber, searchedParmaters: 'search' });
  }



  exportCsv() {
    let exportData = [];
    exportData.push({
      'Order Number ': 'Order Number',
      'Order Problem ': 'Order Problem ',
      'Order Priority ': 'Order Priority ',
      'Start date': 'Start date',
      'End Date': 'End Date',
      'Price': 'Price',
    });
    this.orders.map((item) => {
      if (item.problem == null)
        item.orderProblem = { "name": "-" }
      if (item.orderPriority == null)
        item.orderPriority = { "name": "-" }
      exportData.push({
        'Order Number': (item.code == null) ? '' : item.code,
        'Order Problem': (item.orderProblem.name == null) ? '' : item.orderProblem.name,
        'Order Priority': (item.orderPriority.name == null) ? '' : item.orderPriority.name,
        'Start date': (item.startDate == null) ? '' : item.startDate,
        'End Date': (item.endDate == null) ? '' : item.endDate,
        'Price': (item.price == null) ? '' : item.price,

      })
    });
    return new ngxCsv(exportData, 'OrdersReport', {
      showLabels: true
    });
  }

  setActiveRow(row) {
    this.activeRow = row;
  }

  /**
   * 
   * @param order 
   */
  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        //this.cornerMessage.push({
        //  severity: "success",
        //  summary: "Order Saved!",
        //  detail: "Order edits applied & Saved successfully."
        //})
      })
      .catch(() => {
        //this.cornerMessage.push({
        //  severity: "info",
        //  summary: "Cancelled!",
        //  detail: "Order Edits cancelled without saving."
        //})
      })
  }


  viewOrderStatus(order) {
    this.openModalStatus(order);
  }

  /**
   * 
   * @param data 
   */
  openModal(data) {
    this.modalRef = this.modalService.open(EditOrderModalComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.data = data;

    this.modalRef.result.then(res => {
      this.emitRefresh.emit(true);
    })
  }

  /**
   * 
   * @param data 
   */
  openModalStatus(data) {
    this.modalRef = this.modalService.open(OrderModalStatusComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.data = data;
  }


  /**
 * 
 * @param row 
 */
  deleteOrder(row) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.lookupService.deleteOrder(row.id).subscribe(() => {

          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
          })

          Toast.fire({
            type: 'success',
            title: 'Order has been deleted'
          })

          setTimeout(() => {
            this.emitRefresh.emit(true);
          }, 2000);
        });
      }
    });
  }

}
