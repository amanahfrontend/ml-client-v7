import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private messageSource = new BehaviorSubject('default');
  currentCustomer = this.messageSource.asObservable();

  constructor() { }

  /**
   * 
   * @param customer 
   */
  sendCustomer(customer: string) {
    this.messageSource.next(customer);
  }

}
