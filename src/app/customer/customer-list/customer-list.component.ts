import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { Page } from '../../shared-module/shared/model/page';
import { CustomerService } from "./customer.service";
import { PrintService } from './../../services/print.service';
import Swal from 'sweetalert2';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { AuthenticationService } from '../../api-module/services/authentication/authentication.service';
import { AlertService } from './../../services/alert.service';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})

export class CustomerListComponent implements OnInit {
  customers: any[] = [];
  allCustomers: any[] = [];
  toggleLoading: boolean;
  hideDate: boolean = false;
  filteredCustomers: any[] = [];
  s: string;
  roles: string[];
  rows = new Array<any>();

  page: Page = new Page();
  searchedString: any;
  totalElements: number = 0;
  paginationFlag: boolean = false;
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();

  isAdmin: boolean = false;

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private auth: AuthenticationService,
    private customerService: CustomerService,
    private printService: PrintService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
    if (this.authService.isAdmin) {
      this.isAdmin = true;
    }

    this.toggleLoading = true;
    this.roles = this.auth.CurrentUser().roles;
    this._getCustomers({ offset: 0 });
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.paginationFlag == false)
      this._getCustomers(this.page.pageNumber);
    else
      this.searchByValue(this.searchedString);
  }

  _getCustomersAllCustomers() {
    this.paginationFlag = false
    this.searchedString = null;
    this._getCustomers({ offset: 0 });
  }

  _getCustomers(pageNumber: any) {
    this.toggleLoading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;



    this.lookup.getAllCustomersByPaging(objToPost)
      .subscribe((customers) => {
        this.rows = customers.result;
        this.totalElements = customers.totalCount;

        this.customers = customers.result;
        this.allCustomers = JSON.parse(JSON.stringify(this.customers));
        this.filteredCustomers = this.customers;

        this.toggleLoading = false;
      },
        err => {
          this.toggleLoading = false;
        })
  }

  //filterQustomers(searchData) {
  //  this.paginationFlag = true;
  //  if (searchData.searchText != undefined)
  //    this.searchedString = searchData.searchText;
  //  else
  //    this.searchedString = searchData;

  //  this.filteredCustomers = [];
  //  for (let i = 0; i < this.customers.length; i++) {
  //    if (this.customers[i].name) {
  //      if (this.customers[i].name.toLowerCase().includes(this.searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].civilId) {
  //      if (this.customers[i].civilId.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].customerPhoneBook[0].phone) {
  //      if (this.customers[i].customerPhoneBook[0].phone.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].block) {
  //      if (this.customers[i].locations[0].block.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].street) {
  //      if (this.customers[i].locations[0].street.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].area) {
  //      if (this.customers[i].locations[0].area.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].governorate) {
  //      if (this.customers[i].locations[0].governorate.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //  }
  //  console.log('%c we are here', 'color: orange; font-weight: bold;');
  //  console.log(this.filteredCustomers);
  //}


  searchByValue(searchData) {
    this.toggleLoading = true;
    this.paginationFlag = true;
    this.filteredCustomers = [];

    if (searchData.searchText != undefined)
      this.searchedString = searchData.searchText;
    else
      this.searchedString = searchData;

    let pageNumber = this.page.pageNumber;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = this.searchedString;


    this.lookup.searchCustmerByPaging(objToPost).subscribe((searchResult) => {
      this.filteredCustomers = searchResult.result;
      this.totalElements = searchResult.totalCount;
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      })
  }


  printCustomer() {
    this.utilities.printComponent('customer-table')
  }

  exportCsv() {
    let exportData = [];

    this.customers.map((item) => {
      exportData.push({
        'Name': (item.name == null) ? '' : item.name,
        'Phone': (item.customerPhoneBook[0].phone == null) ? '' : item.customerPhoneBook[0].phone,
        'civilId': (item.civilId == null) ? '' : item.civilId,
        'Address': `${item.locations[0].block}, ${item.locations[0].street}, ${item.locations[0].area}, ${item.locations[0].governorate}`
      })
    });

    return new ngxCsv(exportData, 'Customers', {
      showLabels: true,
      headers: ['Name', 'Phone', 'CID', 'Address'],
      showTitle: true,
      keys: ['Name', 'Phone', 'civilId', 'Address'],
      removeNewLines: true,
    });
  }

  /**
   * 
   * @param customer 
   */
  shareCustomerObject(customer: Object) {
    this.customerService.sendCustomer(JSON.stringify(customer));
  }

  printCustomers() {
    this.printService.print('content', 'customers');
  }

  /**
   * 
   * @param row 
   */
  deleteCustomer(row) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.lookup.deleteCustomer(row.id).subscribe(() => {
          this.alertService.alertSuccess({ title: 'Customer has been deleted' })
          this.customers = this.customers.filter((customers) => {
            return customers.id != row.id;
          });
          this._getCustomers({ offset: 0 });
        });
      }
    });
  }
}
