/**
 * five cases keep in mind
 * 
 * -------------------------------------------------------------------------------------------
 * Caller not a customer so should be cash call
 * 
 * Caller is customer with expired contract then should be cash call
 * 
 * Caller is customer with contract then will be normal call with types
 * 
 * Generate order from contract , contract is expired then will be cash call
 * 
 * Generate order from contract , contract is still working then will be normal order with types
 * ---------------------------------------------------------------------------------------------
 **/

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { MessageService } from "primeng/components/common/messageservice";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Message } from "primeng/components/common/message";

@Component({
  selector: 'app-generate-order',
  templateUrl: './generate-order.component.html',
  styleUrls: ['./generate-order.component.css']
})

export class GenerateOrderComponent implements OnInit, OnDestroy {
  contractDetails: any;

  postNewOrderSubscription: Subscription;

  dateConvertedFlag: boolean;
  customerId: any;
  toggleLoading: boolean;
  toggleOrderSectionLoading: boolean;

  todayDate: Date;
  msg: Message[];

  isCashCall: boolean = false;
  expired: boolean = false;

  callerId: number;

  constructor(private activatedRoute: ActivatedRoute,
    private lookup: LookupService,
    private messageService: MessageService,
    private router: Router,
    private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.dateConvertedFlag = false;
    this.utilities.routingFromAndHaveSearch = true;

    this.activatedRoute.params.forEach((params: Params) => {
      if (params['generateOrder'] === "generateOrder") {
        this.isCashCall = true;
        this.callerId = +params['id'];
        this.toggleLoading = false;
      }
      else {
        this.lookup.getContractById(+params['id']).subscribe((contractDetails) => {

          this.contractDetails = contractDetails;

          this.contractDetails.startDate = this.formatDate(this.contractDetails.startDate);
          this.contractDetails.endDate = this.formatDate(this.contractDetails.endDate);

          this.customerId = this.contractDetails.customer.id;
          this.contractDetails.customer.customerName = this.contractDetails.customer.name;
          this.contractDetails.customer.customerMobile = this.contractDetails.customer.phoneNumber;

          const exp = contractDetails.startDate;
          let status = this.checkDates(exp);

          if (status) {
            this.expired = true;
            this.isCashCall = true;
          } else {
            this.isCashCall = false;
          }

          this.toggleLoading = false;
        });
      }
    });
  }

  ngOnDestroy() {
    this.postNewOrderSubscription && this.postNewOrderSubscription.unsubscribe();
  }

  generateOrder(newOrder) {
    
    if (this.isCashCall == true) {
      this.toggleOrderSectionLoading = true;

      newOrder.startDate = this.dateConvertedFlag ? newOrder.startDate : this.utilities.convertDateForSaving(newOrder.startDate);
      newOrder.endDate = this.dateConvertedFlag ? newOrder.endDate : this.utilities.convertDateForSaving(newOrder.endDate);

      if (this.expired) {
        newOrder.fK_Customer_Id = this.contractDetails.customer.id;
        newOrder.fK_Contract_Id = this.contractDetails.id;
        newOrder.fK_Location_Id = newOrder.fK_Location_Id;
      } else {
        newOrder.fK_Customer_Id = null;
        newOrder.fK_Contract_Id = null;
      }

      this.dateConvertedFlag = true;
      newOrder.fk_Call_Id = this.callerId;

      this.postNewOrderSubscription = this.lookup.postNewOrder(newOrder).subscribe(() => {
        this.alertSuccess();
        this.toggleOrderSectionLoading = false;

        setTimeout(() => {
          this.router.navigate(['search/order'])
        }, 500);

      },
        err => {
          this.alertError();;
          this.toggleOrderSectionLoading = false;
        })


    }
    else {
      this.toggleOrderSectionLoading = true;

      newOrder.fK_Customer_Id = this.contractDetails.customer.id;
      newOrder.fK_Contract_Id = this.contractDetails.id;

      if (this.contractDetails.contractQuotations[0]) {
        newOrder.quotationRefNo = this.contractDetails.contractQuotations[0].quotationRefNumber;
      }

      newOrder.price = this.contractDetails.price;
      newOrder.fk_Call_Id = null;

      this.dateConvertedFlag = true;

      this.postNewOrderSubscription = this.lookup.postNewOrder(newOrder).subscribe(() => {
        this.alertSuccess();
        this.toggleOrderSectionLoading = false;
        this.router.navigate(['search/order'])
      },
        err => {
          this.alertError();
          this.toggleOrderSectionLoading = false;
        })
    }

  }


  /**
   * alert error
   * 
   * 
   */
  alertError(): void {
    this.messageService.add({
      severity: 'error',
      summary: 'Failed!',
      detail: 'Failed to generate new Order due to network error.'
    });
  }

  /**
   * alert success
   * 
   * 
   */
  alertSuccess(): void {
    this.messageService.add({
      severity: 'success',
      summary: 'Order Generated!',
      detail: 'Order Generated successfully!'
    });
  }

  /**
 * 
 * @param date 
 */
  formatDate(date: Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  /**
   * check dates
   * 
   * 
   * @param expire 
   */
  checkDates(expire) {
    const today = new Date();
    const date = new Date(expire);

    const finalToday = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate());
    const expireDate = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());

    if (expireDate < finalToday) {      
      return true;
    } else {
      return false;
    }

  }

}
