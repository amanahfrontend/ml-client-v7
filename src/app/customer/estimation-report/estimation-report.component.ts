import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-estimation-report',
  templateUrl: './estimation-report.component.html',
  styleUrls: ['./estimation-report.component.css']
})
export class EstimationReportComponent implements OnInit {
  estimations: any[];
  toggleLoading: boolean;

  constructor(private lookup: LookupService) {
  }

  ngOnInit() {
    this.filterEstimations({});
  }

  filterEstimations(filterObj) {
    this.toggleLoading = true;
    this.lookup.filterEstimations(filterObj).subscribe((estimations) => {
      this.estimations = estimations;
      this.toggleLoading = false;
    }, err => {
      this.toggleLoading = false;
    })
  }
}
