import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from "@angular/router";
import { Message } from "primeng/components/common/message";
import { AuthenticationService } from "../../api-module/services/authentication/authentication.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { CustomerCrudService } from "../../api-module/services/customer-crud/customer-crud.service";
import { Subscription } from "rxjs";
import { SearchComponent } from "../../shared-module/search/search.component";
import { Page } from "../../shared-module/shared/model/page";

@Component({
  selector: 'calls-history',
  templateUrl: './calls-history.html',
  styleUrls: ['./calls-history.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class CallsHistory implements OnInit, OnDestroy {
  calls: any[];
  callsSubscrption: Subscription;
  searchSubscription: Subscription;
  content: any;
  roles: string[];
  // statusSubscription: Subscription;
  warningMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  notificationSubscription: Subscription;
  page = new Page();
  rows = new Array<any>();

  constructor(private lookUp: LookupService,
    private utilities: UtilitiesService,
    private customerService: CustomerCrudService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {

    /*init new action object to be filled and send later*/
    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
      if (value) {
        this.searchComponent.searchText = value;
        this.utilities.routingFromAndHaveSearch = false;
        this.searchByValue(value);
      }
      else if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch && this.utilities.currentSearch.searchType == 'call') {
        this.searchComponent.searchText = this.utilities.currentSearch.searchText;
        this.utilities.routingFromAndHaveSearch = false;
        this.searchByValue(this.utilities.currentSearch.searchText);
      }
      else {
        this.getAllCalls({ offset: 0 });
      }
    },
      err => {

      });

    this.customerService.searchedValue = '';
  }

  ngOnDestroy() {
    this.callsSubscrption && this.callsSubscrption.unsubscribe();
    this.notificationSubscription && this.notificationSubscription.unsubscribe();
    this.utilities.routingFromAndHaveSearch = false;
    this.utilities.setSavedNotificationText('');
  }




  getAllCalls(pageInfo) {
    this.toggleLoading = true;
    if (pageInfo > 0)
      this.page.pageNumber = pageInfo;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    this.callsSubscrption = this.lookUp.callsHistoryByPaging(objToPost).subscribe((callsHistory) => {
      this.rows = callsHistory.result;
      this.page.totalElements = callsHistory.totalCount;

      this.toggleLoading = false;
      this.calls = callsHistory.result;
      //console.log(this.calls);
      this.warningMessage = [];
      this.warningMessage.push({
        severity: "info",
        summary: "DropDown Arrow",
        detail: "You Can Add new action by click on the dropdown arrow and choose Add Action"
      })
    },
      err => {
        // //console.log(err);
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get Calls history due to server error"
        })
      });
  }

  searchByValue(value) {
    this.toggleLoading = true;
    this.customerService.paginationFlag = true;
    let pageNumber;


    if (value.searchedParmaters == null || value.searchedParmaters == undefined)
      this.customerService.searchedValue = value.trim();
    else {
      pageNumber = value.pageNumber;
      value = this.customerService.searchedValue;
    }

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = value;
    this.utilities.currentSearch.searchType = 'call';

    this.searchSubscription = this.customerService.SearchCustomerByPaging(objToPost).subscribe((searchResult) => {
      this.calls = searchResult.result;
      this.page.totalElements = searchResult.totalCount;
      this.toggleLoading = false;


    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to find item du to server error!'
        });
        this.toggleLoading = false;
      })
  }


}
