import { Component, OnInit, ViewEncapsulation, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Router } from "@angular/router";
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from "primeng/components/common/message";
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../api-module/services/authentication/authentication.service';

@Component({
  selector: 'app-estimation-table',
  templateUrl: './estimation-table.component.html',
  styleUrls: ['./estimation-table.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class EstimationTableComponent implements OnInit, OnChanges {
  @Input() items: any[];
  @Input() editMode: boolean;
  @Input() type: string;
  @Input() id: any;
  @Input() EstimationRefNumber: any;
  @Input() callId: any;
  @Output() deletedId = new EventEmitter();
  totalMarginPrice: number;
  toggleLoading: boolean;
  statuses: any[];
  cornerMessage: Message[] = [];
  statusSubscription: Subscription;


  constructor(private lookUp: LookupService, private router: Router, private messageService: MessageService, private auth: AuthenticationService) {
  }

  ngOnInit() {
    // this.items = [];
    //console.log(this.items);
    //console.log(this.type);
    this.getStatuses();
    if (!this.editMode) {
      // this.totalPriceToggle = {visibility: 'hidden'}
    }
  }

  ngOnChanges() {
    //console.log('in change event estimation table');
    //console.log(this.items);
    //console.log(this.type);
    // this.totalMarginPrice = 0;
    this.totalMarginPrice = 0;
    if (this.type === 'contract') {
      this.items && this.items.map((item) => {
        this.totalMarginPrice += Math.round(item.totalMarginPrice * 100) / 100;
      });
    } else {
      //console.log('in else');
      this.items && this.items.map((item) => {
        this.calculatePrice(item);
      })
    }
  }

  routeToAllEstimations() {
    this.router.navigate(['search/estimation']);
  }

  calculatePrice(item) {
    this.totalMarginPrice = 0;
    item.totalPrice = item.unitPrice * item.soldQuantity;
    item.totalMarginPrice = item.totalPrice + (item.totalPrice * (item.margin / 100));
    this.items.map((item) => {
      //console.log(item.totalMarginPrice);
      this.totalMarginPrice += item.totalMarginPrice;
    });
    this.totalMarginPrice = Math.round(this.totalMarginPrice * 100) / 100;
  }

  deleteItem(no) {
    this.deletedId.emit(no);
  }

  editEstimation() {
    this.router.navigate(['/search/editEstimation/', this.id]);
  }

  postEstimations() {
    let choosedItems = [];
    let price = 0;
    this.toggleLoading = true;
    this.items.map((item) => {
      choosedItems.push(Object.assign({}, item));
    });
    choosedItems.map((item) => {
      item.quantity = item.soldQuantity;
      price += item.totalMarginPrice;

      price = Math.round(price * 100) / 100;


      delete item.id;
      delete item.error;
      delete item.soldQuantity;
      delete item.description;
      delete item.fk_category_id;
      //console.log(item);
    });

    let toSend: any = {
      fk_Call_Id: this.callId,
      refNumber: this.EstimationRefNumber,
      price: price,
      lstEstimationItems: choosedItems
    };

    //console.log(toSend);
    if (this.type == 'estimation') {

      delete toSend.refNumber;
      this.lookUp.postEstimation(toSend).subscribe(() => {

        var calllog: any = {}
        calllog.fk_Call_Id = this.callId;
        calllog.customerServiceName = this.auth.CurrentUser().fullName;
        calllog.customerServiceUserName = this.auth.CurrentUser().userName;

        var callstatus = this.statuses.find(x => x.name == 'Estimated', 0);
        console.log(callstatus, 'Call Status');
        calllog.fk_CallStatus_Id = callstatus.id;
        calllog.customerServiceComment = 'New Estimation created';


        this.lookUp.postNewLog(calllog).subscribe(() => {
          //console.log('posted successfully');
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Estimation saved successfully!'
          });
        },
          err => {
            this.cornerMessage.push({
              severity: "error",
              summary: "Failed",
              detail: "Failed Save new Action due to server error"
            })
          });



        this.toggleLoading = false;
        // this.routeToAllEstimations();
        //console.log('success');
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/estimation']);
        }, 2000);
      },
        err => {
          this.toggleLoading = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to Save Estimation due to server error'
          });
        });
    } else if (this.type == 'quotation') {
      toSend = {
        EstimationRefNumber: this.EstimationRefNumber,
        price: toSend.price
      };
      //console.log(toSend);
      this.lookUp.generateQuotation(toSend).subscribe(() => {
        var calllog: any = {}
        calllog.fk_Call_Id = this.callId;
        calllog.customerServiceName = this.auth.CurrentUser().fullName;
        calllog.customerServiceUserName = this.auth.CurrentUser().userName;

        var callstatus = this.statuses.find(x => x.name == 'Quoted', 0);
        console.log(callstatus, 'Call Status');
        calllog.fk_CallStatus_Id = callstatus.id;
        calllog.customerServiceComment = 'New Quoted created';


        this.lookUp.postNewLog(calllog).subscribe(() => {
          //console.log('posted successfully');
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Quotation Generated successfully!'
          });
        },
          err => {
            this.cornerMessage.push({
              severity: "error",
              summary: "Failed",
              detail: "Failed Save new Action due to server error"
            })
          });


        this.toggleLoading = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/quotation'])
        }, 2000);
      },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to Generate Quotation due to server error'
          });
          this.toggleLoading = false;
        });
    } else if (this.type == 'editEstimation') {
      //console.log(toSend);
      toSend.id = this.id;
      this.lookUp.updateEstimation(toSend).subscribe(() => {


        var calllog: any = {}
        calllog.fk_Call_Id = this.callId;
        calllog.customerServiceName = this.auth.CurrentUser().fullName;
        calllog.customerServiceUserName = this.auth.CurrentUser().userName;

        var callstatus = this.statuses.find(x => x.name == 'Estimated', 0);
        console.log(callstatus, 'Call Status');
        calllog.fk_CallStatus_Id = callstatus.id;
        calllog.customerServiceComment = 'Estimation Updated ';


        this.lookUp.postNewLog(calllog).subscribe(() => {
          //console.log('posted successfully');
          // //console.log('updated successfully');
          this.messageService.add({
            severity: 'success',
            summary: 'Success!',
            detail: 'Estimation saved successfully!'
          });
        },
          err => {
            this.cornerMessage.push({
              severity: "error",
              summary: "Failed",
              detail: "Failed Save new Action due to server error"
            })
          });


        this.toggleLoading = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/calls-history']);
        }, 2000);
      },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Failed to Save Estimation due to server error!'
          });
          this.toggleLoading = false;
        })
    }
  }



  getStatuses() {
    this.statusSubscription = this.lookUp.getStatues().subscribe((status) => {
      this.statuses = status;
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to server error"
        })
      })
  }

  back() {
    let route = this.router.url,
      search = 'editEstimation',

      isEstimation = !(route.indexOf(search) == -1);
    if (isEstimation) {
      // Estimations route
      this.router.navigate(['search/estimation']);
    } else { 
      // Quotations route
      this.router.navigate(['search/quotation']);
    }
  }

  /**
   * 
   * @param num 
   */
  roundToTwo(num: number): number {
    return Math.round(num * 100) / 100;
  }
}
