import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges, SimpleChanges, ChangeDetectionStrategy } from "@angular/core";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgForm } from "@angular/forms";
import { DataService } from "../../admin/settings/data.service";

@Component({
  selector: 'app-customer-address',
  templateUrl: './customer-address.component.html',
  styleUrls: ['./customer-address.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})

export class CustomerAddressComponent implements OnInit, OnChanges {


  i;
  cornerMessage: any = [];

  @Input() loadPreviousData: boolean;
  @Input() addresstoggleLoading: boolean;
  @Input() location: any;
  @Input() governorates: any;
  @Input() convertIntoTextInputs: Boolean;
  @Input() paciStatus: any;
  @Input() viewValidation: boolean;
  @Input() fetchPreviousData: boolean;

  @Output() keyUpPaci = new EventEmitter();
  @Output() governorateChanged = new EventEmitter();
  @Output() areaChanged = new EventEmitter();
  @Output() blockChanged = new EventEmitter();
  @Output() locationChanged = new EventEmitter<object>();

  @ViewChild('locationForm') locationForm: NgForm;

  @Input() isSubmitted: boolean = false;

  paciNumberNotFound: boolean = false;

  isPaciGovernmentWork: boolean = true;
  isPaciareaWork: boolean = true;
  isPaciBlocksWork: boolean = true;
  isPaciStreetWork: boolean = true;
  governoratesFetchedBefore: boolean = false;

  constructor(private lookup: LookupService) {

  }

  ngOnInit() {
    if (this.paciStatus) {
      this.convertIntoTextInputs = false;
    }
    else {
      this.convertIntoTextInputs = true
    };
  }


  /**
   * 
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    if ('fetchPreviousData' in changes && 'currentValue' in changes.fetchPreviousData) {
      const fetchPreviousDataValue = changes.fetchPreviousData.currentValue;

      if (!this.isEmpty(fetchPreviousDataValue)) {
        if (!this.governoratesFetchedBefore) {
          this.getAllGovs(fetchPreviousDataValue);
        }
        //this.ValidateLocation();
      } else {
        this.getAllGovs(fetchPreviousDataValue);
      }
      this.ValidateLocation();
      this.locationChanged.emit(this.location);

    }
  }


  /**
   * 
   * @param obj 
   */
  isEmpty(obj: Object): boolean {
    if (typeof obj === 'object') {
      return Object.keys(obj).length === 0;
    }
    return true;
  }

  OnPaciChanged(event) {
    if (event.target.value.length > 7) {
     this.keyUpPaci.emit();
      this.GetLocationByPaci(this.location.paciNumber);
      this.ValidateLocation();
      this.locationChanged.emit(this.location);

    } else if (event.target.value.length === 0) {
      this.paciNumberNotFound = false;
    }

   

  }

  onGovernorateChanged() {
    
    if (this.location.governorate != 'undefined') {
      this.addresstoggleLoading = true;
      this.getAreasByGovernorate(this.location);
    
    } else {
      this.location.governorate = undefined;
      this.location.areas = [];
      this.location.area = undefined;
      this.location.blocks = [];
      this.location.block = undefined;
      this.location.streets = [];
      this.location.street = undefined;
    }
    this.ValidateLocation();
    this.locationChanged.emit(this.location);
  }

  onAreaChange() {
   
    if (this.location.area != 'undefined') {
      this.addresstoggleLoading = true;
      this.getBlocksByArea(this.location);
   
    } else {
      this.location.area = undefined;
      this.location.blocks = [];
      this.location.block = undefined;
      this.location.streets = [];
      this.location.street = undefined;
    }
    this.ValidateLocation();
    this.locationChanged.emit(this.location);
  }

  onBlockChange() {
  
    if (this.location.block != 'undefined') {
      this.addresstoggleLoading = true;
      this.getStreetsByBlock(this.location);
     } else {
      this.location.block = undefined;
      this.location.streets = [];
      this.location.street = undefined;
    }
    this.ValidateLocation();
    this.locationChanged.emit(this.location);
  }

  onLocationTitleChange() {
    this.ValidateLocation();
    this.locationChanged.emit(this.location);
  }



  onStreetChange() {
    this.ValidateLocation();
    this.locationChanged.emit(this.location);

  }



  getAreasByGovernorate(singleLocation) {

    this.location.areas = [];
    this.location.blocks = [];
    this.location.streets = [];

    this.location.area = undefined;
    this.location.block = undefined;
    this.location.street = undefined;
    if (this.location != undefined) {
      let id = this.selectId(this.location.governorate, this.governorates);


      if (this.isPaciGovernmentWork) {


        this.lookup.GetallAreas(id)
          .subscribe(
            (areas) => {

              if (areas == undefined || areas == null || areas['length'] == 0 || areas[0].name == 'Not Found In Paci') {

                this.isPaciareaWork = false;
                this.location.blocks = [];
                this.location.streets = [];

              }
              else {
                this.location.areas = areas;
                this.isPaciareaWork = true;
              }
              this.addresstoggleLoading = false;

            }, err => {
              this.addresstoggleLoading = false;
              this.isPaciareaWork = false;
              this.location.blocks = [];
              this.location.streets = [];
            }
          );


      }
      else {
        this.lookup.getAreaswithoutPaci(id)
          .subscribe(
            (areas) => {
              this.location.areas = areas;
              this.addresstoggleLoading = false;
              this.isPaciareaWork = false;
              this.location.blocks = [];
              this.location.streets = [];
            }, err => {
              this.addresstoggleLoading = false;
              this.isPaciareaWork = false;
              this.location.blocks = [];
              this.location.streets = [];
            }
          );

      }
    }
  }

  selectId(name, list) {
    if (name != undefined) {
      var res = list.filter((item) => {
        return item.name == name;
      })[0];

      if (res != undefined) {
        return res.id;
      }
      else {
        return name;
      }
    }
  }


  getBlocksByArea(singleLocation) {

    this.location.blocks = [];
    this.location.streets = [];

    this.location.block = undefined;
    this.location.street = undefined;

    let id = this.selectId(singleLocation.area, singleLocation.areas);

    this.lookup.GetallBlocks(id)
      .subscribe(
        (blocks) => {
          if (blocks == undefined || blocks == null || blocks['length'] == 0 || blocks[0].name == 'Not Found In Paci') {
            this.isPaciBlocksWork = false;
            this.location.streets = [];

          } else {
            this.location.blocks = blocks;
            this.isPaciBlocksWork = true;
          }

          this.addresstoggleLoading = false;
        }, err => {
          this.isPaciBlocksWork = false;

          this.addresstoggleLoading = false;
        }
      );
  }

  getStreetsByBlock(singleLocation) {
    this.location.streets = [];
    this.location.street = undefined;
    let govId = this.selectId(singleLocation.governorate, this.governorates);
    let areaId = this.selectId(singleLocation.area, singleLocation.areas);
    if (govId != undefined && areaId != undefined) {
      this.lookup.GetallStreets(areaId, singleLocation.block, govId)
        .subscribe(
          (streets) => {

            if (streets == undefined || streets == null || streets['length'] == 0 || streets[0].name == 'Not Found In Paci') {

              this.isPaciStreetWork = false;
              this.location.streets = [];

            } else {
              this.location.streets = streets;
              this.isPaciStreetWork = true;


            }
            this.addresstoggleLoading = false;
          }, err => {
            this.isPaciStreetWork = false;
            this.addresstoggleLoading = false;
          }
        );
    }
  }

  GetLocationByPaci(paciNumber: any) {
    this.addresstoggleLoading = true;
    this.lookup.GetLocationByPaci(paciNumber)
      .subscribe(
        loc => {
          if (loc != null) {
            if (loc['governorate'].name != 'Not Found In Paci') {
              this.location.governorate = loc['governorate'].name;
              this.paciNumberNotFound = false;
            }
            else {
              this.paciNumberNotFound = true;
            }

            if (loc['area'].name != 'Not Found In Paci') {
              this.location.area = loc['area'].name;
              this.location.areas = [];
              this.location.areas.push(loc['area']);
              this.paciNumberNotFound = false;
            }
            else {
              this.paciNumberNotFound = true;
            }

            if (loc['block'].name != 'Not Found In Paci') {
              this.location.block = loc['block'].name;
              this.location.blocks = [];
              this.location.blocks.push(loc['block']);
              this.paciNumberNotFound = false;
            }
            else {
              this.paciNumberNotFound = true;
            }

            if (loc['street'].name != 'Not Found In Paci') {
              this.location.street = loc['street'].name;
              this.location.streets = [];
              this.location.streets.push(loc['street']);
              this.paciNumberNotFound = false;
            }
            else {
              this.paciNumberNotFound = true;
            }
            this.ValidateLocation();
            this.locationChanged.emit(this.location);

          }
          this.addresstoggleLoading = false;
        }, err => {
          //err.status == 401 && this.utilities.unauthrizedAction();
          this.addresstoggleLoading = false;
        }
      );
  }


  ValidateLocation() {
    if (this.location.area &&
      this.location.block &&
      this.location.street &&
      this.location.governorate) {
      this.location.isValid = true;
    }
    else {
      this.location.isValid = false;
    }
  }


  /**
   * 
   * @param location 
   */
  getAllGovs(location?: any) {
    this.addresstoggleLoading = true;
    this.lookup.GetallGovernorates().subscribe(Governorates => {
      this.convertIntoTextInputs = false;
      this.governoratesFetchedBefore = true;

      if (Governorates == undefined || Governorates == null || Governorates['length'] == 0 || Governorates[0].name == 'Not Found In Paci') {

        this.isPaciGovernmentWork = false;
        this.isPaciareaWork = false;
        this.isPaciBlocksWork = false;
        this.isPaciStreetWork = false;
        this.isPaciareaWork = false;
        this.location.areas = [];
        this.location.blocks = [];
        this.location.streets = [];

      }
      else {
        this.governorates = Governorates;
        this.isPaciGovernmentWork = true;
        this.addresstoggleLoading = false;

        if (typeof this.location.governorate !== 'undefined') {
          const check = this.isEmpty(location);

          switch (check) {
            case false:
              this.existedLocation(location);
              break;

            default:
              let InputLocation = this.location;
              this.existedLocation(InputLocation);
              break;
          }

        }
      }

      if (Governorates[0].name == 'Not Found In Paci' && this.governoratesFetchedBefore) {
        this.getAllGovernments();
        this.paciStatus = true;
      } else {
        this.governorates = Governorates;
      }
    },
      err => {
        this.convertIntoTextInputs = true;
        this.paciStatus = false;
        this.isPaciGovernmentWork = false;

        if (!this.governoratesFetchedBefore) {
          this.getAllGovernments();
        }
      });
  }



  getAllGovernments() {
    this.lookup.getAllGovernorateswithoutPaci().subscribe(res => {
      this.governorates = res;

      if (this.location.governorate != 'undefined') {
        let InputLocation = this.location;
        this.existedLocation(InputLocation);
      }
    });
  }

  existedLocation(singleLocation) {
    this.addresstoggleLoading = true;

    let id = this.selectId(singleLocation.governorate, this.governorates);

    this.lookup.GetallAreas(id)
      .subscribe(
        (areas) => {
          if (areas == undefined || areas == null || areas['length'] == 0 || areas[0].name == 'Not Found In Paci') {
            this.isPaciareaWork = false;
            this.location.blocks = [];
            this.location.streets = [];
            this.addresstoggleLoading = false;
          }
          else {

            this.location.areas = areas;
            this.isPaciareaWork = true;

            if (this.location.area != 'undefined') {

              let areaid = this.selectId(this.location.area, this.location.areas);
              this.lookup.GetallBlocks(areaid)
                .subscribe(
                  (blocks) => {
                    if (blocks == undefined || blocks == null || blocks['length'] == 0 || blocks[0].name == 'Not Found In Paci') {
                      this.isPaciBlocksWork = false;
                      this.location.streets = [];
                      this.addresstoggleLoading = false;
                    }
                    else {
                      this.location.blocks = blocks;
                      this.isPaciBlocksWork = true;
                      if (this.location.block != 'undefined') {
                        let govId = this.selectId(singleLocation.governorate, this.governorates);
                        let areaId = this.selectId(singleLocation.area, singleLocation.areas)

                        this.selectId(singleLocation.block, singleLocation.blocks)

                        if (govId != undefined && areaId != undefined) {
                          this.lookup.GetallStreets(areaId, singleLocation.block, govId)
                            .subscribe(
                              (streets) => {


                                if (streets == undefined || streets == null || streets['length'] == 0 || streets[0].name == 'Not Found In Paci') {

                                  this.isPaciStreetWork = false;
                                  this.location.streets = [];

                                } else {
                                  this.location.streets = streets;
                                  this.isPaciStreetWork = true;

                                }
                                this.addresstoggleLoading = false;

                              }, err => {
                                this.isPaciStreetWork = false;
                                if (this.location.street != 'undefined') {
                                  this.location.street = singleLocation.street;
                                }
                                this.addresstoggleLoading = false;

                              }
                            );
                        }

                      }


                    }




                  }, err => {
                    this.isPaciBlocksWork = false;


                    if (this.location.block != 'undefined') {
                      this.location.block = singleLocation.block;
                    }
                    this.addresstoggleLoading = false;
                  }
                );

            }


          }



        }, err => {
          this.addresstoggleLoading = false;
          this.isPaciareaWork = false;
          this.location.blocks = [];
          this.location.streets = [];
          if (this.location.area != 'undefined') {

            this.location.area = singleLocation.area;
          }
        }
      );


  }




}
