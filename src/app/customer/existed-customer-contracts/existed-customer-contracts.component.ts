import { Component, OnInit, Input } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { Page } from '../../shared-module/shared/model/page';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { GenerateContractModalComponent } from "../generate-contract-modal/generate-contract-modal.component";

@Component({
  selector: 'app-existed-customer-contracts',
  templateUrl: './existed-customer-contracts.component.html',
  styleUrls: ['./existed-customer-contracts.component.css']
})

export class ExistedCustomerContractsComponent implements OnInit {
  @Input() id;
  contracts: any[];
  getContractByCustomerIdSubscription: Subscription;
  page: Page = new Page();
  toggleLoading: boolean;

  constructor(private lookup: LookupService,
    private modalService: NgbModal) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }


  ngOnInit() {
    this.contracts = [];
    this.getCustomerContracts({ offset: 0 });

  }

  getCustomerContracts(pageNumber) {
    this.toggleLoading = true;

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.id = this.id;

    this.getContractByCustomerIdSubscription = this.lookup.getContractByCustomerIdWithPaging(objToPost).subscribe((contracts) => {
      this.page.totalElements = contracts.totalCount;
      this.contracts = contracts.result;
      this.toggleLoading = false;;
    });

    this.toggleLoading = false;;
  }

  /**
   * add new contract
   * 
   * 
   */
  openAddContractModal() {
    const contractModal = this.modalService.open(GenerateContractModalComponent, { size: 'lg' });
    contractModal.result.then((res) => {
    });

  }

}
