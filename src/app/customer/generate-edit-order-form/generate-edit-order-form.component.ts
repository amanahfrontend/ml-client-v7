import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy
} from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-generate-edit-order-form',
  templateUrl: './generate-edit-order-form.component.html',
  styleUrls: ['./generate-edit-order-form.component.css']
})

export class GenerateEditOrderFormComponent implements OnInit, OnDestroy {

  @Output() order = new EventEmitter;
  @Output() close = new EventEmitter;

  @Input() existedOrder: any;
  @Input() isCashCall: boolean;
  @Input() customerId: any;
  @Input() disabled: boolean;
  @Input() expired: boolean;


  newOrder: any;
  orderTypeSubscription: Subscription;
  orderStatusSubscription: Subscription;
  orderPrioritySubscription: Subscription;
  orderProblemSubscription: Subscription;
  customerLocationsSubscription: Subscription;

  todayDate: any;
  orderTypes: any[];
  statuses: any[];
  orderPriority: any[];
  customerLocations: any[];
  problems: any[];
  isNewOrder: boolean;
  orderTypeCashCall: any[] = [];

  today: Date;
  end: Date;
  minDate;

  minDateForEndDate: Date;
  selectedCashCallId: number;

  constructor(
    private lookup: LookupService,
  ) {
    this.today = new Date();
  }

  ngOnInit() {

    if (this.existedOrder) {
      this.isNewOrder = false;
      this.newOrder = this.existedOrder;

      this.newOrder['startDate'] = this.formatDate(this.newOrder['startDate']);
      this.newOrder['endDate'] = this.formatDate(this.newOrder['endDate']);

      this.end = this.newOrder['startDate'];
      this.minDateForEndDate = new Date(this.existedOrder.createdDate);
      this.customerId = this.existedOrder.fK_Customer_Id;

    } else {
      this.isNewOrder = true;
      this.newOrder = {};

      this.newOrder.fK_OrderStatus_Id = 0;
    }



    this.lookup.getOrderType().subscribe((orderTypes) => {
      this.orderTypes = orderTypes;
      if (this.isCashCall) {
        let orderCashCall = this.orderTypes.find(ord => ord.name == "Cash Call");
        this.newOrder.fK_OrderType_Id = orderCashCall.id;
        this.orderTypeCashCall.push(orderCashCall);
      } else {
        this.orderTypes = this.orderTypes.filter(ord => ord.name != "Cash Call");
      }
    });

    this.orderPrioritySubscription = this.lookup.getOrderPriority().subscribe((orderPriority) => {
      this.orderPriority = orderPriority;
    });

    this.orderProblemSubscription = this.lookup.getAllProblems().subscribe((problems) => {
      this.problems = problems;
    });

    this.customerLocationsSubscription = this.lookup.getLocationByCustomerId(this.customerId).subscribe((locations) => {
      this.customerLocations = locations;
    });

    this.orderStatusSubscription = this.lookup.getOrderStatus().subscribe((status) => {
      this.statuses = status;
    });
  }

  submitOrder(order) {
    // if (!this.isCashCall) {
    //   Object.keys(order).map(() => {
    //     order['startDate'] = this.formatDate(order['startDate']);
    //     order['endDate'] = this.formatDate(order['endDate']);
    //   });
    // }
    // else {
    //   order['startDate'] = this.formatDate(new Date());
    //   order['endDate'] = this.formatDate(new Date());
    // }

    Object.keys(order).map(() => {
      order['startDate'] = this.formatDate(order['startDate']);
      order['endDate'] = this.formatDate(order['endDate']);
    });


    if(this.expired){
      order['fK_OrderType_Id'] = this.newOrder.fK_OrderType_Id;
    }

    this.order.emit(order);
  }

  closeModal() {
    this.close.emit();
  }


  ngOnDestroy() {
    this.orderPrioritySubscription.unsubscribe();
    this.customerLocationsSubscription.unsubscribe();
  }

  onChange(event) {
  }


  onStartDateChange(event) {
    // this.minDateForEndDate = this.newOrder.startDate;
    this.end = this.newOrder.startDate;
    this.newOrder.endDate = undefined;
  }

  /**
   * 
   * @param date 
   */
  formatDate(date: Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

}
