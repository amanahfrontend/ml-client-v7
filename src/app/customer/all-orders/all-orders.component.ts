import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { Message } from 'primeng/components/common/api';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Page } from '../../shared-module/shared/model/page';
import { AuthService } from './../../services/auth.service';
import { AlertService } from './../../services/alert.service';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css'],
})

export class AllOrdersComponent implements OnInit, OnDestroy {
  allOrdersSubscription: Subscription;
  searchSubscription: Subscription;
  allOrders: any[];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  page = new Page();
  rows = new Array<any>();

  isAdmin: boolean = false;

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private alertService: AlertService
  ) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
    this.getAllOrders({ offset: 0 });
  }

  /**
   * 
   * @param pageNumber 
   */
  getAllOrders(pageNumber) {
    this.toggleLoading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;


    this.allOrdersSubscription = this.lookup.getAllOrdersByPaging(objToPost).subscribe((allOrders) => {
      this.rows = allOrders.result;
      this.page.totalElements = allOrders.totalCount;
      this.allOrders = allOrders.result;
      this.toggleLoading = false;

      //console.log(this.allOrders);
      this.bigMessage = [];

      this.bigMessage.push({
        severity: "info",
        summary: "Dropdown arrow",
        detail: "You can remove any order by click on the drop down arrow and choose remove."
      });
    }, (err) => {
      this.toggleLoading = false;
      err.status == 401 && this.utilities.unauthrizedAction();
      this.alertService.alertSuccess({ title: 'Failed', details: 'Failed to get data due to network error, please try again later.' })
    });
  }

  ngOnDestroy() {
    this.allOrdersSubscription.unsubscribe();
    this.searchSubscription && this.searchSubscription.unsubscribe();
  }

  searchByValue(searchText) {
    this.toggleLoading = true;
    this.utilities.paginationFlag = true;
    let pageNumber;

    if (searchText.searchedParmaters == null || searchText.searchedParmaters == undefined)
      this.utilities.searchedValue = searchText;
    else {
      pageNumber = searchText.pageNumber;
      searchText = this.utilities.searchedValue;

    }
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = searchText;


    this.searchSubscription = this.lookup.searchOrderNumberBYpaging(objToPost).subscribe((searchResult) => {
      this.allOrders = searchResult.result;
      this.page.totalElements = searchResult.totalCount;

      this.toggleLoading = false;

      if (searchResult.orderViewModelList !== undefined && searchResult.orderViewModelList.length == 0) {
        this.alertService.alertError({ title: 'Failed', details: 'Result not found.' });
      }
    },
      err => {
        this.toggleLoading = false;
        this.alertService.alertError({ title: 'Failed', details: 'Failed to get data due to network error, please try again later.' });
      })
  }


  /**
   * 
   * @param event 
   */
  refresh(event) {
    this.getAllOrders({ offset: 0 });
  }

}
