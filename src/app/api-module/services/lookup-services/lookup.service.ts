// import {allCustomerLookup} from './../../models/lookups-modal';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import {Observable} from 'rxjs/Observable';
import * as myGlobals from "../globalPath";
import { UtilitiesService } from "../utilities/utilities.service";
import { Subject, Observable } from "rxjs";

@Injectable()
export class LookupService {
  orderPostData: any;
  messageSubject = new Subject<any>();

  constructor(private http: HttpClient, private utilities: UtilitiesService) { }
  /*new services Mohamed Ragab*/

  getAllPreventive():Observable<any> {
    return this.http.get(myGlobals.allPreventive);
  }


  GetAllPreventiveMaintainencePaging(pagingData):Observable<any> {
    return this.http.post(myGlobals.allPreventivePaging, pagingData);
  }


  getPriorities() :Observable<any>{
    return this.http.get(myGlobals.callPriorities);
  }

  getPhoneTypes() :Observable<any>{
    return this.http.get(myGlobals.phoneTypes);
  }

  getCustomerTypes() :Observable<any>{
    return this.http.get(myGlobals.customerTypes);
  }

  getCallsHistory() :Observable<any>{
    return this.http.get(myGlobals.callsHistory)
  }

  callsHistoryByPaging(pagingparametermodel: any) :Observable<any>{
    return this.http.post(myGlobals.callsHistoryByPaging, pagingparametermodel);
  }


  getCallsTypes() :Observable<any>{
    return this.http.get(myGlobals.callsTypes);
  }

  getCallLogs(id) :Observable<any>{
    return this.http.get(myGlobals.callsLogs + id);
  }

  getCustomerCall(id) :Observable<any>{
    return this.http.get(myGlobals.GetCustomerDetailsByID + id);
  }

  getStatues() :Observable<any>{
    return this.http.get(myGlobals.status);
  }

  getItem(code) :Observable<any>{
    return this.http.get(myGlobals.item + code);
  }

  getAllEstimations():Observable<any> {
    return this.http.get(myGlobals.estimations);
  }

  searchByNameOrPhone(searchPhrase) :Observable<any>{
    return this.http.get(myGlobals.searchByPhoneOrName + searchPhrase);
  }

  getEstimationDetails(id) :Observable<any>{
    return this.http.get(myGlobals.estimationDetails + id)
  }

  getAllQuotations() :Observable<any>{
    return this.http.get(myGlobals.quotations);
  }

  getAllQuotationsBypaging(pagingData) :Observable<any>{
    return this.http.post(myGlobals.quotationsByPaging, pagingData);
  }

  getQuoutationByRefernceNo(searchText):Observable<any> {
    return this.http.get(myGlobals.searchQuotation + searchText);
  }

  getQuotationDetailsById(id) :Observable<any>{
    return this.http.get(myGlobals.quotationbyId + id);
  }

  getQuotationDetailsByRefNo(refNo) :Observable<any>{
    return this.http.get(myGlobals.quotationbyRefNo + refNo);
  }

  getContractTypes() :Observable<any>{
    return this.http.get(myGlobals.contractTypes);
  }

  searchContract(refNo) :Observable<any>{
    return this.http.get(myGlobals.searchContract + refNo);
  }

  searchContractBypaging(objToPost) :Observable<any>{
    return this.http.post(myGlobals.searchContractByPaging, objToPost);
  }

  getAllContracts() :Observable<any>{
    return this.http.get(myGlobals.allContracts);
  }

  getAllContractsByPaging(objToPost):Observable<any> {
    return this.http.post(myGlobals.allContractsByPaging, objToPost)
  }

  getContractById(id):Observable<any> {
    return this.http.get(myGlobals.contractById + id);
  }

  getRoles() :Observable<any>{
    return this.http.get(myGlobals.roles);
  }

  getUsers():Observable<any> {
    return this.http.get(myGlobals.users);
  }

  IsAllowToDeleteAdmin(): Observable<any> {
    return this.http.get(myGlobals.IsAllowToDeleteAdmin);
  }
  getOrderType() :Observable<any>{
    return this.http.get(myGlobals.orderTypes)
  }

  getOrderPriority():Observable<any> {
    return this.http
      .get(myGlobals.orderPriorities)
  }

  getAllOrders() :Observable<any>{
    return this.http
      .get(myGlobals.allOrders)
  }
  getAllOrdersByPaging(pagingparametermodel: any) :Observable<any>{
    return this.http
      .post(myGlobals.allOrders, pagingparametermodel)
  }

  searchByOrderNumber(number):Observable<any> {
    return this.http
      .get(myGlobals.searchOrders + number)
  }

  searchOrderNumberBYpaging(number) :Observable<any>{
    return this.http
      .post(myGlobals.searchOrders, number)
  }

  getOrderStatus() :Observable<any>{
    return this.http
      .get(myGlobals.orderStatus)
  }

  getContractByCustomerId(id) :Observable<any>{
    return this.http
      .get(myGlobals.contractByCustomer + id)
  }

  getContractByCustomerIdWithPaging(contractPaging):Observable<any> {
    return this.http
      .post(myGlobals.GetByCustomerByPaging, contractPaging)
  }

  getOrderByCustomerId(id) :Observable<any>{
    return this.http
      .get(myGlobals.orderByCustomer + id)
  }
  getOrderByCustomerIdWithPaging(orderPaging) :Observable<any>{
    return this.http
      .post(myGlobals.getOrderByCustomerByPaging, orderPaging)
  }


  getCallByCustomerId(id) :Observable<any>{
    return this.http
      .get(myGlobals.callrByCustomer + id)
  }

  getCallByCustomerIdBypaging(pagingCallData) :Observable<any>{
    return this.http
      .post(myGlobals.getCallByCustomerIdByPaging, pagingCallData)
  }

  getLocationByCustomerId(id):Observable<any> {
    return this.http
      .get(myGlobals.locationByCustomer + id)
  }

  getDispatcherOrders(id) :Observable<any>{
    return this.http
      .get(myGlobals.ordersByDis + id)
  }

  getDispatchers() :Observable<any>{
    return this.http
      .get(myGlobals.allDispatchers)
  }

  getTecs():Observable<any> {
    return this.http
      .get(myGlobals.unAssignedTecs)
  }

  getAllTecs():Observable<any> {
    return this.http
      .get(myGlobals.allTecs)
  }

  getTecsByDis(id) :Observable<any>{
    return this.http
      .get(myGlobals.assignedTecs + id)
  }


  getAssignedDispbyTechId(id) :Observable<any>{
    return this.http
      .get(myGlobals.assignedDispatureForTech + id)
  }


  getOrderProgress(id):Observable<any>{
    return this.http
      .get(myGlobals.orderProgressUrl + id)
  }

  getProgressStatuses() :Observable<any>{
    return this.http
      .get(myGlobals.progressStatuses)
  }

  getDispatchersWithProb() :Observable<any>{
    return this.http
      .get(myGlobals.getDiByProb)
  }

  getAllProblems() :Observable<any>{
    return this.http
      .get(myGlobals.allProbs)
  }

  getDispatcherAllProblems(id):Observable<any> {
    return this.http
      .get(myGlobals.getDisWithAllProbs + id)
  }

  getAllVehicles() :Observable<any>{
    return this.http
      .get(myGlobals.getAllVehicles)
  }

  getDisAllOrders(id):Observable<any> {
    return this.http
      .get(myGlobals.getOrdersByDis + id)
  }

  IsOrdersAllowDeleteDispature(id): Observable<any> {
    return this.http
      .get(myGlobals.IsOrdersAllowDeleteDispature + id)
  }

  IsOrdersAllowDeleteTechnician(id): Observable<any> {
    return this.http
      .get(myGlobals.IsOrdersAllowDeleteTechnician + id)
  }

 

  getTecAssignedItems(id) :Observable<any>{
    return this.http
      .get(myGlobals.assignedItems + id)
  }

  IsInventoryAllowDeleteTech(id): Observable<any> {
    return this.http
      .get(myGlobals.IsInventoryAllowDeleteTech + id)
  }

  getTecAssignedItemsPagginated(obj) :Observable<any>{
    return this.http
      .post(myGlobals.assignedItemsPagginated, obj)
  }


  getTecReportUsedItems(id) :Observable<any>{
    return this.http
      .get(myGlobals.usedItems + id)
  }

  getAllCustomers():Observable<any> {
    return this.http
      .get(myGlobals.allCustomers)
  }

  getAllCustomersByPaging(pagingparametermodel: any):Observable<any> {
    return this.http.post(myGlobals.allCustomersbyPaging, pagingparametermodel);
  }

  searchCustmerByPaging(objToPost) :Observable<any>{
    return this.http.post(myGlobals.searchCustomer, objToPost);
  }

  getAllItems(obj):Observable<any> {
    return this.http
      .post(myGlobals.getAllItems, obj)
  }

  downloadQuotationWord(id) :Observable<any>{
    return this.http.get(myGlobals.quotationWordUrl + id)
  }

  getAllOrderProgress() :Observable<any>{
    return this.http.get(myGlobals.allOrderProgressUrl)
  }

  getAllOrderProblems() :Observable<any>{
    return this.http.get(myGlobals.allOrderProblemsUrl)
  }

  getCustomerDetails(customerId) :Observable<any>{
    return this.http.get(myGlobals.getCustomerDetails + customerId)
  }

  getAllMachineTypes() :Observable<any>{
    return this.http.get(myGlobals.allMachineTypes)
  }

  postNewDispatureAreasAndProblem(dispatureAreasAndProblems):Observable<any> {
    return this.http
      .post(myGlobals.addDispatureAreasAndProplem, dispatureAreasAndProblems)
  }

  postNewLog(newLog):Observable<any> {
    return this.http
      .post(myGlobals.logPostUrl, newLog)
  }

  postEstimation(estimations):Observable<any> {
    return this.http.post(myGlobals.estimationPostUrl, estimations)
  }

  postNewCall(call):Observable<any> {
    return this.http.post(myGlobals.callPostUrl, call)
  }

  generateQuotation(estimation) :Observable<any>{
    return this.http
      .post(myGlobals.generateQuotationUrl, estimation)
  }

  postNewContract(newContract) :Observable<any>{
    return this.http
      .post(myGlobals.generateContractUrl, newContract)
  }

  postNewUser(newUser):Observable<any> {
    return this.http
      .post(myGlobals.newUserUrl, newUser)
  }

  postItemsExcelSheet(excelSheetObject):Observable<any> {
    return this.http
      .post(myGlobals.itemsExcelSheetUrl, excelSheetObject)
  }

  postSalaryExcelSheet(excelSheetObject) :Observable<any>{
    return this.http
      .post(myGlobals.salaryExcelSheetUrl, excelSheetObject)
  }

  postNewOrder(newOrder) :Observable<any>{
    return this.http
      .post(myGlobals.postNewOrderUrl, newOrder)
  }

  postCustomerTypes(customerType):Observable<any> {
    return this.http
      .post(myGlobals.postCustomerTypesUrl, customerType)
  }

  postCallsTypes(callType):Observable<any> {
    return this.http
      .post(myGlobals.postCallsTypesUrl, callType)
  }

  postCallPriorities(callPriority) :Observable<any>{
    return this.http
      .post(myGlobals.postCallPrioritiesUrl, callPriority)
  }

  postPhoneTypes(phoneType):Observable<any> {
    return this.http
      .post(myGlobals.postPhoneTypesUrl, phoneType)
  }

  postActionStatues(status) :Observable<any>{
    return this.http
      .post(myGlobals.postActionStateUrl, status)
  }

  postContractTypes(contractType):Observable<any>{
    return this.http
      .post(myGlobals.postContractTypeUrl, contractType)
  }

  postRoles(role) :Observable<any>{
    return this.http
      .post(myGlobals.postRoleUrl, role)
  }

  postOrderType(orderType) :Observable<any>{
    return this.http
      .post(myGlobals.postOrderTypeUrl, orderType)
  }

  postOrderPriority(orderPriority):Observable<any> {
    return this.http
      .post(myGlobals.postOrderPriorityUrl, orderPriority)
  }

  postOrderStatus(orderState) :Observable<any>{
    return this.http
      .post(myGlobals.postOrderStateUrl, orderState)
  }

  postTecnichanWithDis(tecWithDis) :Observable<any>{
    return this.http
      .post(myGlobals.postTecForDist, tecWithDis)
  }

  postOrderToTec(orderByTec) :Observable<any>{
    return this.http
      .post(myGlobals.orderByTec, orderByTec)
  }

  postUnAssignedOrder(id):Observable<any> {
    return this.http
      .get(myGlobals.unAssignedOrder + id)
  }

  postTecAvailability(availability):Observable<any> {
    return this.http
      .post(myGlobals.tecAvailable, availability)
  }

  postVisitTime(date):Observable<any> {
    return this.http
      .post(myGlobals.postPreferedVisitDate, date)
  }

  postTransferOrder(transferToDispatcher) :Observable<any>{
    return this.http
      .post(myGlobals.transferToDispatcherUrl, transferToDispatcher)
  }

  postProgress(progress) :Observable<any>{
    return this.http
      .post(myGlobals.postProgressUrl, progress)
  }

  postDispatcherWithProblemsAndAreas(DisWithProb) :Observable<any>{
    return this.http
      .post(myGlobals.postDispatcherWithProblemsUrl, DisWithProb)
  }

  filterOrders(filterObj):Observable<any> {
    return this.http
      .post(myGlobals.filterOrdersUrl, filterObj)
  }

  filterOrdersMap(filterObj) :Observable<any>{
    return this.http
      .post(myGlobals.filterOrdersMapUrl, filterObj)
  }

  assignItemToTec(obj):Observable<any> {
    return this.http
      .post(myGlobals.assignItemUrl, obj)
  }

  postCustomer(obj) :Observable<any>{
    return this.http
      .post(myGlobals.postCustomerUrl, obj)
  }


  updateCaller(obj):Observable<any> {
    return this.http
      .put(myGlobals.callUpdateUrl, obj)
  }

  assignOrdersToDispatcher(obj) :Observable<any>{
    return this.http
      .post(myGlobals.assignOrdersToDispatcherUrl, obj)
  }

  assignOrdersToTec(obj) :Observable<any>{
    return this.http
      .post(myGlobals.assignOrdersToTecUrl, obj)
  }

  searchAssignedItems(searchObject) :Observable<any>{
    return this.http
      .post(myGlobals.assignedItemSearch, searchObject)
  }

  postOrderProgress(objectProgress):Observable<any> {
    return this.http
      .post(myGlobals.postOrderProgress, objectProgress)
  }

  postOrderProblems(objectProblems):Observable<any> {
    return this.http
      .post(myGlobals.postOrderProblems, objectProblems)
  }

  postMachineTypes(object) :Observable<any>{
    return this.http
      .post(myGlobals.postMachineTypes, object)
  }

  filterUsedItems(filterObject) :Observable<any>{
    return this.http
      .post(myGlobals.filterUsedItems, filterObject)
  }

  filterPreventive(filterObject) :Observable<any>{
    return this.http
      .post(myGlobals.filterPreventiveUrl, filterObject)
  }

  FilterPreventiveMaintainencePagingUrl(filterObject) :Observable<any>{
    return this.http
      .post(myGlobals.FilterPreventiveMaintainencePagingUrl, filterObject)
  }

  filterEstimations(filterObject):Observable<any> {
    return this.http
      .post(myGlobals.filterEstimationsUrl, filterObject)
  }

  filterQuotations(filterObject):Observable<any> {
    return this.http
      .post(myGlobals.filterQuotationsUrl, filterObject)
  }
  filterQuotationsByPaging(filterObject) :Observable<any>{
    return this.http
      .post(myGlobals.filterQuotationsPagingUrl, filterObject)
  }

  postNewLocation(location) :Observable<any>{
    return this.http
      .post(myGlobals.postNewCustomerLocationUrl, location)
  }

  /*DELETE*/

  deleteEstimation(id):Observable<any> {
    return this.http
      .delete(myGlobals.deletEstimationUrl + id)
  }

  deleteQuotation(id):Observable<any> {
    return this.http
      .delete(myGlobals.deleteQuotationUrl + id)
  }


  /**
   * delete contract
   * 
   * 
   * @param id 
   */
  deleteDispatureAreasAndProblems(id) :Observable<any>{
    return this.http
      .delete(myGlobals.DeleteDispatureAreasAndProplem + id)
  }




  deleteContract(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteContractUrl + id)
  }

  deleteCustomertype(id):Observable<any> {
    return this.http
      .delete(myGlobals.deleteCustomertypeUrl + id)
  }

  deleteCallType(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteCallTypeUrl + id)
  }

  deletePriorities(id):Observable<any> {
    return this.http
      .delete(myGlobals.deletePrioritiesUrl + id)
  }

  deletePhoneTypes(id):Observable<any> {
    return this.http
      .delete(myGlobals.deletePhoneTypeUrl + id)
  }

  deleteStatus(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteStatusUrl + id)
  }

  deleteContractType(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteContractTypeUrl + id)
  }

  deleteRole(id):Observable<any> {
    return this.http
      .delete(myGlobals.deleteRoleUrl + id)
  }

  lockUnlockUser(id):Observable<any> {
    return this.http
      .delete(myGlobals.lockUnLockUserUrl + id)
  }

  removeUser(id):Observable<any> {
    return this.http
      .delete(myGlobals.removeUserUrl + id)
  }

  deleteOrder(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteOrderUrl + id)
  }

  deleteOrderType(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteOrderTypeUrl + id)
  }

  deleteOrderPriority(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteOrderPriorityUrl + id)
  }

  deleteOrderStatus(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteOrderStatusUrl + id)
  }

  deleteCall(id: number) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteCall + id)
  }




  removeTecFromDis(id) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteTecFromDis + id)
  }

  removeDisByProb(id) :Observable<any>{
    return this.http
      .delete(myGlobals.removeDisWithProb + id)
  }

  removeProgressStatus(id) :Observable<any>{
    return this.http
      .delete(myGlobals.removeProgressStatusUrl + id)
  }

  removeProblem(id) :Observable<any>{
    return this.http
      .delete(myGlobals.removeProblemUrl + id)
  }

  removeMachineType(id) :Observable<any>{
    return this.http
      .delete(myGlobals.removeMachineType + id)
  }

  /*UPDATE*/


  updateDispatureAreasAndProblemsLookUp(problem):Observable<any>{
    return this.http
      .put(myGlobals.updateDispatureAreasAndProplem, problem)
  }


  updateEstimation(estimation):Observable<any> {
    return this.http
      .put(myGlobals.updateEstimationUrl, estimation)
  }

  updateCustomerTypes(customerType) :Observable<any>{
    return this.http
      .put(myGlobals.updateCustomerTypeUrl, customerType)
  }

  updateCallsTypes(callType):Observable<any> {
    return this.http
      .put(myGlobals.updateCallTypeUrl, callType)
  }

  updatePriorities(priority) :Observable<any>{
    return this.http
      .put(myGlobals.updatePriorityUrl, priority)
  }

  updatePhoneTypes(phoneType):Observable<any> {
    return this.http
      .put(myGlobals.updatePhoneTypeUrl, phoneType)
  }

  updateStatues(status):Observable<any> {
    return this.http
      .put(myGlobals.updateStatusUrl, status)
  }

  updateContractTypes(contractType):Observable<any> {
    return this.http
      .put(myGlobals.updateContractTypeUrl, contractType)
  }

  updateRoles(role) :Observable<any>{
    return this.http
      .post(myGlobals.updateRoleUrl, role)
  }

  updateUser(userData) :Observable<any>{
    return this.http
      .post(myGlobals.updateUserUrl, userData)
  }

  updateOrder(order):Observable<any> {
    return this.http
      .put(myGlobals.updateOrderUrl, order)
  }

  updateOrderStatus(status) :Observable<any>{
    return this.http
      .put(myGlobals.updateOrderStatusUrl, status)
  }

  updateOrderPriority(priority) :Observable<any>{
    return this.http
      .put(myGlobals.updateOrderPriorityUrl, priority)
  }

  updateOrderType(type) :Observable<any>{
    return this.http
      .put(myGlobals.updateOrderTypeUrl, type)
  }

  updateOrderProgressLookUp(progress) :Observable<any>{
    return this.http
      .put(myGlobals.updateOrderProgressUrl, progress)
  }

  updateOrderProblemsLookUp(problem) :Observable<any>{
    return this.http
      .put(myGlobals.updateOrderProblemsUrl, problem)
  }

  updateCustomer(customerData):Observable<any> {
    return this.http
      .put(myGlobals.updateCustomerUrl, customerData)
  }

  /**
   * 
   * @param customerId 
   */
  deleteCustomer(customerId: number) :Observable<any>{
    return this.http
      .delete(myGlobals.deleteCustomer + customerId)
  }


  updateLocation(location):Observable<any> {
    return this.http
      .post(myGlobals.updateLocationUrl, location)
  }

  updateMachineType(machineType) :Observable<any>{
    return this.http
      .put(myGlobals.updateMachineTypeUrl, JSON.stringify(machineType));
  }

  /*-----------------------------------------------*/

  GetallGovernorates() :Observable<any>{
    return this.http
      .get(myGlobals.allGovernorates)
  }

  GetallAreas(Gov: number) :Observable<any>{
    return this.http
      .get(myGlobals.allAreas + "?govId=" + Gov)
  }

  GetallBlocks(area: number):Observable<any> {
    return this.http
      .get(myGlobals.allBlocks + "?areaId=" + area)
  }

  GetallStreets(area: number, block: string, gov: number) :Observable<any>{
    return this.http
      .get(
        myGlobals.allStreets +
        "?govId=" +
        gov +
        "&areaId=" +
        area +
        "&blockName=" +
        block      )
  }

  GetLocationByPaci(PACI: number) :Observable<any>{
    return this.http
      .get(myGlobals.allGetLocationByPaci + PACI)
  }


  /**
   * all goverments 
   * 
   * 
   */
  getAllGovernorateswithoutPaci():Observable<any> {
    return this.http
      .get(myGlobals.getAllGovernorateswithoutPaci)
  }


  /**
   * area via government id
   * 
   * 
   * @param govId 
   */
  getAreaswithoutPaci(govId: number):Observable<any> {
    return this.http
      .get(myGlobals.getAreaswithoutPaci + govId)
  }

  getWebsiteSettingsBykey() :Observable<any>{
    return this.http
      .get(myGlobals.websiteSettingsGetKey)
  }


  /**
   * 
   * @param key 
   */
  setWebsiteSettings(object: Object) :Observable<any>{
    return this.http.post(myGlobals.websiteSettingsSet, object)
  }


  /**
   * 
   * @param value 
   */
  searchInCustomers(value: string) :Observable<any>{
    return this.http.get(myGlobals.searchCustomerByName + '/' + value)
  }

}
