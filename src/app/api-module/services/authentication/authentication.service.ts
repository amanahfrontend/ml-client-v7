import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, BehaviorSubject } from "rxjs";
import * as myGlobals from '../globalPath';
import { Router } from '@angular/router';

export enum types {
  order = 1,
  call = 2
}

@Injectable()

export class AuthenticationService {

  isLoggedin = new BehaviorSubject(<boolean>false);
  userRoles = new Subject<string[]>();

  constructor(private http: HttpClient, private router: Router) {
  }

  setLoggedIn(value) {
    this.isLoggedin.next(value);
  }

  setRoles(value) {
    this.userRoles.next(value);
  }

  login(username: string, password: string) {
    this.http.post(myGlobals.BaseUrlUserManagement + 'token',
      {
        userName: username,
        password: password
      }
    ).subscribe(user => {
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.setLoggedIn(true);
      this.setRoles(user['roles']);
      this.router.navigate(['/']);
    });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.isLoggedin.next(false);
    this.router.navigate(['login']);
  }

  isLoggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }

  getToken() {
    if (localStorage.getItem('currentUser')) {
      return JSON.parse(localStorage.getItem('currentUser')).token.accessToken;
    } else {
      return null;
    }
  }

  CurrentUser() {
    if (localStorage.getItem("currentUser") == null) {
      this.setLoggedIn(false);
      return null;
    }
    else {
      this.setLoggedIn(true);
      this.setRoles(JSON.parse(localStorage.getItem("currentUser")).roles);
      return JSON.parse(localStorage.getItem('currentUser'));
    }
  }


}
