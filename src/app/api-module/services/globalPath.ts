'use strict';


export const API_Key = 'AIzaSyDJiuQ2hD0H4plorriRdU_VDaenjNsl_7E';

// /*****************************************************************************************
//  *  Localhost
//  *****************************************************************************************/
//  export const BaseUrl = 'http://localhost/';
//  export const MainPath = 'http://localhost/';
//  export const BaseUrlCall = BaseUrl + 'EnmaaCall/api/';
//  export const BaseUrlCustomer = BaseUrl + 'EnmaaCustomer/api/';
//  export const BaseUrlContract = BaseUrl + 'EnmaaContract/api/';
//  export const BaseUrlOrder = BaseUrl + 'EnmaaOrder/api/';
//  export const BaseUrlEstimationQuotation = BaseUrl + 'EnmaaEstimationQuotation/api/';
//  export const BaseUrlSalary = BaseUrl + 'EnmaaEmployeeSalary/api/';
//  export const BaseUrlInventory = BaseUrl + 'EnmaaInventory/api/';
//  export const BaseUrlUserManagement = BaseUrl + 'EnmaaUserManagment/api/';
//  export const BaseUrlVehicle = BaseUrl + 'EnmaaVehicle/api/';
//  export const baseUrlSettings = 'EnmaaCustomer/api';


/*****************************************************************************************
 *  ML DEV
 *****************************************************************************************/
// export const BaseUrl = '';
// export const MainPath = '';
// export const BaseUrlCall = 'http://amanahadmin-001-site11.itempurl.com/api/';
// export const BaseUrlContract = 'http://amanahadmin-001-site25.itempurl.com/api/';
// export const BaseUrlCustomer = 'http://amanahadmin-001-site41.itempurl.com/api/';
// export const BaseUrlSalary = 'http://amanahadmin-001-site43.itempurl.com/api/';
// export const BaseUrlInventory = 'http://amanahadmin-001-site45.itempurl.com/api/';
// export const BaseUrlUserManagement = 'http://amanahadmin-001-site46.itempurl.com/api/';
// export const baseUrlSettings = 'http://amanahadmin-001-site46.itempurl.com/api/';
// export const BaseUrlOrder = 'http://amanahadmin-001-site50.itempurl.com/api/';
// export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site40.itempurl.com/api/';
// export const BaseUrlVehicle = 'http://amanahadmin-001-site1.itempurl.com/api/';


// /*****************************************************************************************
//  *  Gulf ENG
//  *****************************************************************************************/
// export const BaseUrl = '';
// export const MainPath = '';
// export const BaseUrlCall = 'http://amanahadmin-001-site67.itempurl.com/api/';
// export const BaseUrlCustomer = 'http://amanahadmin-001-site69.itempurl.com/api/';
// export const BaseUrlContract = 'http://amanahadmin-001-site68.itempurl.com/api/';
// export const BaseUrlOrder = 'http://amanahadmin-001-site75.itempurl.com/api/';
// export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site72.itempurl.com/api/';
// export const BaseUrlSalary = 'http://amanahadmin-001-site71.itempurl.com/api/';
// export const BaseUrlInventory = 'http://amanahadmin-001-site74.itempurl.com/api/';
// export const BaseUrlUserManagement = 'http://amanahadmin-001-site76.itempurl.com/api/';
// export const BaseUrlVehicle = 'http://amanahadmin-001-site77.itempurl.com/api/';
// export const baseUrlSettings = 'http://amanahadmin-001-site76.itempurl.com/api/';


/*****************************************************************************************
 *  Dasco
 *****************************************************************************************/
//  export const BaseUrl = '';
//  export const MainPath = '';
//  export const BaseUrlCall = 'http://amanahadmin-001-site27.itempurl.com/api/';
//  export const BaseUrlCustomer = 'http://amanahadmin-001-site29.itempurl.com/api/';
//  export const BaseUrlContract = 'http://amanahadmin-001-site28.itempurl.com/api/';
//  export const BaseUrlOrder = 'http://amanahadmin-001-site35.itempurl.com/api/';
//  export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site32.itempurl.com/api/';
//  export const BaseUrlSalary = 'http://amanahadmin-001-site31.itempurl.com/api/';
//  export const BaseUrlInventory = 'http://amanahadmin-001-site34.itempurl.com/api/';
//  export const BaseUrlUserManagement = 'http://amanahadmin-001-site36.itempurl.com/api/';
//  export const BaseUrlVehicle = 'http://amanahadmin-001-site37.itempurl.com/api/';
//  export const baseUrlSettings = 'http://amanahadmin-001-site36.itempurl.com/api/';


/*****************************************************************************************
 *  Enmaa
 *****************************************************************************************/
// export const BaseUrl = '';
// export const MainPath = '';
// export const BaseUrlCall = 'http://amanahadmin-001-site14.itempurl.com/api/';
// export const BaseUrlCustomer = 'http://amanahadmin-001-site16.itempurl.com/api/';
// export const BaseUrlContract = 'http://amanahadmin-001-site15.itempurl.com/api/';
// export const BaseUrlOrder = 'http://amanahadmin-001-site21.itempurl.com/api/';
// export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site18.itempurl.com/api/';
// export const BaseUrlSalary = 'http://amanahadmin-001-site17.itempurl.com/api/';
// export const BaseUrlInventory = 'http://amanahadmin-001-site20.itempurl.com/api/';
// export const BaseUrlUserManagement = 'http://amanahadmin-001-site23.itempurl.com/api/';
// export const BaseUrlVehicle = 'http://amanahadmin-001-site22.itempurl.com/api/';
// export const baseUrlSettings = 'http://amanahadmin-001-site23.itempurl.com/api/';


/*****************************************************************************************
 *  Gulf Group
 *****************************************************************************************/
 export const BaseUrl = '';
 export const MainPath = '';
 export const BaseUrlCall = 'http://amanahadmin-001-site83.itempurl.com/api/';
 export const BaseUrlCustomer = 'http://amanahadmin-001-site85.itempurl.com/api/';
 export const BaseUrlContract = 'http://amanahadmin-001-site48.itempurl.com/api/';
 export const BaseUrlOrder = 'http://amanahadmin-001-site90.itempurl.com/api/';
 export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site88.itempurl.com/api/';
 export const BaseUrlSalary = 'http://amanahadmin-001-site87.itempurl.com/api/';
 export const BaseUrlInventory = 'http://amanahadmin-001-site89.itempurl.com/api/';
 export const BaseUrlUserManagement = 'http://amanahadmin-001-site91.itempurl.com/api/';
 export const BaseUrlVehicle = 'http://amanahadmin-001-site93.itempurl.com/api/';
 export const baseUrlSettings = 'http://amanahadmin-001-site91.itempurl.com/api/';



/*****************************************************************************************
 *  Arfelon
 //*****************************************************************************************/
//export const BaseUrl = '';
//export const MainPath = '';
//export const BaseUrlCall = 'http://amanahadmin-001-site96.atempurl.com/api/';
//export const BaseUrlCustomer = 'http://amanahadmin-001-site97.itempurl.com/api/';
//export const BaseUrlContract = 'http://amanahadmin-001-site98.itempurl.com/api/';
//export const BaseUrlOrder = 'http://amanahadmin-001-site65.itempurl.com/api/';
//export const BaseUrlEstimationQuotation = 'http://amanahadmin-001-site81.itempurl.com/api/';
//export const BaseUrlSalary = 'http://amanahadmin-001-site100.itempurl.com/api/';
//export const BaseUrlInventory = 'http://amanahadmin-001-site61.itempurl.com/api/';
//export const BaseUrlUserManagement = 'http://amanahadmin-001-site59.itempurl.com/api/';
//export const BaseUrlVehicle = 'http://amanahadmin-001-site66.itempurl.com/api/';
//export const baseUrlSettings = 'http://amanahadmin-001-site59.itempurl.com/api/';

//Debug:
//export const BaseUrlUserManagement = "http://localhost:50699/api/";

/***************************************/
/***************************************/
// SignalR function
/***************************************/

export function vehicleHubUrl(userId, roles, token) {
  return BaseUrl + 'Vehicles/dispatchingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token
}

export function quotationHubUrl(userId, roles, token) {
  return BaseUrl + 'Estimating/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function callHubUrl(userId, roles, token) {
  return BaseUrl + 'Call/callingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function orderHubUrl(userId, roles, token) {
  return BaseUrl + 'Order/orderingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

// http://localhost/EnmaaEstimationQuotation/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token

/***************************************/
//
/*---JSON mocked - Mohamed ragab---*/
export const SearchCustomer = BaseUrlCall + 'Call/Search/';
export const SearchCustomerByPaging = BaseUrlCall + 'Call/SearchByPaging/';

export const callPriorities = BaseUrlCall + 'CallPriority'; // Json mocking
export const deleteCall = BaseUrlCall + 'Call/Delete/'; // Json mocking

export const callsTypes = BaseUrlCall + 'CallType'; // Json mocking
export const customerTypes = BaseUrlCustomer + 'CustomerType/GetAll'; // Json mocking
export const phoneTypes = BaseUrlCustomer + 'PhoneType/GetAll'; // Json mocking
export const locationByCustomer = BaseUrlCustomer + 'location/GetLocationByCustomerId?customerId='; // Json mocking
export const callsHistory = BaseUrlCall + 'Call/GetAll'; // Json mocking
export const callsHistoryByPaging = BaseUrlCall + 'Call/GetALLByPaging'; // Json mocking



export const callsLogs = BaseUrlCall + 'CallLog/GetByCall/'; // Json mocking
export const status = BaseUrlCall + 'CallStatus'; // Json mocking
export const item = BaseUrlEstimationQuotation + 'EstimationItem/SearchItems/'; // Json mocking
export const itemSearch = BaseUrlInventory + 'item/GetByCode/'; // Json mocking
export const estimations = BaseUrlEstimationQuotation + 'Estimation'; // Json mocking
export const searchByPhoneOrName = BaseUrlEstimationQuotation + 'Estimation/Search/'; // Json mocking
export const estimationDetails = BaseUrlEstimationQuotation + 'Estimation/Get/'; // Json mocking
export const quotations = BaseUrlEstimationQuotation + 'Quotation/GetAll'; // Json mocking
export const quotationsByPaging = BaseUrlEstimationQuotation + 'Quotation/GetAllByPaging'; // Json mocking

export const searchQuotation = BaseUrlEstimationQuotation + 'Quotation/Search/'; // Json mocking
export const quotationbyId = BaseUrlEstimationQuotation + 'Quotation/Get/'; // Json mocking
export const quotationbyRefNo = BaseUrlEstimationQuotation + 'Quotation/GetByRef/'; // Json mocking
export const contractTypes = BaseUrlContract + 'ContractType/GetAll'; // Json mocking
export const orderTypes = BaseUrlOrder + 'OrderType/GetAll'; // Json mocking
export const orderPriorities = BaseUrlOrder + 'OrderPriority/GetAll'; // Json mocking
export const searchContract = BaseUrlContract + 'Contract/search/'; // Json mocking
export const searchContractByPaging = BaseUrlContract + 'Contract/SearchBypaging/'; // Json mocking

export const allContracts = BaseUrlContract + 'Contract/GetAll'; // Json mocking
export const allContractsByPaging = BaseUrlContract + 'Contract/GetAllContractsByPaging'; // Json mocking


export const contractById = BaseUrlContract + 'Contract/Get/'; // Json mocking
export const roles = BaseUrlUserManagement + 'Role/getall'; // Json mocking
export const users = BaseUrlUserManagement + 'User/getall'; // Json mocking

export const IsAllowToDeleteAdmin = BaseUrlUserManagement + 'User/IsAllowToDeleteAdmin'; // Json mocking



export const allOrders = BaseUrlOrder + 'Order/GetAll'; // Json mocking
export const searchOrders = BaseUrlOrder + 'Order/Search/'; // Json mocking
export const orderStatus = BaseUrlOrder + 'OrderStatus/GetAll'; // Json mocking
export const searchCustomer = BaseUrlCustomer + 'Customer/SearchCustomer'; // Json mocking

export const allPreventive = BaseUrlContract + 'Contract/GetAllPreventiveMaintainence';
export const allPreventivePaging = BaseUrlContract + 'Contract/GetAllPreventiveMaintainencePaging';

export const GetCallDetails = BaseUrlCall + 'Call/Get/';

export const GetCustomerDetailsByID = BaseUrlCall + 'Call/Get/';

export const contractByCustomer = BaseUrlContract + 'Contract/GetByCustomer/';
export const GetByCustomerByPaging = BaseUrlContract + 'Contract/GetByCustomerByPaging/';


export const orderByCustomer = BaseUrlOrder + 'Order/SearchByCustomerId/';
export const getOrderByCustomerByPaging = BaseUrlOrder + 'Order/GetOrderByCustomerByPaging/';

export const callrByCustomer = BaseUrlCall + 'Call/Search/';
export const getCallByCustomerIdByPaging = BaseUrlCall + 'Call/GetCallByCustomerIdByPaging/';

export const allLocation = BaseUrlCustomer + 'location/getall';

export const allGovernorates = BaseUrlCustomer + 'location/GetAllGovernorates';

export const allAreas = BaseUrlCustomer + 'location/GetAreas';

export const allBlocks = BaseUrlCustomer + 'location/GetBlocks';

export const allStreets = BaseUrlCustomer + 'location/GetStreets';

export const allGetLocationByPaci = BaseUrlCustomer + 'location/GetLocationByPaci?paciNumber=';

export const allDispatchers = BaseUrlUserManagement + 'User/GetDispatchers';

export const unAssignedTecs = BaseUrlOrder +
  'AssignedTechnicans/GetUnAssignedTechnicans';

export const assignedTecs = BaseUrlOrder + 'AssignedTechnicans/GetTechnicansByDispatcherId/';


export const assignedDispatureForTech = BaseUrlOrder + 'AssignedTechnicans/GetAssignedDispatureForTechnican/';

export const ordersByDis = BaseUrlOrder + 'AssignedTechnicans/GetDispatcher/';

export const assignedItems = BaseUrlInventory + 'TechnicanAssignedItems/GetAssignedItemsByTechnican/';

export const IsInventoryAllowDeleteTech = BaseUrlInventory + 'TechnicanAssignedItems/IsInventoryAllowDeleteTech/';

export const assignedItemsPagginated = BaseUrlInventory + 'TechnicanAssignedItems/GetAssignedItemsByTechnicanPaging';


export const usedItems = BaseUrlInventory + 'TechnicanUsedItems/GetUsedItemsByTechnican/';

export const allTecs = BaseUrlUserManagement + 'User/GetTechnicans';

export const allCustomers = BaseUrlCustomer + 'Customer/GetAll';
export const allCustomersbyPaging = BaseUrlCustomer + 'Customer/GetAllByPaging';

/*----- Started 12/12 services mocks -----*/

export const orderProgressUrl = BaseUrlOrder + 'OrderProgress/GetByOrderId/';

export const progressStatuses = BaseUrlOrder + 'ProgressStatus/GetAll';

// export const getDiByProb = BaseUrlOrder + 'OrderDistributionCriteria/GetAllDistribution';

export const getDiByProb = BaseUrlOrder + 'DispatureAreasAndProplems/GetAll';
export const addDispatureAreasAndProplem = BaseUrlOrder + 'DispatureAreasAndProplems/Add';
export const updateDispatureAreasAndProplem = BaseUrlOrder + 'DispatureAreasAndProplems/Update';
export const DeleteDispatureAreasAndProplem = BaseUrlOrder + 'DispatureAreasAndProplems/Delete/';

export const allProbs = BaseUrlOrder + 'OrderProblem/GetAll';

export const removeDisWithProb = BaseUrlOrder + '/OrderDistributionCriteria/Delete/';

export const getDisWithAllProbs = BaseUrlOrder + 'OrderDistributionCriteria/GetByDispatcherId/';

export const getAllVehicles = BaseUrlVehicle + 'Vehicle/GetVehicles';

export const getAllItems = BaseUrlInventory + 'Item/GetAllByPaging';

export const allOrderProgressUrl = BaseUrlOrder + 'ProgressStatus/GetAll';

export const allOrderProblemsUrl = BaseUrlOrder + 'OrderProblem/GetAll';

export const getCustomerDetails = BaseUrlCustomer + 'Customer/Get/';

export const allMachineTypes = BaseUrlCustomer + 'MachineType/GetAll';

/*-------------------post--------------------*/

export const logPostUrl = BaseUrlCall + 'CallLog/Add'; // Json mocking
export const estimationPostUrl = BaseUrlEstimationQuotation + 'Estimation/Add'; // Json mocking
export const callPostUrl = BaseUrlCall + 'Call/Add'; // Json mocking
export const callUpdateUrl = BaseUrlCall + 'Call/Update'; // Json mocking

export const generateQuotationUrl = BaseUrlEstimationQuotation + 'Quotation/Add'; // Json mocking
export const generateContractUrl = BaseUrlContract + 'Contract/Add'; // Json mocking
export const newUserUrl = BaseUrlUserManagement + 'User/Add'; // Json mocking
export const itemsExcelSheetUrl = BaseUrlInventory + 'ProcessItem/UpdateSalaries'; // Json mocking
export const salaryExcelSheetUrl = BaseUrlSalary + 'ProcessEmployeeSalary/UpdateSalaries'; // Json mocking
export const postNewOrderUrl = BaseUrlOrder + 'Order/Add'; // Json mocking
export const postCustomerTypesUrl = BaseUrlCustomer + 'CustomerType/Add'; // Json mocking
export const postCallsTypesUrl = BaseUrlCall + 'CallType/Add'; // Json mocking
export const postCallPrioritiesUrl = BaseUrlCall + 'CallPriority/Add'; // Json mocking
export const postPhoneTypesUrl = BaseUrlCustomer + 'PhoneType/Add'; // Json mocking
export const postActionStateUrl = BaseUrlCall + 'CallStatus/Add'; // Json mocking
export const updateStatusUrl = BaseUrlCall + 'CallStatus/Update';

export const postContractTypeUrl = BaseUrlContract + 'ContractType/Add'; // Json mocking
export const postRoleUrl = BaseUrlUserManagement + 'Role/Add'; // Json mocking
export const postOrderTypeUrl = BaseUrlOrder + 'OrderType/Add'; // Json mocking
export const postOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Add'; // Json mocking
export const postOrderStateUrl = BaseUrlOrder + 'OrderStatus/Add'; // Json mocking
export const postTecForDist = BaseUrlOrder + 'AssignedTechnicans/Add'; // Json mocking
export const orderByTec = BaseUrlOrder + 'Order/AssignOrderToTechnican';
export const unAssignedOrder = BaseUrlOrder + 'Order/UnAssignOrderToTechnican/';
export const tecAvailable = BaseUrlUserManagement + 'User/ChangeUserAvailability';

// new urls 12/12/2017
export const postPreferedVisitDate = BaseUrlOrder + 'Order/SetPreferedVisitTime';
export const transferToDispatcherUrl = BaseUrlOrder + 'Order/TransferOrderToDispatcher';
export const postProgressUrl = BaseUrlOrder + 'Order/AddOrderProgress';
export const postDispatcherWithProblemsUrl = BaseUrlOrder + '/OrderDistributionCriteria/AssignCriteriaToDispatchers';
export const filterOrdersUrl = BaseUrlOrder + 'Order/GetFilteredOrderByDispatcherId';
export const filterOrdersMapUrl = BaseUrlOrder + 'Order/GetMapFilteredOrderByDispatcherId';
export const getOrdersByDis = BaseUrlOrder + 'AssignedTechnicans/GetOrderByDispatcher/';

export const IsOrdersAllowDeleteDispature = BaseUrlOrder + 'Order/CanDeleteDispature/';
export const IsOrdersAllowDeleteTechnician = BaseUrlOrder + 'Order/CanDeleteTechnician/';


export const assignItemUrl = BaseUrlInventory + 'TechnicanAssignedItems/AssignItem';

export const quotationWordUrl = BaseUrlEstimationQuotation + 'Quotation/GenerateWord/';

export const filterUsedItems = BaseUrlInventory + 'TechnicanUsedItems/FilterPaging';

// ***********************filter preventive***************

export const filterPreventiveUrl = BaseUrlContract + 'Contract/FilterPreventiveMaintainence';

export const FilterPreventiveMaintainencePagingUrl = BaseUrlContract + 'Contract/FilterPreventiveMaintainencePaging';

export const filterEstimationsUrl = BaseUrlEstimationQuotation + 'Estimation/Filter';

export const filterQuotationsUrl = BaseUrlEstimationQuotation + 'Quotation/Filter';
export const filterQuotationsPagingUrl = BaseUrlEstimationQuotation + 'Quotation/FilterByPaging';

export const postCustomerUrl = BaseUrlCustomer + 'Customer/Add';

export const assignOrdersToDispatcherUrl = BaseUrlOrder + 'Order/AssignOrdersToDispatcher';

export const assignOrdersToTecUrl = BaseUrlOrder + 'Order/AssignOrdersToTechnican';

export const assignedItemSearch = BaseUrlInventory + 'TechnicanAssignedItems/ChekItemExistForTechnican';

export const postOrderProgress = BaseUrlOrder + 'ProgressStatus/Add';

export const postOrderProblems = BaseUrlOrder + 'OrderProblem/Add';

export const postNewCustomerLocationUrl = BaseUrlCustomer + 'location/add';

export const postMachineTypes = BaseUrlCustomer + 'MachineType/Add';

export const removeProgressStatusUrl = BaseUrlOrder + 'ProgressStatus/Delete/';

export const removeProblemUrl = BaseUrlOrder + 'OrderProblem/Delete/';

export const removeMachineType = BaseUrlCustomer + 'MachineType/Delete/';


/*-------------- Delete -------------------*/

export const deletEstimationUrl = BaseUrlEstimationQuotation + 'Estimation/Delete/'; // Json mocking
export const deleteQuotationUrl = BaseUrlEstimationQuotation + 'Quotation/Delete/'; // Json mocking
export const deleteContractUrl = BaseUrlContract + 'Contract/Delete/'; // Json mocking
export const deleteCustomertypeUrl = BaseUrlCustomer + 'CustomerType/Delete/'; // Json mocking
export const deleteCallTypeUrl = BaseUrlCall + 'CallType/Delete/'; // Json mocking
export const deletePrioritiesUrl = BaseUrlCall + 'CallPriority/Delete/'; // Json mocking
export const deletePhoneTypeUrl = BaseUrlCustomer + 'PhoneType/Delete/'; // Json mocking
export const deleteStatusUrl = BaseUrlCall + 'CallStatus/Delete/'; // Json mocking
export const deleteContractTypeUrl = BaseUrlContract + 'ContractType/Delete/'; // Json mocking
export const deleteRoleUrl = BaseUrlUserManagement + 'Role/Delete/'; // Json mocking
export const lockUnLockUserUrl = BaseUrlUserManagement + 'User/ToggleDelete/'; // Json mocking
export const removeUserUrl = BaseUrlUserManagement + 'User/Delete/'; // Json mocking

export const deleteOrderUrl = BaseUrlOrder + 'Order/Delete/'; // Json mocking
export const deleteOrderStatusUrl = BaseUrlOrder + 'OrderStatus/Delete/'; // Json mocking
export const deleteOrderTypeUrl = BaseUrlOrder + 'OrderType/Delete/'; // Json mocking
export const deleteOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Delete/'; // Json mocking
export const deleteTecFromDis = BaseUrlOrder + 'AssignedTechnicans/DeleteAssignedTechnican/'; // Json mocking

/*----------------- PUT -------------------*/

export const updateEstimationUrl = BaseUrlEstimationQuotation + 'Estimation/Update';
export const updateCustomerTypeUrl = BaseUrlCustomer + 'CustomerType/Update';
export const updateCallTypeUrl = BaseUrlCall + 'CallType/Update';
export const updatePriorityUrl = BaseUrlCall + 'CallPriority/Update';
export const updatePhoneTypeUrl = BaseUrlCustomer + 'PhoneType/Update';
export const updateContractTypeUrl = BaseUrlContract + 'ContractType/Update';
export const updateRoleUrl = BaseUrlUserManagement + 'Role/Update';
export const updateUserUrl = BaseUrlUserManagement + 'User/Update';
export const updateOrderUrl = BaseUrlOrder + 'Order/Update';
export const updateOrderStatusUrl = BaseUrlOrder + 'OrderStatus/Update';
export const updateOrderPriorityUrl = BaseUrlOrder + 'OrderPriority/Update';
export const updateOrderTypeUrl = BaseUrlOrder + 'OrderType/Update';
export const updateOrderProgressUrl = BaseUrlOrder + 'ProgressStatus/Update';
export const updateOrderProblemsUrl = BaseUrlOrder + 'OrderProblem/Update';
export const updateCustomerUrl = BaseUrlCustomer + 'Customer/Update';
export const updateLocationUrl = BaseUrlCustomer + 'location/update';
export const updateMachineTypeUrl = BaseUrlCustomer + 'MachineType';


//*************************************************************
//*************************************************************
//*************************************************************
// -------------------- Customers URLs ------------------------

export const SearchCustomerByPhoneAndCID = MainPath + '/api/customer/SearchCustomer?searchToken=';
export const CreatenewCustomer = MainPath + '/api/customer/CreateDetailedCustomer';
// export const GetCustomerDetailsByID = MainPath + '/api/customer/CustomerHistory/';
export const EditCustomerInfo = MainPath + '/api/customer/Update';
export const EditContract = MainPath + '/api/contract/Update';
export const AddContract = MainPath + '/api/contract/add';
export const AddNewWorkOrder = MainPath + '/api/WorkOrder/AddWorkOrder';
export const AddLocation = MainPath + '/api/location/add';

export const EditComplain = MainPath + '/api/complain/Update';
export const AddComplain = MainPath + '/api/complain/add';
export const EditWorkOrder = MainPath + '/api/WorkOrder/EditWorkOrder';

//-------------------- Lookups URls -----------------------------
export const allCustomerTypes = MainPath + '/api/CustomerType/getall';
export const allContractType = MainPath + '/api/ContractType/getall';
export const allWorkItems = MainPath + '/api/Item/GetAll';
// export const allLocation = MainPath + '/api/location/getall';
export const allFactories = MainPath + '/api/Factory/GetAll';
// export const allGovernorates = MainPath + '/api/location/GetAllGovernorates';

// export const allAreas = MainPath + '/api/location/GetAreas';
// export const allBlocks = MainPath + '/api/location/GetBlocks';
// export const allStreets = MainPath + '/api/location/GetStreets';
// export const allGetLocationByPaci = MainPath + '/api/location/GetLocationByPaci?paciNumber=';
export const allStatus = MainPath + '/api/Status/getall';


export const editStatus = MainPath + '/api/Status/Update';
export const AddStatus = MainPath + '/api/Status/Add';
export const DeleteStatus = MainPath + '/api/Status/Delete';

export const allRole = MainPath + '/api/Role/getall';
export const editRole = MainPath + '/api/Role/Update';
export const AddRole = MainPath + '/api/Role/Add';
export const DeleteRole = MainPath + '/api/Role/Delete';

export const allPeriorities = MainPath + '/api/Priority/getall';
export const editPeriorities = MainPath + '/api/Priority/Update';
export const AddPeriorities = MainPath + '/api/Priority/Add';
export const DeletePeriorities = MainPath + '/api/Priority/Delete';

export const editCustomerType = MainPath + '/api/CustomerType/Update';
export const AddCustomerType = MainPath + '/api/CustomerType/Add';
export const DeleteCustomerType = MainPath + '/api/CustomerType/Delete';

export const editContractType = MainPath + '/api/ContractType/Update';
export const AddContractType = MainPath + '/api/ContractType/Add';
export const DeleteContractType = MainPath + '/api/ContractType/Delete';

export const allEquipmentType = MainPath + '/api/EquipmentType/getall';
export const editEquipmentType = MainPath + '/api/EquipmentType/Update';
export const AddEquipmentType = MainPath + '/api/EquipmentType/Add';
export const DeleteEquipmentType = MainPath + '/api/EquipmentType/Delete';

export const allEquipment = MainPath + '/api/Equipment/getall';
export const editEquipment = MainPath + '/api/Equipment/Update';
export const AddEquipment = MainPath + '/api/Equipment/Add';
export const DeleteEquipment = MainPath + '/api/Equipment/Delete';

export const editItem = MainPath + '/api/Item/Update';
export const AddItem = MainPath + '/api/Item/Add';
export const DeleteItem = MainPath + '/api/Item/Delete';

export const editFactory = MainPath + '/api/Factory/Update';
export const AddFactory = MainPath + '/api/Factory/Add';
export const DeleteFactory = MainPath + '/api/Factory/Delete';

export const alluser = MainPath + '/api/User/getall';
export const Adduser = MainPath + '/api/User/Add';
export const Deleteuser = MainPath + '/api/User/Delete';

export const DeleteComplain = MainPath + '/api/complain/delete?id=';
export const DeleteContract = MainPath + '/api/contract/delete?id=';
export const DeleteWorkOrder = MainPath + '/api/workorder/delete?id=';

export const UsernameExists = MainPath + '/api/User/UsernameExists?username=';
export const EmailExists = MainPath + '/api/User/EmailExists?email=';
// ---------------------------Work Order Management page -------------------------

export const AllWorkOrdersDispature = MainPath + '/api/WorkOrder/GetAllSuborders';
export const AllWorkOrdersMovementController = MainPath + '/api/WorkOrder/GetByMovementController';
export const AvailableEquipmentInFactory = MainPath + '/api/Equipment/GetAvailableEquipmentInFactory?factoryId=';
export const UpdateAssignmentToWorkOrder = MainPath + '/api/Equipment/UpdateAssignmentToWorkOrder';
export const UpdateSubOrder = MainPath + '/api/WorkOrder/UpdateSubOrder';
export const reassinFactory = MainPath + '/api/WorkOrder/CanAssignToFactory?workorderId=';
export const AssignmentToFactory = MainPath + '/api/WorkOrder/UpdateOrderAssignmentToFactory?orderId=';
export const JobDetails = MainPath + '/api/job/GetByWorkOrder?workorderId=';
export const AvailableVehicleInFactory = MainPath + '/api/vehicle/GetAvailableVehicleInFactory?factoryId=';
export const AddJop = MainPath + '/api/job/add';
export const CancelJop = MainPath + '/api/job/Cancel?jobId=';
export const ReassignJop = MainPath + '/api/job/Reassign?jobId=';
export const AllWorkOrder = MainPath + '/api/WorkOrder/getall';
export const checkCustomerNameExist = MainPath + '/api/customer/IsCustomerNameExist?name=';
export const checkCustomerphoneExist = MainPath + '/api/customer/IsPhoneExist?phone=';
export const AllVehicles = MainPath + '/api/vehicle/getall';
export const deleteCustomer = BaseUrlCustomer + 'Customer/Delete/';
export const NotificationOrders = MainPath + '/api/WorkOrder/getall';
export const RemindOrders = MainPath + '/api/WorkOrder/getall';
export const getAllGovernorateswithoutPaci = BaseUrlCustomer + 'location/GetAllGovernorateswithoutPaci';
export const getAreaswithoutPaci = BaseUrlCustomer + 'location/GetAreaswithoutPaci?govId=';
export const websiteSettingsGetKey = baseUrlSettings + 'WebsiteSetting/get';
export const websiteSettingsSet = baseUrlSettings + 'WebsiteSetting/Add';
export const searchCustomerByName = BaseUrlCustomer + 'Customer/SearchCustomerByName';

