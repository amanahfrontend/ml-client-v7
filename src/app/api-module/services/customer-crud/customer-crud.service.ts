import {
  CustomerHistory,
  customer,
  contract,
  complain,
  WorkOrderDetails
} from './../../models/customer-model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as myGlobals from '../globalPath';

@Injectable()
export class CustomerCrudService {
  searchedValue: any;

  constructor(private http: HttpClient) { }


  paginationFlag: boolean = false;

  //SearchCustomerByPaging

  GoSearchCustomer(SearchField: string):Observable<any> {
    return this.http.get(myGlobals.SearchCustomer + `${SearchField}`);
  }

  SearchCustomerByPaging(SearchField: any) :Observable<any>{
    return this.http.post(myGlobals.SearchCustomerByPaging, SearchField);
  }

  addCustomer(customer: any) :Observable<any>{
    return this.http.post(myGlobals.CreatenewCustomer, customer);
  }

  GetCallDetail(Id: number): Observable<CustomerHistory> {
    return this.http.get(myGlobals.GetCallDetails + `${Id}`);
  }

  EditCustomerInfo(customer: customer) :Observable<any>{
    return this.http.post(myGlobals.EditCustomerInfo, customer);
  }
  EditContract(contract: contract) :Observable<any>{
    return this.http.post(myGlobals.EditContract, contract);
  }
  AddContract(contract: contract) :Observable<any>{
    return this.http.post(myGlobals.AddContract, contract);
  }

  EditComplain(complain: complain) :Observable<any>{
    return this.http.post(myGlobals.EditComplain, complain);
  }
  AddComplain(complain: complain) :Observable<any>{
    return this.http.post(myGlobals.AddComplain, complain);
  }

  EditWorkOrderServ(WorkOrderDetails: WorkOrderDetails) :Observable<any>{
    return this.http.post(myGlobals.EditWorkOrder, WorkOrderDetails);
  }
  AddNewWorkOrder(WorkOrderDetails: WorkOrderDetails) :Observable<any>{

    return this.http.post(myGlobals.AddNewWorkOrder, WorkOrderDetails);
  }

  AddNewLocation(location: any) :Observable<any>{
    return this.http.post(myGlobals.AddLocation, location);
  }

  DeleteComplain(Id: any) :Observable<any>{
    return this.http.delete(myGlobals.DeleteComplain + `${Id}`);
  }

  DeleteWorkOrder(Id: any) :Observable<any>{
    return this.http.delete(myGlobals.DeleteWorkOrder + `${Id}`);
  }

  DeleteContract(Id: any) :Observable<any>{
    return this.http.delete(myGlobals.DeleteContract + `${Id}`);
  }

  GetAllWorkOrdersDispature(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrdersDispature);
  }

  GetAllWorkOrdersMovementController(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrdersMovementController);
  }

  GetAvailableEquipmentInFactory(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AvailableEquipmentInFactory + `${Id}`);
  }

  UpdateAssignmentToWorkOrder(newEquip: any):Observable<any> {
    return this.http.post(myGlobals.UpdateAssignmentToWorkOrder, newEquip);
  }
  
  UpdateSubOrder(newOrder: any):Observable<any> {
    return this.http.post(myGlobals.UpdateSubOrder, newOrder);
  }
 
  ReassinFactory(workorderId: any, factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.reassinFactory + `${workorderId}` + '&factoryId=' + `${factoryId}`);
  }
 
  UpdateOrderAssignmentToFactory(workorderId: any, factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AssignmentToFactory + `${workorderId}` + '&factoryId=' + `${factoryId}`);
  }
 
  GetJobDetails(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.JobDetails + `${Id}`);
  }

  GetAvailableVehicleInFactory(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AvailableVehicleInFactory + `${Id}`);
  }
  
  AddJop(Job: any):Observable<any> {
    return this.http.post(myGlobals.AddJop, Job);
  }

  CancelJop(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.CancelJop + `${Id}`);
  }

  ReassignJop(JobId: any, workorderId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.ReassignJop + `${JobId}` + '&workorderId=' + `${workorderId}`);
  }

  GetAllWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrder);
  }

  checkCustomerNameExist(name: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerNameExist + `${name}`);
  }

  checkCustomerphoneExist(phone: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerphoneExist + `${phone}`);
  }

  GetAllNotificationsWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.NotificationOrders);
  }

  GetAllRemindWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.RemindOrders);
  }
}
