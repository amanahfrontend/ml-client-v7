import { AuthGuardGuard } from './guards/auth-guard.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { LookupService } from './services/lookup-services/lookup.service';
import { AuthInterceptor } from "./../shared-module/shared/auth.interceptor";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    AuthGuardGuard,
    LookupService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],

  declarations: []
})
export class ApiModuleModule { }
