import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
@Injectable()
export class RoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let userRoles = [];
    userRoles.push(... this.authService.user.roles);

    const routeRoles = next.data.roles;

    let found = userRoles.some(role => { return routeRoles.indexOf(role) != -1; });

    if (found) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }


  }
}
