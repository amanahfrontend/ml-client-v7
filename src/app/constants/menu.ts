export interface Item {
    title: string,
    path: string,
    icon: string
    roles: Array<String>
}

export function menu() {
    const items: Item[] = [
        {
            title: 'Admin page',
            path: './admin',
            icon: 'fa-cogs',
            roles: [
                'Admin'
            ]
        },
        {
            title: 'Users',
            path: 'admin/users',
            icon: 'fa-user',
            roles: [
                'Admin'
            ]
        },
        {
            title: 'Start Call',
            path: './search',
            icon: 'fa-user-plus',
            roles: [
                'CallCenter'
            ]
        },
        {
            title: 'Calls history',
            path: './search/calls-history',
            icon: 'fa-phone',
            roles: [
                'CallCenter', 'Maintenance'
            ]
        },
        {
            title: 'Customers',
            path: './search/customersList',
            icon: 'fa-users',
            roles: [
                'CallCenter', 'Maintenance', 'Admin'
            ]
        },
        {
            title: 'Estimations',
            path: './search/estimation',
            icon: 'fa-file-text',
            roles: [
                'Maintenance'
            ]
        },
        {
            title: 'Quotations',
            path: './search/quotation',
            icon: 'fa-usd',
            roles: [
                'Maintenance', 'CallCenter'
            ]
        },
        {
            title: 'Contracts',
            path: './search/contract',
            icon: 'fa-book',
            roles: [
                'CallCenter', 'Admin'
            ]
        },
        {
            title: 'Orders',
            path: './search/order',
            icon: 'fa-briefcase',
            roles: [
                'CallCenter', 'Admin'
            ]
        },
        {
            title: 'Orders Management',
            path: './dispatcher',
            icon: 'fa-briefcase',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        {
            title: 'Preventive Orders',
            path: './dispatcher/preventive',
            icon: 'fa-history',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        // {
        //   title: 'Estimations Report',
        //   path: './search/estimations-report',
        //   icon:'fa-briefcase',
        //   roles: [
        //   'Maintenance','CallCenter'
        //   ]
        // },
        {
            title: 'Report',
            path: './dispatcher/report',
            icon: 'fa-table',
            roles: [
                'SupervisorDispatcher', 'Dispatcher', 'Admin'
            ]
        },
        {
            title: 'Visit Time Calender',
            path: './dispatcher/calender',
            icon: 'fa-calendar',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        {
            title: 'Inventory',
            path: './assign-item',
            icon: 'fa-industry',
            roles: [
                'SupervisorDispatcher', 'Dispatcher', 'Admin'
            ]
        },
    ];

    return items;
}