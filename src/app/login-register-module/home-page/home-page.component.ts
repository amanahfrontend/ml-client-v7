import {
    Component,
    OnInit,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    OnDestroy
} from '@angular/core';
import { AuthenticationService } from "./../../api-module/services/authentication/authentication.service";
import { Item, menu } from './../../constants/menu';


@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class HomePageComponent implements OnInit, OnDestroy {

    currentUser: any;
    roles: any[] = [];
    countMenuItems: number;
    menuClass: string = 'col-md-2';

    items: Item[];

    constructor(
        private authService: AuthenticationService,
        private cdr: ChangeDetectorRef,
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

        setTimeout(() => {
            if (!this.cdr['destroyed']) {
                this.cdr.detectChanges();
            }
        }, 0);

        this.items = menu();
    }

    ngOnInit() {
        this.authService.userRoles.subscribe((roles) => {
            this.roles = roles || [];
            this.compare(this.roles, this.items)
        });
    }

    /**
     * 
     * @param source 
     * @param target 
     */
    containsAny(target) {
        let result = this.roles.filter((item) => {
            return target.indexOf(item) > -1;
        });
        return (result.length > 0);
    }



    compare(arr1, arr2) {
        var result = {}

        arr1.forEach((item) => {
            result[item] = 0
        })

        arr2.forEach((arr) => {
            arr.roles.forEach(element => {
                if (result.hasOwnProperty(element)) {
                    result[element]++
                }
            });
        });

        for (const key in result) {
            if (result.hasOwnProperty(key)) {
                const element = result[key];
                this.countMenuItems = element;
            }
        }

        if (this.countMenuItems > 0 && this.countMenuItems <= 3) {
            this.menuClass = 'col-md-4';
        } else {
            this.menuClass = 'col-md-3';
        }
    }


    ngOnDestroy() {
        this.cdr.detach();
    }

}