import {
  Component,
  OnInit,
} from "@angular/core";
import {
  Router,
  ActivatedRoute
} from "@angular/router";
import { AuthService } from './../../services/auth.service';
import { AuthenticationService } from "./../../api-module/services/authentication/authentication.service";

@Component({
  selector: "app-login-component",
  templateUrl: "./login-component.component.html",
  styleUrls: ["./login-component.component.css"],
  providers: [
    AuthService
  ]
})

export class LoginComponentComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    if (this.authService.authenticated) {
      this.router.navigate(['/']);
    }

    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password);
  }

  modelChanged(event) {
    this.loading = false;
  }
}
