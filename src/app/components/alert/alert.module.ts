import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert.component';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    AlertComponent
  ],
  providers: [
    AlertServiceService
  ],
  exports: [
    AlertComponent
  ]
})

export class AlertModule { }
