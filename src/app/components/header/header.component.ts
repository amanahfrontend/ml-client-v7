import { Router } from '@angular/router';
import { AuthenticationService } from './../../api-module/services/authentication/authentication.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { DataService } from "../../admin/settings/data.service";
import { Title } from "@angular/platform-browser";
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  isLoggedin: boolean = false;
  // toggleNotification: boolean;
  // newNotificationToggle: boolean;
  // notificationBackgroundClass: any[];
  CurentUser: any = {};
  // toggelingNotificationIconInterval: any;
  // quotationSubscription: Subscription;
  callSubscription: Subscription;
  orderSubscription: Subscription;
  // notifications: any[] = [];
  // toggelingNotificationIconClass: string[] = [];

  image: string = '';
  aboutus: string = '';

  @Output() loggedOut = new EventEmitter();

  constructor(
    public authenticationService: AuthenticationService,
    private router: Router,
    private utilities: UtilitiesService,
    private lookupService: LookupService,
    private dataService: DataService,
    private titleService: Title,
    private authService: AuthService
  ) {

    this.dataService.subject.subscribe(msg => {
      for (const key in msg) {
        if (msg.hasOwnProperty(key)) {
          const element = msg[key];
          if (element.key === 'image') {
            this.image = element.value;
          } else if (element.key === 'title') {
            this.titleService.setTitle(element.value);
          }
        }
      }
    });

  }



  ngOnInit() {

    this.getSettings();
    this.isLoggedin = this.authenticationService.isLoggedIn();
    this.CurentUser = this.authService.user;

    // this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    // this.quotationSubscription = this.authenticationService.quotaionSignalR.subscribe((quotation) => {

    //   quotation.type = 'quotation';
    //   this.notifications.push(quotation);
    //   this.newNotificationToggle = true;
    //   this.toggelingNotificationIcon();
    // });

    // this.callSubscription = this.authenticationService.callSignalR.subscribe((call) => {
    //   call.type = 'call';
    //   this.notifications.push(call);
    //   this.newNotificationToggle = true;
    //   this.toggelingNotificationIcon();
    // });

    // // console.log('will subscribe to orders');
    // this.orderSubscription = this.authenticationService.orderSignalR.subscribe((order) => {
    //   order.type = 'order';
    //   this.notifications.push(order);
    //   this.newNotificationToggle = true;
    //   this.toggelingNotificationIcon();
    // },
    //   (err) => {
    //     console.log(err)
    //   });

    // // } else {
    // this.notifications = [];
    // // this.orderSubscription && this.orderSubscription.unsubscribe();
    // // this.callSubscription && this.callSubscription.unsubscribe();
    // // this.quotationSubscription && this.quotationSubscription.unsubscribe();
    // // }
    // // });

  }



  logout() {
    // this.notifications = [];
    // this.newNotificationToggle = false;
    // this.toggelingNotificationIconClass = ['show-notification-icon'];
    // this.stopIntervalNotificationIconToggeling();
    this.loggedOut.emit(true);
    
    this.authService.logout();
    this.router.navigate(['login']);
  }

  // toggleNotificationBody() {
  //   this.toggleNotification = !this.toggleNotification;
  //   this.newNotificationToggle = false;
  //   this.toggelingNotificationIconClass = ['show-notification-icon'];
  //   this.stopIntervalNotificationIconToggeling();
  //   if (this.notificationBackgroundClass.toString() == ['show-notification-transparent-background'].toString()) {
  //     this.notificationBackgroundClass = ['hide-notification-transparent-background'];
  //   } else {
  //     this.notificationBackgroundClass = ['show-notification-transparent-background'];
  //   }
  // }

  // toggelingNotificationIcon() {
  //   this.toggelingNotificationIconInterval = setInterval(() => {
  //     if (this.newNotificationToggle) {
  //       if (this.toggelingNotificationIconClass.toString() == ['show-notification-icon'].toString()) {
  //         this.toggelingNotificationIconClass = ['hide-notification-icon'];
  //       } else {
  //         this.toggelingNotificationIconClass = ['show-notification-icon'];
  //       }
  //     } else {
  //       this.toggelingNotificationIconClass = ['show-notification-icon'];
  //     }
  //     // this.toggelingNotificationIcon(this.newNotificationToggle);
  //   }, 1000)
  // }

  // stopIntervalNotificationIconToggeling() {
  //   clearInterval(this.toggelingNotificationIconInterval);
  // }

  // routeToNotification(notificationData) {
  //   this.utilities.routingFromAndHaveSearch = true;
  //   this.utilities.currentSearch = {};
  //   if (notificationData.type == 'quotation') {
  //     this.utilities.currentSearch.searchText = notificationData.refNumber;
  //     this.utilities.currentSearch.searchType = 'quotation';
  //     this.utilities.routingFromAndHaveSearch = true;
  //     this.utilities.setSavedNotificationText(notificationData.refNumber);
  //     this.router.navigate(['/search/quotation'])
  //   } else if (notificationData.type == 'call') {
  //     this.utilities.currentSearch.searchText = notificationData.callerNumber;
  //     this.utilities.routingFromAndHaveSearch = true;
  //     this.utilities.setSavedNotificationText(notificationData.callerNumber);
  //     this.router.navigate(['/search/calls-history'])
  //   } else if (notificationData.type == 'order') {
  //     this.utilities.setSavedNotificationText(notificationData);
  //     // this.utilities.currentNotificationOrder = notificationData;
  //     this.router.navigate(['/dispatcher'])
  //   }
  //   this.toggleNotificationBody();
  // }


  /**
   * settings 
   * 
   * 
   */
  getSettings() {
    this.lookupService.getWebsiteSettingsBykey().subscribe(settings => {
      let array = [];
      array = settings;
      array.map(item => {
        if (item.key === 'image') {
          this.image = item.value
        } else if (item.key === 'aboutus' || item.key === 'about-us') {
          this.aboutus = item.value;
        } else if (item.key === 'title') {
          this.titleService.setTitle(item.value)
        }
      })
    });
  }

  changeLogo() {
    this.dataService.subject.subscribe(res => {
      console.log(res);
    })
  }

  home() {
    this.router.navigate(['/']);
  }
}
