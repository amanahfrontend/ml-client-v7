import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";

interface Customer {
  id: number;
  name: string;
}

@Component({
  selector: 'app-customer-dropdown',
  templateUrl: './customer-dropdown.component.html',
  styleUrls: ['./customer-dropdown.component.css']
})
export class CustomerDropdownComponent implements OnInit {

  @Output() customer = new EventEmitter();

  error: string = 'Fill at least 5 chars !';
  msg: string = 'No results, try again !';
  show: boolean = false;

  constructor(private lookupService: LookupService) { }

  ngOnInit() {
  }

  text: string;
  results: Customer[];

  /**
   * search in customers
   * 
   * 
   * @param event 
   */
  search(event) {    
    this.show = false;
    this.customer.emit({ id: null });

    this.lookupService.searchInCustomers(event.query).subscribe(customers => {
      this.show = false;
      if (customers.length > 0) {
        this.results = customers;
      } else {
        this.show = true;
      }
    });
  }

  /**
   * select customer
   * 
   * @param event 
   */
  select(event) {
    this.customer.emit(event);
  }

  clear(){
    this.show = false;
    this.customer.emit({ id: null });
  }
}
