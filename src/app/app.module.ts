import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MessageService } from "primeng/components/common/messageservice";
import { UtilitiesService } from "./api-module/services/utilities/utilities.service";
import { AppComponent } from "./app.component";
import { RoutingComponents, RoutingModule } from "./router.module";
import { SharedModuleModule } from "./shared-module/shared-module.module";
import { DataService } from "./admin/settings/data.service";
import { AuthService } from './services/auth.service';
import { HeaderModule } from './components/header/header.module';
import { FooterModule } from './components/footer/footer.module';
import { AlertModule } from './components/alert/alert.module';
import { AuthenticationService } from './api-module/services/authentication/authentication.service';
import { AuthInterceptor } from "./shared-module/shared/auth.interceptor";
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HeaderModule,
    FooterModule,
    AlertModule,
    HttpClientModule,
    SharedModuleModule.forRoot(),
    ToastModule,
  ],

  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    UtilitiesService,
    MessageService,
    AuthenticationService,
    DataService,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
