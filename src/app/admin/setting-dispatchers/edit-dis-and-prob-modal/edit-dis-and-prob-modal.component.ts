import {
  Component,
  OnInit,
  Input,
} from "@angular/core";
import {
  FormControl,
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';
import {
  displayFieldCss,
  isFieldValid,
  validateAllFormFields
} from './../../../helpers/validator';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-edit-dis-and-prob-modal',
  templateUrl: './edit-dis-and-prob-modal.component.html',
  styleUrls: ['./edit-dis-and-prob-modal.component.css'],
})

export class EditDisAndProbModalComponent implements OnInit {
  @Input() data;

  areas: any[] = [];
  getareas: any[] = [];
  areasOptions: any[] = [];
  governorates: any[] = [];
  problems: any[]=[];

  selectedGovernerates: any[] = [];
  selectedAreas: any[] = [];
  selectedAreaOptions: any[] = [];

  selectedProblems: any[] = [];
  ids = [];

  prevGovernerates: any[] = [];

  areasRequestsCount: number = 0;

  governoratesFetchedBefore = [];

  form: FormGroup;

  GovernmentsCtrl = new FormControl('');
  areasCtrl = new FormControl('');
  problemsCtrl = new FormControl('');

  displayFieldCss = displayFieldCss;
  isFieldValid = isFieldValid;
  validateAllFormFields = validateAllFormFields;
  toggleLoading: boolean;
  id;
  cornerMessage;

  constructor(
    private activeModal: NgbActiveModal,
    private _lookup: LookupService,
    private fb: FormBuilder) {
    this.form = this.fb.group({
      governoratesCtrl: this.GovernmentsCtrl,
      areasCtrl: this.areasCtrl,
      problemsCtrl: this.problemsCtrl,
    });

  }


  ngOnInit() {
    this.governorates = this.data.governorates;
    this.getAllProblems();
    this.splitStrigs();
  }

  /**
   * governorates, areas , ... to arrays
   * 
   * 
   */
  splitStrigs() {
    console.log(this.data.settings);
    
    if (this.data.dispatcher.governments === "") {
      return;
    }

    for (let key in this.data.dispatcher) {
      if (this.data.dispatcher.hasOwnProperty(key) && key === 'governments') {
        let selectedGovernerateNames: any[] = this.data.dispatcher.governments.split(",");

        this.selectedGovernerates = selectedGovernerateNames.map(selectedGovName => {
          let selectedgovernrate = this.governorates.find(x => x.name == selectedGovName.trim(), 0);

          this.getAreasByGovernorate(selectedgovernrate.id);
          return selectedgovernrate;
        });

        this.prevGovernerates = this.selectedGovernerates;
      }


      if (this.data.dispatcher.hasOwnProperty(key) && key === 'selectedProplems') {
        this.selectedProblems = this.data.dispatcher.selectedProplems;
      }
    }
  }

  send() {
    //////intialize send object

    this.toggleLoading = true;
    let DispAreaAndProplem: any = {}

    DispAreaAndProplem.id = this.data.dispatcher.id;

    /// :proplem ids comma separated
    DispAreaAndProplem.proplems = this.selectedProblems.map(function (elem) {
      return elem.id;
    }).join(",");;

    //areas ids comma separated
    DispAreaAndProplem.areas = this.selectedAreas.map(function (elem) {
      return elem.name;
    }).join(",");



    /// : governments ids comma separated
    DispAreaAndProplem.governments = this.selectedGovernerates.map(function (elem) {
      return elem.name;
    }).join(",");;

    ////Attach Dispature Id  
    DispAreaAndProplem.fK_Dispatcher_Id = this.data.dispatcher.fK_Dispatcher_Id;


    if (DispAreaAndProplem.id == 0) {

      //if (this.form.invalid) {
      //  return this.validateAllFormFields(this.form);
      //}

      this._lookup.postNewDispatureAreasAndProblem(DispAreaAndProplem).subscribe(
        (resUser) => {
          this.alertSuccess();
          this.toggleLoading = false;

          this.close();
        },
        err => { });
    }
    else {
      // const formValue = this.form.value;
      //if (formValue['governoratesCtrl'].length == 0 && formValue['areasCtrl'].length == 0 && formValue['problemsCtrl'].length == 0) {
      //  return this.validateAllFormFields(this.form);
      //} else {
      this._lookup.updateDispatureAreasAndProblemsLookUp(DispAreaAndProplem).subscribe(
        (res) => {
          this.toggleLoading = false;
          this.alertSuccess();
          this.close();
        });
      //}
    }



  }



  reset() {
    //////intialize send object
    this.toggleLoading = true;

    this.areas = [];
    this.governorates = [];
    this.problems = [];
    let DispAreaAndProplem: any = {}

    DispAreaAndProplem.id = this.data.dispatcher.id;


    /// :proplem ids comma separated
    DispAreaAndProplem.proplems = ''

    //areas ids comma separated
    DispAreaAndProplem.areas = ''



    /// : governments ids comma separated
    DispAreaAndProplem.governments = ''

    ////Attach Dispature Id  
    DispAreaAndProplem.fK_Dispatcher_Id = this.data.dispatcher.fK_Dispatcher_Id;


    if (DispAreaAndProplem.id == 0) {

      //if (this.form.invalid) {
      //  return this.validateAllFormFields(this.form);
      //}

      this._lookup.postNewDispatureAreasAndProblem(DispAreaAndProplem).subscribe(
        (resUser) => {
          this.alertSuccess();
          this.toggleLoading = false;

          this.close();
        },
        err => { });
    }
    else {
      // const formValue = this.form.value;
      //if (formValue['governoratesCtrl'].length == 0 && formValue['areasCtrl'].length == 0 && formValue['problemsCtrl'].length == 0) {
      //  return this.validateAllFormFields(this.form);
      //} else {
      this._lookup.updateDispatureAreasAndProblemsLookUp(DispAreaAndProplem).subscribe(
        (res) => {
          this.alertSuccess();
          this.toggleLoading = true;

          this.close();
        });
      //}
    }



  }
  /**
   * fetch all problems
   * 
   * 
   */
  getAllProblems() {
    this._lookup.getAllProblems().subscribe((problems) => {


      this.problems = problems;
      this.problems.map((problem, i) => {
        this.problems[i] = {
          id: problem.id,
          name: problem.name
        }
      });

      this.selectedProblems = this.selectedProblems.map((selcted, index) => {

        var se = this.problems.find(x => x.id == selcted.id);
        if (se != undefined) {
          return se;
        }
        else {
          return undefined;
        }
      }).filter(x=>x != undefined);

    });
  }

  /**
   * areas by governorate
   * 
   * 
   * @param governorate 
   * @param id 
   */
  getAreasByGovernorate(id: number) {

    this.toggleLoading = true;
    this.areasRequestsCount++;

    this._lookup.GetallAreas(id).subscribe((areas) => {
      this.areasRequestsCount--;

      let array = [];
      array = areas.map((area, i) => {
        return {
          id: area.id,
          name: area.name,
          govId: id
        }

      });
      this.areasOptions.push(...array);

      if (this.areasRequestsCount == 0) {
        this.toggleLoading = false;
        this.areas = new Array<any>();
        this.areas.push(...this.areasOptions);



        let selectedAreasNames: any[] = this.data.dispatcher.areas.split(",");

        this.selectedAreaOptions = selectedAreasNames.map((SelectedArea, index) => {
          let selectedArea = this.areas.find(x => x.name == SelectedArea.trim(), 0);

          if (selectedArea != undefined || selectedArea != null) {

            return selectedArea;
          }
          else {
            return {
              id: 0,
              name: '',
              govId: 0,
            };
          }

        }).filter(x => x.id != 0);


        this.selectedAreas = new Array<any>();
        this.selectedAreas.push(...this.selectedAreaOptions);

      }

    }, error => {
      this.toggleLoading = false;
      this.areasRequestsCount--;
    });

  }


  close() {
    this.activeModal.dismiss();
  }


  /**
   * selected governorates
   * 
   * 
   */
  handleGovernoratesValues(event) {
    const diff = this.arrayDiff(this.selectedGovernerates, this.prevGovernerates);
    const status = this.selectedGovernerates.length > this.prevGovernerates.length ? 'selected' : 'unselected';

    switch (status) {
      case 'selected':
        this.getAreasByGovernorate(diff[0].id);
        break;

      case 'unselected':
        this.data.dispatcher.areas = '';
        this.areasOptions = this.areasOptions.filter(x => x.govId != diff[0].id);

        let selectedAreasList = this.selectedAreas.filter(x => x.govId != diff[0].id);

        this.data.dispatcher.areas = selectedAreasList.map(function (elem) {
          return elem.name;
        }).join(",");

        this.areas = new Array<any>();
        this.areas.push(...this.areasOptions);

        this.selectedAreas = new Array<any>();
        this.selectedAreas.push(...selectedAreasList);

        break;

      default:
        break;
    }

    this.prevGovernerates = this.selectedGovernerates;
  }

  /**
   * 
   * @param arrayOne 
   * @param arrayTwo 
   */
  arrayDiff(arrayOne, arrayTwo) {
    return [
      ...arrayOne.filter(x => arrayTwo.indexOf(x) === -1),
      ...arrayTwo.filter(x => arrayOne.indexOf(x) === -1)
    ];
  }


  alertSuccess() {
    Swal.fire('Success !')
  }

  handleAreasValues(event) {
    this.data.dispatcher.areas = '';
    this.data.dispatcher.areas = this.selectedAreas.map(function (elem) {
      return elem.name;
    }).join(",");
  }

}




