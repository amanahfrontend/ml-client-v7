import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDisAndProbModalComponent } from './edit-dis-and-prob-modal.component';

describe('EditDisAndProbModalComponent', () => {
  let component: EditDisAndProbModalComponent;
  let fixture: ComponentFixture<EditDisAndProbModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDisAndProbModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDisAndProbModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
