import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { EditDisAndProbModalComponent } from "./edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";

@Component({
  selector: 'app-setting-dispatchers',
  templateUrl: './setting-dispatchers.component.html',
  styleUrls: ['./setting-dispatchers.component.css'],
})

export class SettingDispatchersComponent implements OnInit {
  disWithProbs: any[];
  allDispatchers: any[] = [];
  allAreas;
  governorates = [];
  allProblems: any[] = [];
  toggleLoading;
  dispatureWithProblems: any[] = [];

  settings = {
    dispArea: true,
    dispProblem: true,
  };

  constructor(
    private lookup: LookupService,
    private modalService: NgbModal) {
  }


  ngOnInit() {
    //this.toggleLoading = true;
    this.getSettings();
    this.getAllProblems();
    this.allGovernorates();
  }

  setupAreasForFilter() {
    this.allAreas = [];
    this.disWithProbs.map((dis) => {
      if (!this.allAreas.includes(dis.area) && dis.area != '-') {
        this.allAreas.push(dis.area);
      }
    });

    this.allAreas.map((area, i) => {
      this.allAreas[i] = {
        label: area,
        value: area
      }
    });
  }

  getAllProblems() {

    this.toggleLoading = true;

    this.lookup.getAllProblems().subscribe((problems) => {
      this.allProblems = problems;

      //this.toggleLoading = false;

      this.getDispatcherWithProblems();
      this.allProblems.map((prob, i) => {
        this.allProblems[i] = {
          id: prob.id,
          name: prob.name,
          label: prob.name,
          value: prob.name
        }
      });
    });
  }

  getAllDispatchers() {
    let emptyList = [];

    //this.toggleLoading = true;

    this.lookup.getDispatchers().subscribe((dispatchers) => {


      this.allDispatchers = dispatchers;
      this.disWithProbs = this.allDispatchers.map((dis, i) => {
        let DispatureAndAreaProblem: any = this.dispatureWithProblems.find(x => x.fK_Dispatcher_Id == dis.id, 0);
        if (DispatureAndAreaProblem == undefined) {
          DispatureAndAreaProblem = {};
          DispatureAndAreaProblem.id = 0;
          DispatureAndAreaProblem.fK_Dispatcher_Id = dis.id;
          DispatureAndAreaProblem.dispatcher = dis;

          DispatureAndAreaProblem.governments = '';
          DispatureAndAreaProblem.areas = '';
          DispatureAndAreaProblem.proplems = '';
        }

        return DispatureAndAreaProblem;
      });

      this.toggleLoading = false;
      //this.disWithProbs = this.disWithProbs.concat(this.allDispatchers);
      //this.setupAreasForFilter();
      //this.toggleLoading = false;
    });
  }


  openEditModal(dispatcher) {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false
    };

    let modalRef = this.modalService.open(EditDisAndProbModalComponent, ngbModalOptions);
    modalRef.componentInstance.data = { dispatcher: dispatcher, governorates: this.governorates, settings: this.settings };

    modalRef.result
      .then(() => {
        this.getAllProblems();
      })
      .catch(() => {
        this.getAllProblems();
      });
  }

  /**
   * remove dispatcher
   * 
   * 
   * @param dispatcher 
   */
  // remove(dispatcher) {
  //   this.lookup.removeDisByProb(dispatcher.id).subscribe(() => {
  //     this.disWithProbs = this.disWithProbs.filter((disWIthProb) => {
  //       return disWIthProb.id != dispatcher.id;
  //     });
  //   });
  // }




  /**
   *all dispatchers
   * 
   * 
   */
  getDispatcherWithProblems() {
    this.lookup.getDispatchersWithProb().subscribe((diswithProbs) => {
      var proplemNames = '';

      this.disWithProbs = diswithProbs;

      this.getAllDispatchers();

      this.disWithProbs.forEach((v, index, arr) => {
        v.problemnames = '';
        let proplemids: any[] = v.proplems.split(",");
        let selectedProplems: any[] = proplemids.map((proplemid) => {
          var proplemobj = this.allProblems.find(x => x.id == proplemid, 0);
          if (proplemobj != undefined) {

            return {
              id: proplemid,
              name: this.allProblems.find(x => x.id == proplemid, 0).name,
            };

          }
          else {
            return undefined;

          }

        });

        v.selectedProplems = selectedProplems;
        v.selectedProplemNames = selectedProplems.filter(x => x != undefined).map(function (elem) {
          return elem.name;
        }).join(",");
      });
      this.dispatureWithProblems = this.disWithProbs;
    });
  }

  /**
   * fetch all Governments
   * 
   * 
   */
  allGovernorates() {
    this.lookup.GetallGovernorates().subscribe((governorates) => {
      this.governorates = governorates;
    });
  }


  /**
   * get settings 
   * 
   * 
   */
  getSettings() {
    // this.lookup.getWebsiteSettingsBykey().subscribe(settings => {
    //   let array = [];
    //   array = settings;

    //   array.map(item => {
    //     if (item.key === 'dispArea') {
    //       this.settings.dispArea = this.getBoolean(item.value);
    //     }

    //     if (item.key === 'dispProblem') {
    //       this.settings.dispProblem = this.getBoolean(item.value);
    //     }
    //   });
    // });

    this.settings.dispArea = true;
    this.settings.dispProblem = true;
  }

  /**
   * return bool
   * 
   * @param value 
   */
  getBoolean(value): boolean {
    switch (value) {
      case "true":
        return true;
      default:
        return false;
    }
  }

}
