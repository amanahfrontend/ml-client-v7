import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SharedModuleModule } from "../shared-module/shared-module.module";
import { AdminMainPageComponent } from "./admin-main-page/admin-main-page.component";
import { AdminUploadResourcesComponent } from "./admin-upload-resources/admin-upload-resources.component";
import { AdminUsersComponent } from "./admin-users/admin-users.component";
import { AssignTecComponent } from "./assign-tec/assign-tec.component";
import { CustomerListComponent } from './customer-list/customer-list.component';
import { UserService } from "./services/user.service";
import { SettingsComponent } from './settings/settings.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AllContractsComponent } from './all-contracts/all-contracts.component';
import { ContractTableComponent } from './all-contracts/contract-table/contract-table.component';
import { FieldErrorDisplayModule } from './../components/field-error-display/field-error-display.module';
import { SettingDispatchersComponent } from "./setting-dispatchers/setting-dispatchers.component";
import { SimpleAdminContentComponent } from "./simple-admin-content/simple-admin-content.component";
import { EditDisAndProbModalComponent } from "./setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";
import { AdminChangePasswordComponent } from "./admin-users/admin-change-password/admin-change-password.component";
import { RoleGuard } from './../api-module/guards/role.guard';
import { FilterPipe } from './../pipes/filter.pipe';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

const routes: Routes = [
  {
    path: "",
    component: AdminMainPageComponent,
    canActivate: [RoleGuard],
    data: { roles: ["Admin"] }
  },
  {
    path: "users",
    component: AdminUsersComponent,
    canActivate: [RoleGuard],
    data: { roles: ["Admin"] }
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    CommonModule,
    SharedModuleModule,
    MultiSelectModule,
    FieldErrorDisplayModule,
    RouterModule.forChild(routes),
    FormsModule,
    ToastModule,
    DialogModule,
    PerfectScrollbarModule,
    AccordionModule,
    CheckboxModule
  ],
  declarations: [
    CustomerListComponent,
    AdminMainPageComponent,
    SimpleAdminContentComponent,
    AdminUsersComponent,
    AdminUploadResourcesComponent,
    AssignTecComponent,
    SettingDispatchersComponent,
    EditDisAndProbModalComponent,
    AdminChangePasswordComponent,
    SettingsComponent,
    AllContractsComponent,
    ContractTableComponent,
    FilterPipe
  ],
  entryComponents: [
    EditDisAndProbModalComponent,
    AdminChangePasswordComponent
  ],
  providers: [UserService, RoleGuard, {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }]
})
export class AdminModule {
}
