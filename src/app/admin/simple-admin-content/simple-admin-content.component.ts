import { Component, OnInit, Input, OnChanges, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { PromptComponent } from '../../shared-module/shared/prompt/prompt.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-simple-admin-content',
  templateUrl: './simple-admin-content.component.html',
  styleUrls: ['./simple-admin-content.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})

export class SimpleAdminContentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() getData: string;

  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;

  loadedBefore = {
    customerType: false,
    callType: false,
    callPriority: false,
    phoneType: false,
    actionStatus: false,
    contractType: false,
    orderType: false,
    orderPriority: false,
    orderStatus: false,
    orderProblems: false,
    orderProgress: false,
    machineType: false,
    role: false,
  };

  customerTypes: any[];
  callTypes: any[];
  callPriorities: any[];
  phoneTypes: any[];
  actionStatus: any[];
  contractTypes: any[];
  orderTypes: any[];
  orderPriorities: any[];
  orderStatus: any[];
  orderProblems: any[];
  orderProgress: any[];
  machineTypes: any[];
  roles: any[];

  currentTitle: string = '';

  constructor(private lookUp: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal,
    private utilities: UtilitiesService) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.listViaType();
  }


  listViaType() {
    if (this.type == 'Customer Types' && this.getData == 'Customer Types') {
      switch (this.loadedBefore.customerType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getCustomerTypes().subscribe((data) => {
            this.loadedBefore.customerType = true;
            this.toggleLoading = false;
            this.customerTypes = data;
            this.rows = this.customerTypes;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.customerTypes;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Call Types' && this.getData == 'Call Types') {
      switch (this.loadedBefore.callType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getCallsTypes().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.callType = true;
            this.callTypes = data;
            this.rows = this.callTypes;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.callTypes;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Call Priorities' && this.getData == 'Call Priorities') {
      switch (this.loadedBefore.callPriority) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getPriorities().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.callPriority = true;
            this.callPriorities = data;
            this.rows = this.callPriorities;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            });
          break;

        case true:
          this.rows = this.callPriorities;
          break;

        default:
          break;
      }

    }
    else if (this.type == 'Phone Types' && this.getData == 'Phone Types') {
      switch (this.loadedBefore.phoneType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getPhoneTypes().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.phoneType = true;

            this.phoneTypes = data;
            this.rows = this.phoneTypes;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.phoneTypes;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Action Statuses' && this.getData == 'Action Statuses') {
      switch (this.loadedBefore.phoneType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getStatues().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.actionStatus = true;

            this.actionStatus = data;
            this.rows = this.actionStatus;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.actionStatus;
          break;

        default:
          break;
      }

    }
    else if (this.type == 'Contract Types' && this.getData == 'Contract Types') {
      switch (this.loadedBefore.contractType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getContractTypes().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.contractType = true;
            this.contractTypes = data;
            this.rows = this.contractTypes;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.contractTypes;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Role' && this.getData == 'Role') {
      switch (this.loadedBefore.role) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getRoles().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.role = true;

            this.roles = data;
            this.rows = this.roles;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:

        default:
          break;
      }
    }
    else if (this.type == 'Order Types' && this.getData == 'Order Types') {
      switch (this.loadedBefore.orderType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getOrderType().subscribe((data) => {
            this.toggleLoading = false;
            this.orderTypes = data;
            this.rows = this.orderTypes;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.orderTypes;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Order Priorities' && this.getData == 'Order Priorities') {
      switch (this.loadedBefore.orderPriority) {
        case false:
          this.dataSubscription = this.lookUp.getOrderPriority().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.orderPriority = true;

            this.orderPriorities = data;
            this.rows = this.orderPriorities;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.orderPriorities;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Order Statuses' && this.getData == 'Order Statuses') {
      switch (this.loadedBefore.orderStatus) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getOrderStatus().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.orderStatus = true;

            this.orderStatus = data;
            this.rows = this.orderStatus;
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            })
          break;

        case true:
          this.rows = this.orderStatus;
          break;

        default:
          break;
      }
    }
    else if (this.type == 'Order Progresses' && this.getData == 'Order Progresses') {
      switch (this.loadedBefore.orderProgress) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getAllOrderProgress().subscribe((data) => {
            this.toggleLoading = false;
            this.loadedBefore.orderProgress = true;

            this.orderProgress = data;
            this.rows = this.orderProgress
          },
            err => {
              this.alertError();
              this.toggleLoading = false;
            });
          break;

        case true:
          this.rows = this.orderProgress
          break;

        default:
          break;
      }

    }
    else if (this.type == 'Order Problems' && this.getData == 'Order Problems') {
      switch (this.loadedBefore.orderProblems) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getAllOrderProblems()
            .subscribe(
              (data) => {
                this.toggleLoading = false;
                this.loadedBefore.orderProgress = true;

                this.orderProblems = data;
                this.rows = this.orderProblems;
              }, err => {
                this.alertError();
                this.toggleLoading = false;
              }
            );
          break;

        case true:
          this.rows = this.orderProblems;
          break;
        default:
          break;
      }

    }
    else if (this.type == 'Machine Types' && this.getData == 'Machine Types') {
      switch (this.loadedBefore.machineType) {
        case false:
          this.toggleLoading = true;
          this.dataSubscription = this.lookUp.getAllMachineTypes()
            .subscribe(
              (data) => {
                this.toggleLoading = false;
                this.loadedBefore.machineType = true;

                this.machineTypes = data;
                this.rows = this.machineTypes;
              }, err => {
                this.alertError();
                this.toggleLoading = false;
              }
            );
          break;

        case true:
          this.rows = this.machineTypes;
          break;

        default:
          break;
      }

    }
  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    this.openModal({}, `Add New ${this.type}`, this.type);
    this.modalRef.result.then((newValue) => {

      if (this.type == 'Customer Types') {
        this.dataSubscription = this.lookUp.postCustomerTypes(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data);

          this.loadedBefore.customerType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })

      }
      else if (this.type == 'Call Types') {
        this.dataSubscription = this.lookUp.postCallsTypes(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.callType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Call Priorities') {
        this.dataSubscription = this.lookUp.postCallPriorities(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.callPriority = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Phone Types') {
        this.dataSubscription = this.lookUp.postPhoneTypes(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.phoneType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Action Statuses') {
        this.dataSubscription = this.lookUp.postActionStatues(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.actionStatus = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Contract Types') {
        this.dataSubscription = this.lookUp.postContractTypes(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.contractType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Role') {
        this.dataSubscription = this.lookUp.postRoles(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.role = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Types') {
        this.dataSubscription = this.lookUp.postOrderType(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.orderType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Priorities') {
        this.dataSubscription = this.lookUp.postOrderPriority(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)


          this.loadedBefore.orderPriority = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Statuses') {
        this.dataSubscription = this.lookUp.postOrderStatus(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)


          this.loadedBefore.orderStatus = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Progresses') {
        this.dataSubscription = this.lookUp.postOrderProgress(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.orderProgress = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Problems') {
        this.dataSubscription = this.lookUp.postOrderProblems(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data)

          this.loadedBefore.orderProblems = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Machine Types') {
        this.dataSubscription = this.lookUp.postMachineTypes(newValue).subscribe((data) => {
          this.alertSuccess('add');
          this.rows.push(data);

          this.loadedBefore.machineType = false;
          this.listViaType();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
    })
      .catch((result) => {
        //console.log('nothing added');
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Added!',
          detail: "You didn't saved new value."
        });
      });

    this.ngOnChanges();
  }

  edit(row) {
    //console.log(row);
    this.openModal(Object.assign({}, row), 'Edit', this.type);

    this.modalRef.result.then((editedValue) => {
      // let editedToPost = {
      //   id: row.id,
      //   name: editedValue.name,
      //   fK_OrderStatus_Id: editedValue.fK_OrderStatus_Id
      // };
      // console.log(editedValue);
      if (this.type == 'Customer Types') {
        this.dataSubscription = this.lookUp.updateCustomerTypes(row).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Call Types') {
        this.dataSubscription = this.lookUp.updateCallsTypes(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Call Priorities') {
        this.dataSubscription = this.lookUp.updatePriorities(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Phone Types') {
        this.dataSubscription = this.lookUp.updatePhoneTypes(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Action Statuses') {
        this.dataSubscription = this.lookUp.updateStatues(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Contract Types') {
        this.dataSubscription = this.lookUp.updateContractTypes(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Role') {
        this.dataSubscription = this.lookUp.updateRoles(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Types') {
        this.dataSubscription = this.lookUp.updateOrderType(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Priorities') {
        this.dataSubscription = this.lookUp.updateOrderPriority(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Statuses') {
        this.dataSubscription = this.lookUp.updateOrderStatus(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Progresses') {
        this.dataSubscription = this.lookUp.updateOrderProgressLookUp(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Order Problems') {
        this.dataSubscription = this.lookUp.updateOrderProblemsLookUp(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
      else if (this.type == 'Machine Types') {
        this.dataSubscription = this.lookUp.updateMachineType(editedValue).subscribe((data) => {
          row.name = editedValue.name;
          this.alertSuccess('edit');
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.alertError();
          })
      }
    })
      .catch((result) => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      });
  }

  remove(row) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        //console.log(row);
        if (this.type == 'Customer Types') {
          this.deleteDataSubscription = this.lookUp.deleteCustomertype(row.id).subscribe(() => {
            // this.rows = data;
            // //console.log(this.rows);
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();

            })
        }
        else if (this.type == 'Call Types') {
          this.dataSubscription = this.lookUp.deleteCallType(row.id).subscribe((data) => {
            // this.rows = data;
            // //console.log(this.rows);
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();

            })
        }
        else if (this.type == 'Call Priorities') {
          this.dataSubscription = this.lookUp.deletePriorities(row.id).subscribe((data) => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();

            })
        }
        else if (this.type == 'Phone Types') {
          this.dataSubscription = this.lookUp.deletePhoneTypes(row.id).subscribe((data) => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Action Statuses') {
          this.dataSubscription = this.lookUp.deleteStatus(row.id).subscribe((data) => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Contract Types') {
          this.dataSubscription = this.lookUp.deleteContractType(row.id).subscribe((data) => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Role') {
          this.dataSubscription = this.lookUp.deleteRole(row.name).subscribe((data) => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Order Types') {
          this.dataSubscription = this.lookUp.deleteOrderType(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Order Priorities') {
          this.dataSubscription = this.lookUp.deleteOrderPriority(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Order Statuses') {
          this.dataSubscription = this.lookUp.deleteOrderStatus(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Order Progresses') {
          this.dataSubscription = this.lookUp.removeProgressStatus(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Order Problems') {
          this.dataSubscription = this.lookUp.removeProblem(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();
            })
        }
        else if (this.type == 'Machine Types') {
          this.dataSubscription = this.lookUp.removeMachineType(row.id).subscribe(() => {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != row.id;
            });
            this.alertSuccess('remove');
          },
            err => {
              err.status == 401 && this.utilities.unauthrizedAction();
              this.alertError();

            })
        }
      }

    });

  }

  openModal(data, header, type?) {
    this.modalRef = this.modalService.open(PromptComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type;
    this.modalRef.componentInstance.data = data;
  }



  /**
   * error msg
   * 
   * 
   */
  alertError() {
    this.messageService.add({
      severity: 'error',
      summary: 'Failed!',
      detail: "Failed to Save due to network error"
    });
  }

  /**
   * success msg
   * 
   * 
   */
  alertSuccess(type: string) {
    switch (type) {
      case 'add':
        this.messageService.add({
          severity: 'success',
          summary: 'Success!',
          detail: 'Added successfully!'
        });
        break;

      case 'edit':
        this.messageService.add({
          severity: 'success',
          summary: 'Success!',
          detail: 'Edited successfully!'
        });
        break;

      case 'remove':
        this.messageService.add({
          severity: 'success',
          summary: 'Success!',
          detail: 'Removed successfully!'
        });
        break;

      default:
        break;
    }

  }


}
