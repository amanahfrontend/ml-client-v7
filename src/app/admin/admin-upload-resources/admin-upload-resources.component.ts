import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';

@Component({
  selector: 'app-admin-upload-resources',
  templateUrl: './admin-upload-resources.component.html',
  styleUrls: ['./admin-upload-resources.component.css']
})

export class AdminUploadResourcesComponent implements OnInit {
  uploadedFiles: any[] = [];
  toggleItemsUploadButtons: boolean;
  toggleSalaryUploadButtons: boolean;
  itemsError: Message[] = [];
  salaryError: Message[] = [];

  constructor(private lookup: LookupService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.toggleItemsUploadButtons = false;
    this.toggleSalaryUploadButtons = false;
    this.salaryError = [];
  }

  showAtView(event, type) {
    this.uploadedFiles = [];
    this.uploadedFiles.push(event.files[0]);
    if (type == 'items') {
      this.toggleItemsUploadButtons = true;
    } else if (type == 'salary') {
      this.toggleSalaryUploadButtons = true;
    }
  }

  onUpload(event, type) {
    let files = event.files;
    let file = files[0];

    if (file) {
      let reader = new FileReader();
      let base64file;

      reader.onload = (readerEvt) => {
        let binaryStringFileReader = <FileReader>readerEvt.target;
        let binaryString = binaryStringFileReader.result;
        // document.getElementById("base64textarea").value = btoa(binaryString);
        base64file = btoa(binaryString.toString());

        /*********** collect the json object *************/
        let jsonObject = {
          "fileName": file.name,
          "fileContent": base64file
        };
        if (type == 'items') {
          this.lookup.postItemsExcelSheet(jsonObject)
          .subscribe(
            res => {
              this.toggleItemsUploadButtons = false;
              this.itemsError = [];
              this.itemsError.push({
                severity: 'success',
                summary: 'Successful!',
                detail: "Excel sheet uploaded successfully!"
              });
            },
            err => {
              this.toggleItemsUploadButtons = true;
              this.itemsError = [];
              this.itemsError.push({
                severity: 'error',
                summary: 'Failed!',
                detail: "Failed to upload item excel sheet due to network error, please try again later."
              });
            });
        } else if (type == 'salary') {
          this.lookup.postSalaryExcelSheet(jsonObject)
          .subscribe(
            res => {
              this.toggleSalaryUploadButtons= false;
              this.salaryError = [];
              this.salaryError.push({
                severity: 'success',
                summary: 'Successful!',
                detail: "Excel sheet uploaded successfully!"
              });
            },
            err => {
              this.toggleSalaryUploadButtons = true;
              this.salaryError = [];
              this.salaryError.push({
                severity: 'error',
                summary: 'Failed!',
                detail: "Failed to upload salary excel sheet due to network error, please try again later."
              });
            });
        }

        /***************************************************/
      };

      /* Not sure about the importance of this libe :( */
      let x = reader.readAsBinaryString(file);
    }
  };
}
