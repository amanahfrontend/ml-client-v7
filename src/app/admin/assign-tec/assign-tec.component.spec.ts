import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTecComponent } from './assign-tec.component';

describe('AssignTecComponent', () => {
  let component: AssignTecComponent;
  let fixture: ComponentFixture<AssignTecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignTecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
