import {Component, OnInit, OnDestroy} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {DragulaService} from 'ng2-dragula';
import {Message} from "primeng/components/common/message";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-assign-tec',
  templateUrl: './assign-tec.component.html',
  styleUrls: ['./assign-tec.component.css'],
  providers:[
    DragulaService
  ]
})

export class AssignTecComponent implements OnInit, OnDestroy {
  dispatchers: any[];
  tecs: any[];
  tecToPost: any;
  disToPost: any;
  activeDis: any;
  toggleLoading: boolean;
  tecLoading: boolean;
  toggleRemoveTecButtons: boolean;
  getDispatchersSubscription: Subscription;
  getTecsByIdSubscription: Subscription;
  getTecsSubscription: Subscription;
  removeSubscription: Subscription;
  cornerMessage: Message[];

  constructor(private lookup: LookupService, private dragulaService: DragulaService) {
  }

  postTecByDis(val) {
    this.tecLoading = true;
    setTimeout(() => {
      let tecWithDis = {
        fK_technican_id: this.tecToPost.id,
        fK_dispatcher_id: this.disToPost.id
      };
      if (val.target.classList.contains('dispatcher-card')) {
        this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {
            this.cornerMessage = [];
            this.cornerMessage.push({
              severity: 'success',
              summary: 'successfully',
              detail: `Technician ${this.tecToPost.firstName} ${this.tecToPost.middleName} Assigned to dispatcher ${this.disToPost.firstName} ${this.disToPost.middleName} successfully!`
            });
            this.tecLoading = false;
          },
          err => {
            this.cornerMessage = [];
            this.cornerMessage.push({
              severity: 'error',
              summary: 'Failed',
              detail: 'Failed to Assign Technician!'
            });
            this.tecLoading = false;
          })
      }
    }, 200)
  }

  ngOnInit() {
    // this.tecs = [];
    // this.dispatchers = [];
    this.toggleLoading = true;
    this.getDispatchers();
    this.getTecs();

    this.dragulaService.createGroup('tecs-drag', {
      removeOnSpill: false,
      copySortSource: false,
      revertOnSpill: true,
      accepts: (item) => {
        return item.classList.contains('tec-name');
      }
    });
    this.dragulaService.drop('tecs-drag').subscribe((value: any) => {
      this.postTecByDis(value);
    });

  }

  ngOnDestroy() {
    this.dragulaService.destroy('tecs-drag');
    this.getTecsSubscription.unsubscribe();
    this.getTecsByIdSubscription && this.getTecsByIdSubscription.unsubscribe();
    this.getDispatchersSubscription.unsubscribe();
    this.removeSubscription && this.removeSubscription.unsubscribe();
  }

  setActiveDis(dis) {
    this.activeDis = dis;
  }

  setTecToPost(tec) {
    this.tecToPost = tec;
  }

  setDisToPost(dis) {
    this.disToPost = dis;
  }

  getTecs() {
    this.getTecsSubscription = this.lookup.getTecs().subscribe((tecs) => {
        this.tecs = tecs;
        this.toggleRemoveTecButtons = false;
        this.activeDis = null;
        if (this.dispatchers) {
          this.toggleLoading = false;
        }
      },
      err => {
        this.toggleLoading = false;
      }
    )
  };

  getTecsByDis(dis) {
    this.tecLoading = true;

    this.getTecsByIdSubscription = this.lookup.getTecsByDis(dis.id).subscribe((tecs) => {
        this.tecs = tecs;
        this.toggleRemoveTecButtons = true;
        this.setActiveDis(dis);
        this.tecLoading = false;
      },
      err => {
        this.tecLoading = false;
      }
    )
  };

  removeTec(id) {
    this.tecLoading = true;
    this.removeSubscription = this.lookup.removeTecFromDis(id).subscribe(() => {
        this.tecs = this.tecs.filter((tec) => {
          return tec.id != id;
        });
        this.tecLoading = false;
      },
      err => {
        this.tecLoading = false;
        // this.toggleLoading = false;
      }
    )
  };

  getDispatchers() {
    this.getDispatchersSubscription = this.lookup.getDispatchers().subscribe((dispatchers) => {
        this.dispatchers = dispatchers;

        console.log(this.dispatchers);
        
        if (this.tecs) {
          this.toggleLoading = false;
        }
      },
      err => {
        this.toggleLoading = false;
      })
  }

}
