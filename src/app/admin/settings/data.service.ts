import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  public ValidateAddressSubject = new Subject();
  public isAddressComponentValid: boolean = true;
  public addresssCount: number = 0;
  public ValidateAddressCompleted = new Subject();


  update: Observable<Object>;
  public subject = new Subject<Object>();

  /**
   * 
   * @param value 
   */
  pushSettings(value) {
    this.subject.next(value);
  }


  constructor() {
    this.update = this.subject.asObservable();

    this.ValidateAddressCompleted.subscribe(
      x => {
        this.addresssCount = 0;
      }
    )
  }


  ValidateAddress() {

    console.log("Validate Address ");
    this.ValidateAddressSubject.next();

  }

  ValidateAllAddressCompleted()
  {
    console.log("Validate Address Completed ");
    this.ValidateAddressCompleted.next();
  }

}
