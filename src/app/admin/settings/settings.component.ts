import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { displayFieldCss, validateAllFormFields, isFieldValid } from './../../helpers/validator';
import { LookupService } from './../../api-module/services/lookup-services/lookup.service';
import { DataService } from './data.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})

export class SettingsComponent implements OnInit {

  form: FormGroup;

  image = new FormControl('');
  aboutus = new FormControl('', [Validators.required]);
  title = new FormControl('', [Validators.required]);

  dispArea: any;
  dispProblem: any;


  imageSrc: string = 'assets/Images/photo.png';


  isFieldValid = isFieldValid;
  displayFieldCss = displayFieldCss;
  validateAllFormFields = validateAllFormFields;

  toggleLoading: boolean = false;

  selectedCities: string[] = [];


  constructor(
    private fb: FormBuilder,
    private lookupService: LookupService,
    private dataService: DataService) {

    this.form = this.fb.group({
      image: this.image,
      aboutus: this.aboutus,
      title: this.title,
    });

  }

  ngOnInit() {
    this.getSettings();
  }

  /**
   * 
   * @param event 
   */
  onChangeFile(event) {
    let file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    let pattern = /image-*/;
    let reader = new FileReader();

    if (!file.type.match(pattern)) {
      Swal.fire({
        type: 'error',
        title: 'invalid type',
      });

      return;
    }

    reader.onload = this.handleReader.bind(this);
    reader.readAsDataURL(file);
  }


  /**
   * 
   * @param event 
   */
  handleReader(event) {
    let reader = event.target;
    this.imageSrc = reader.result;
  }

  /**
   * update settings 
   * 
   * 
   */
  updateSettings() {
    console.log('yes... ', this.form.value);

    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }

    const body = [
      {
        File: null,
        Key: "aboutus",
        Value: this.form.value['aboutus']
      },
      {
        File: null,
        Key: "title",
        Value: this.form.value['title']
      },
      {
        File: null,
        Key: "dispArea",
        Value: this.dispArea
      },
      {
        File: null,
        Key: "dispProblem",
        Value: this.dispProblem
      },
    ];

    if (!this.validURL(this.imageSrc)) {
      body.push({
        File: {
          FileContent: this.imageSrc.split(',').pop()
        },
        Key: "image",
        Value: ""
      });
    }

    body.forEach(element => {
      this.lookupService.setWebsiteSettings(element).subscribe(res => {

        this.dataService.pushSettings([{ key: 'image', value: this.imageSrc }]);

        Swal.fire({
          type: 'success',
          title: 'success updated',
        })
      });
    });
  }

  getSettings() {
    this.toggleLoading = true;
    this.lookupService.getWebsiteSettingsBykey().subscribe(settings => {

      this.toggleLoading = false;

      let array = [];
      array = settings;

      this.dataService.pushSettings(settings);

      array.map(item => {
        if (item.key === 'image') {
          this.imageSrc = item.value;
        }
        else if (item.key === 'aboutus' || item.key === 'about-us') {
          this.form.patchValue({ aboutus: item.value })
        }
        else if (item.key === 'title') {
          this.form.patchValue({ title: item.value })
        }
        else if (item.key === 'dispArea') {
          this.dispArea = this.getBoolean(item.value);
        }
        else if (item.key === 'dispProblem') {
          this.dispProblem = this.getBoolean(item.value);
        }

      })
    });
  }

  validURL(str: string) {
    let pattern = new RegExp('^(https?:\\/\\/)?' +
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' +
      '((\\d{1,3}\\.){3}\\d{1,3}))' +
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
      '(\\?[;&a-z\\d%_.~+=-]*)?' +
      '(\\#[-a-z\\d_]*)?$', 'i');
    return !!pattern.test(str);
  }


  /**
   * return bool
   * 
   * @param value 
   */
  getBoolean(value): boolean {
    switch (value) {
      case "true":
        return true;
      default:
        return false;
    }
  }
}

