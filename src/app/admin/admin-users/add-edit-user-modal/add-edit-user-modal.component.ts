import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { isFieldValid, validateAllFormFields } from './../../../helpers/validator';

@Component({
  selector: 'app-add-edit-user-modal',
  templateUrl: './add-edit-user-modal.component.html',
  styleUrls: ['./add-edit-user-modal.component.css'],
})
export class AddEditUserModalComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Input() header: string;

  passwordRegex: any;
  emailRegex: any;
  roles: string[];
  rolesSubscription: Subscription;
  toggleLoading: boolean;
  allDispatchers: any[] = [];
  isTechnician: boolean = false;
  selectedDispatureid: string = '';

  userForm: FormGroup;
  isFieldValid = isFieldValid;

  constructor(
    public activeModal: NgbActiveModal,
    private lookup: LookupService,
    fb: FormBuilder
  ) {
    this.userForm = fb.group({
      firstName: ['', Validators.required],
      middleName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.minLength(8), Validators.pattern(/^\d+$/)])],
      userName: ['', Validators.required],
      email: ['', Validators.compose([Validators.pattern(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/)])],
      roleNames: ['', Validators.required],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)])],
      selectedDispatureid: [''],
    })
  }

  ngOnInit() {
    this.toggleLoading = true;

    switch (this.data['type']) {

      case 'edit':
        this.userForm.patchValue(this.data);
        this.userForm.controls["password"].clearValidators();
        this.userForm.controls['roleNames'].clearValidators();
        this.userForm.controls['roleNames'].disable();
        break;

      case 'add':
        this.userForm.controls['roleNames'].setValidators([Validators.required]);
        break;
    }


    this.rolesSubscription = this.lookup.getRoles().subscribe(roles => {
      this.roles = roles;
      this.toggleLoading = false;
    });

    this.data.roleNames = this.data.roleNames && this.data.roleNames[0];

    this.getAllDispatchers();

    if (this.data.roleNames == 'Technican') {
      this.isTechnician = true;
      this.getassignedDispatchersForTechnican(this.data.id);
    }
  }

  ngOnChanges() {
  }

  UserRoleChanged(event) {
    this.isTechnician = false;
    if (event.target.value == 'Technican') {
      this.isTechnician = true;
    }
  }


  getAllDispatchers() {
    this.lookup.getDispatchers().subscribe((dispatchers) => {
      this.allDispatchers = dispatchers;
    });
  }


  emitData() {
    const body = this.userForm.value;
    switch (this.data['type']) {
      case 'edit':
        delete this.userForm.value['password'];
        body['roleNames'] = [
          this.data['roleNames'],
        ];

        body['createdDate'] = this.data['createdDate'];

        break;
    }

    if (this.userForm.invalid) {
      return validateAllFormFields(this.userForm);
    }

    this.activeModal.close(body);
  }


  getassignedDispatchersForTechnican(id) {
    this.lookup.getAssignedDispbyTechId(id).subscribe((result) => {
      if (result != undefined) {
        this.data.selectedDispatureid = result.fK_Dispatcher_Id;
      }
    });
  }


  /**
   * special characters
   * 
   * 
   * @param e 
   */
  alpha(e) {
    let k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

} 
