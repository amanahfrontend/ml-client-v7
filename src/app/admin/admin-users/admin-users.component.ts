import {
  Component,
  Input,
  OnInit,
  OnDestroy
} from "@angular/core";
import Swal from 'sweetalert2';
import { Subscription } from "rxjs";
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FilterPipe } from './../../pipes/filter.pipe';
import { AlertService } from '../../services/alert.service';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AddEditUserModalComponent } from "./add-edit-user-modal/add-edit-user-modal.component";
import { AdminChangePasswordComponent } from "./admin-change-password/admin-change-password.component";

@Component({
  selector: "app-admin-users",
  templateUrl: "./admin-users.component.html",
  styleUrls: ["./admin-users.component.css"],
  providers: [
    FilterPipe
  ]
})

export class AdminUsersComponent implements OnDestroy, OnInit {
  @Input() getData: boolean;

  users;
  tempUsers: any[];

  UsersSubscription: Subscription;
  modalRef: any;
  toggleLoading;

  rolesCount: any;
  lockedUnlockedCount: any;

  searchString: string;

  constructor(private lookup: LookupService,
    private modalService: NgbModal,
    private filterPipe: FilterPipe,
    private alertService: AlertService
  ) {
  }


  ngOnInit() {
    this.toggleLoading = true;
    this.getUsers();
  }

  getUsers() {
    this.UsersSubscription = this.lookup.getUsers().subscribe(
      (users) => {
        this.toggleLoading = false;
        this.users = users;
        this.users.map((user) => {
          user.name = `${user.firstName} ${user.middleName} ${user.lastName}`;
        });

        this.countRoles(this.users);
        this.countlockedAndUnlockedUsers(this.users);

        this.tempUsers = this.users;
      },
      err => {
        this.toggleLoading = false;
        this.alertService.alertError({ title: 'Failed', details: 'Failed to load data due to server error.' })
      });
  }

  ngOnDestroy() {
    this.UsersSubscription && this.UsersSubscription.unsubscribe();
  }

  edit(user) {

    let userCopy = Object.assign({}, user);

    this.openAddEditUserModal(userCopy, `Edit ${userCopy.name} info`, 'edit');

    this.modalRef.result.then(
      (editedData) => {

        let userRoles = [];
        userRoles.push(editedData.roleNames);

        editedData.roleNames = userRoles;
        editedData['id'] = user['id'];

        delete editedData.name;

        editedData['roleNames'] = editedData['roleNames'][0];

        this.lookup.updateUser(editedData).subscribe(res => {
          this.getUsers();
          user = editedData;
          this.alertService.alertSuccess({ title: 'Successful', details: 'Saved Successfully' })
        },
          err => {
            this.alertService.alertError({ title: 'Failed', details: 'Failed to edit user, info due to server error.' })
          });
      })
      .catch(() => {
        this.alertService.alertInfo({ title: 'Nothing Edited', details: 'No edits saved' })
      });
  }

  changeUserPassword(user) {
    let userCopy = Object.assign({}, user);
    this.openModalChangePassword(userCopy, `Change ${userCopy.name} password`);

  }

  lockUnlock(user, str: string) {
    Swal.fire({
      title: `Are you sure to ${str} it?`,
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${str} it!`
    }).then((result) => {
      if (result.value) {
        this.lookup.lockUnlockUser(user.userName).subscribe(
          () => {
            this.alertService.alertSuccess({ title: 'Successful' });
            user.isLock = true;
            this.users.forEach(mapedUser => {
              if (user.id === mapedUser.id) {
                return user.isLocked = !user.isLocked;
              }
            });

          },
          err => {
            this.alertService.alertError({ title: 'Failed' })
          });
      }
    });


  }

  remove(user) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        if (user.roleNames[0] == "Dispatcher" || user.roleNames[0] == "SupervisorDispatcher") {
          this.lookup.IsOrdersAllowDeleteDispature(user.id).subscribe(
            (res) => {

              if (res == true) {
                this.lookup.removeUser(user.userName).subscribe(
                  () => {

                    this.alertService.alertSuccess({ title: 'Successful', details: '"User deleted Successfully' })

                    this.users = this.users.filter((oneUser) => {
                      return oneUser.userName !== user.userName;
                    });

                    this.getUsers();
                  });
              }
            });
        }
        else if (user.roleNames[0] == "Technican") {

          this.lookup.IsOrdersAllowDeleteTechnician(user.id).subscribe(
            (res) => {

              if (res == true) {
                this.lookup.IsInventoryAllowDeleteTech(user.id).subscribe(
                  (res) => {

                    if (res == true) {
                      this.lookup.removeUser(user.userName).subscribe(
                        () => {

                          this.alertService.alertSuccess({ title: 'Successful', details: 'User deleted Successfully' })

                          this.users = this.users.filter((oneUser) => {
                            return oneUser.userName !== user.userName;
                          });

                          this.getUsers();
                        },
                        err => {
                          this.alertService.alertError({ title: 'Failed', details: 'Failed to delete user due to network error, please try again later' })
                        });
                    }
                  });
              }
            });

        }
        else if (user.roleNames[0] == "Admin") {
          this.lookup.IsAllowToDeleteAdmin().subscribe(
            (res) => {

              if (res == true) {
                this.lookup.removeUser(user.userName).subscribe(
                  () => {

                    this.alertService.alertSuccess({ title: 'Successful', details: 'User deleted Successfully' })

                    this.users = this.users.filter((oneUser) => {
                      return oneUser.userName !== user.userName;
                    });

                    this.getUsers();
                  });
              }
            });
        }
        else {
          this.lookup.removeUser(user.userName).subscribe(
            () => {

              this.alertService.alertSuccess({ title: 'Successful', details: 'User deleted Successfully' })

              this.users = this.users.filter((oneUser) => {
                return oneUser.userName !== user.userName;
              });

              this.getUsers();
            });
        }
      }
    });
  }


  add() {
    this.openAddEditUserModal({}, `Add new user`, 'add');
    this.modalRef.result.then(
      (newUser) => {
        let userRoles = [];
        userRoles.push(newUser.roleNames);
        newUser.roleNames = userRoles;

        if (!newUser['email'] || newUser['email'] == '' || newUser['email'] == null || newUser['email'] == undefined) {
          newUser['email'] = `${newUser['userName']}@domain.com`;
        }

        this.lookup.postNewUser(newUser).subscribe(
          (resUser) => {
            if (resUser['roleNames'][0] == 'Technican') {
              let tecWithDis = {
                fK_technican_id: resUser['id'],
                fK_dispatcher_id: newUser.selectedDispatureid
              };

              this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {
                const details = `New user ${resUser['firstName']} ${resUser['middleName']} ${resUser['lastName']} saved successfully!`;
                this.alertService.alertSuccess({ title: 'Saved successfully!', details: details })

                resUser['name'] = resUser['firstName'] + resUser['middleName'] + resUser['lastName'];

                this.users.push(resUser);

                this.getUsers();

              });
            }
            else {
              const details = `New user ${resUser['firstName']} ${resUser['middleName']} ${resUser['lastName']} saved successfully!`;
              this.alertService.alertSuccess({ title: 'Saved successfully!', details: details });

              resUser['name'] = resUser['firstName'] + resUser['middleName'] + resUser['lastName'];

              this.users.push(resUser);

            }

            this.lookup.getUsers().subscribe(
              (users) => {
                this.toggleLoading = false;
                this.users = users;
                this.users.map((user) => {
                  user.name = `${user.firstName} ${user.middleName} ${user.lastName}`;
                });
                this.countRoles(this.users);
                this.tempUsers = this.users;
              },
              err => {
                this.toggleLoading = false;
                this.alertService.alertError({ title: 'Failed', details: `Failed to load data due to server error.` });
              });

          });
      })
      .catch(() => {
        this.alertService.alertInfo({ title: 'User canceled', details: `New user didn't saved and canceled.` });
      });
  }

  /**
   * user modal
   * 
   *  
   * @param data 
   * @param header 
   */
  openAddEditUserModal(data, header, type: string) {
    this.modalRef = this.modalService.open(AddEditUserModalComponent, { backdrop: 'static', size: 'lg' });
    this.modalRef.componentInstance.header = header;

    data['type'] = type;

    this.modalRef.componentInstance.data = data;
  }

  /**
   * change password modal
   * 
   * 
   * @param data 
   * @param header 
   */
  openModalChangePassword(data, header) {
    this.modalRef = this.modalService.open(AdminChangePasswordComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;

  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Name': 'Name',
      'UserName': 'User Name',
      'Email': 'Email',
      'Phone': 'Phone',
      'Roles': 'Roles',
    });

    this.users.map((item) => {
      exportData.push({
        'Name': item.name,
        'UserName': item.userName,
        'Email': item.email,
        'Phone': item.phoneNumber,
        'Roles': this.converArrayToString(item.roleNames)
      })
    });
    return new ngxCsv(exportData, 'Users', {
      showLabels: true
    });
  }

  /**
   * 
   * @param array 
   */
  converArrayToString(array = []): string {
    return array.join();
  }

  filter() {
    if (this.searchString.length == 0) {
      this.users = this.tempUsers;
    } else {
      this.users = this.filterPipe.transform(this.users, 'name', this.searchString);
    }

  }

  reset() {
    this.searchString = '';
    this.users = this.tempUsers;
  }

  /**
   * duplicates (count)
   * 
   * 
   * @param array 
   * @param key 
   */
  calculateDuplicates(array = [], key: string) {
    let map = new Object();
    for (let i = 0; i < array.length; i++) {
      if (map[array[i]] != null) {
        map[array[i][key]] += 1;
      } else {
        map[array[i][key]] = 1;
      }
    }

    return map;
  }

  /**
 * duplicates (count)
 * 
 * 
 * @param array 
 * @param key 
 */
  countRoles(array = []) {
    let map = new Object();
    for (let i = 0; i < array.length; i++) {
      if (map[array[i]['roleNames'][0]] != null) {
        map[array[i]['roleNames'][0]] += 1;
      } else {
        map[array[i]['roleNames'][0]] = 1;
      }
    }
    this.rolesCount = map;
  }

  /**
   * locked and unlocked (count)
   * 
   * 
   * @param array 
   * @param key 
   */
  countlockedAndUnlockedUsers(array = []) {
    let map = new Object();

    for (let i = 0; i < array.length; i++) {
      if (map[array[i]['isLocked']] != null) {
        map[array[i]['isLocked']] += 1;
      } else {
        map[array[i]['isLocked']] = 1;
      }
    }

    this.lockedUnlockedCount = map;
  }


}
