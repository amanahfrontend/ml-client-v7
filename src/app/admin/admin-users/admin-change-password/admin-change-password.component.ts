import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { UserService } from "../../services/user.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { isFieldValid, validateAllFormFields } from './../../../helpers/validator';
import { PasswordValidation } from "./match-passwords";
import { MessageService } from 'primeng/api';

@Component({
  selector: "app-admin-change-password",
  templateUrl: "./admin-change-password.component.html",
  styleUrls: ["./admin-change-password.component.css"]
})
export class AdminChangePasswordComponent implements OnInit {

  @Input() data: any;
  toggleLoading;
  header;
  submitted: boolean;
  doesNotMatch: boolean;

  changePasswordForm: FormGroup;

  isFieldValid = isFieldValid;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly userService: UserService,
    fb: FormBuilder,
    private messageService: MessageService
  ) {

    this.changePasswordForm = fb.group({
      password: ['', Validators.compose([Validators.required, Validators.min(6), Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)])],
      confirmedPassword: ['', Validators.required],
    }, {
        Validator:
          PasswordValidation.matchPassword
      });

  }

  ngOnInit() {
  }

  saveUserPassword() {
    if (this.changePasswordForm.invalid) {
      this.doesNotMatch = true;
      return validateAllFormFields(this.changePasswordForm);
    }

    if (this.changePasswordForm.value['password'] !== this.changePasswordForm.value['confirmedPassword']) {
      this.doesNotMatch = true;
      return validateAllFormFields(this.changePasswordForm);
    }

    const body = {
      id: this.data.id,
      modifiedByUserId: JSON.parse(localStorage.getItem("currentUser")).id,
      password: this.changePasswordForm.value['password'],
    }

    this.userService.updateUserPassword(body).subscribe((userRes) => {
      this.toast();
      this.close();
    });
  }

  close() {
    this.activeModal.close();
  }

  getLocalStorage(key) {
    return localStorage.getItem(key);
  }

  get changePasswordFormControls() { return this.changePasswordForm.controls; }

  onChange() {
    if (this.changePasswordForm.value['password'] === this.changePasswordForm.value['confirmedPassword']) {
      this.doesNotMatch = false;
    }
  }


  toast() {
    this.messageService.add({ severity: 'success', summary: 'Password updated successfully', detail: '' });
  }

}
