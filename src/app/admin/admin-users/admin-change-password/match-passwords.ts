import { AbstractControl } from '@angular/forms';
export class PasswordValidation {

    static matchPassword(AC: AbstractControl) {
        let password = AC.get('password').value;
        let confirmPassword = AC.get('confirmedPassword').value;
        if (password != confirmPassword) {
            AC.get('confirmedPassword').setErrors({ MatchPassword: true })
        } else {
            return null
        }
    }
}