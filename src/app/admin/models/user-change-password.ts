export class UserChangePassword {
  id: string; // User Id.
  password: string;
  modifiedByUserId: string;
}
